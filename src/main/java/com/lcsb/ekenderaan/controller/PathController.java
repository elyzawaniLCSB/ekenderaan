/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.controller;

import com.lcsb.ekenderaan.dao.AccountingPeriod;
import com.lcsb.ekenderaan.dao.BookingDAO;
import com.lcsb.ekenderaan.dao.CarDAO;
import com.lcsb.ekenderaan.dao.DriverDAO;
import com.lcsb.ekenderaan.dao.EmailDAO;
import com.lcsb.ekenderaan.dao.LeaveDAO;
import com.lcsb.ekenderaan.dao.NotifyDAO;
import com.lcsb.ekenderaan.dao.StaffDAO;
import com.lcsb.ekenderaan.model.Book;
import com.lcsb.ekenderaan.model.BookingDestinationTemp;
import com.lcsb.ekenderaan.model.Car;
import com.lcsb.ekenderaan.model.CoStaff;
import com.lcsb.ekenderaan.model.Driver;
import com.lcsb.ekenderaan.model.Executive;
import com.lcsb.ekenderaan.model.ExecutiveChild;
import com.lcsb.ekenderaan.model.Leave;
import com.lcsb.ekenderaan.model.LeaveComment;
import com.lcsb.ekenderaan.model.LeaveInfo;
import com.lcsb.ekenderaan.model.LeaveRequest;
import com.lcsb.ekenderaan.model.LeaveType;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.Notification;
import com.lcsb.ekenderaan.model.SysLogActivity;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "PathController", urlPatterns = {"/PathController"})
public class PathController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String path = request.getParameter("path");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLogActivity sys = new SysLogActivity();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "addnewcar":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_car_add.jsp";
                    break;
                case "viewleaveapproval":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/view_leave_approval.jsp";
                    break;
                case "applyleavestep":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/apply_leave_step.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    break;
                case "applysickleave":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/apply_sick_leave.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp().replaceAll("[-+.^:,]", "") + AccountingPeriod.getCurrentTime().replaceAll("[-+.^:,]", "");
                    break;
                case "applyhospitalleave":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/apply_hospital_leave.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp().replaceAll("[-+.^:,]", "") + AccountingPeriod.getCurrentTime().replaceAll("[-+.^:,]", "");
                    break;
                case "applymaternityleave":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/apply_maternity_leave.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp().replaceAll("[-+.^:,]", "") + AccountingPeriod.getCurrentTime().replaceAll("[-+.^:,]", "");
                    break;
                    case "applyihsanleave":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/apply_ihsan_leave.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp().replaceAll("[-+.^:,]", "") + AccountingPeriod.getCurrentTime().replaceAll("[-+.^:,]", "");
                    break;
                case "viewleavedetail":

                    NotifyDAO.updateNotifyFromPage(log, request.getParameter("leaveID"));

                    urlsend = "view_leave_approval.jsp?leaveID=" + request.getParameter("leaveID");
                    break;
                case "viewleavegrid":

                    NotifyDAO.updateNotifyFromPage(log, request.getParameter("leaveID"));

                    urlsend = "view_leave_bygrid.jsp?leaveID=" + request.getParameter("leaveID");
                    break;
                case "saveleaveform": {

                    String datestart = request.getParameter("datestart");
                    String dateend = request.getParameter("dateend");

                    datestart = datestart.substring(6, 10) + "-" + datestart.substring(3, 5) + "-" + datestart.substring(0, 2);
                    dateend = dateend.substring(6, 10) + "-" + dateend.substring(3, 5) + "-" + dateend.substring(0, 2);

                    Leave c = new Leave();

                    c.setDateapply(request.getParameter("dateapply"));
                    c.setDateend(dateend);
                    c.setDatestart(datestart);
                    c.setDays(Integer.parseInt(request.getParameter("days")));

                    if (request.getParameter("type").equals("C01")) {
                        if (request.getParameter("reason").equals("Lain-lain")) {
                            c.setReason(request.getParameter("reason-other"));
                        } else {
                            c.setReason(request.getParameter("reason"));
                        }
                    }else if (request.getParameter("type").equals("C05")){
                        c.setReason(request.getParameter("reason"));
                    }

                    c.setType(request.getParameter("type"));

                    String leaveID = LeaveDAO.saveLeave(log, c, request.getParameter("sessionid"));

                    EmailDAO.sendNow(log, "Preparing", leaveID);

                    sys.setActivity("apply leave");
                    sys.setDeptID("");
                    sys.setLeaveID(leaveID);
                    sys.setLocID(log.getLocID());
                    sys.setName(log.getFullname());
                    sys.setSessionid(session.getId());
                    sys.setUserid(log.getUserID());
                    LeaveDAO.saveActivity(log, sys);
                    
                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "saveleaveformupdaterecord": {

                    String datestart = request.getParameter("datestart");
                    String dateend = request.getParameter("dateend");

                    datestart = datestart.substring(6, 10) + "-" + datestart.substring(3, 5) + "-" + datestart.substring(0, 2);
                    dateend = dateend.substring(6, 10) + "-" + dateend.substring(3, 5) + "-" + dateend.substring(0, 2);

                    Leave c = new Leave();

                    c.setDateapply(request.getParameter("dateapply"));
                    c.setDateend(dateend);
                    c.setDatestart(datestart);
                    c.setDays(Integer.parseInt(request.getParameter("days")));
                    c.setReason(request.getParameter("reason"));
                    c.setType(request.getParameter("type"));

                    c.setStaffID(request.getParameter("staffID"));

                    String leaveID = LeaveDAO.saveLeaveOldRecord(log, c, request.getParameter("sessionid"));

                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "saveintroform": {

                    CoStaff c = new CoStaff();

                    c.setName(request.getParameter("name"));
                    c.setStaffid(request.getParameter("staffID"));
                    c.setEmail(request.getParameter("email"));
                    c.setPosition(request.getParameter("position"));
                    c.setLocation(request.getParameter("location"));
                    c.setDepartment(request.getParameter("department"));

                    StaffDAO.saveStaff(log, c, request.getParameter("supervisor"));

                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "updateleaveform": {

                    Leave c = new Leave();

                    c.setLeaveID(request.getParameter("leaveID"));
                    c.setDateapply(request.getParameter("dateapply"));
                    c.setDateend(request.getParameter("dateend"));
                    c.setDatestart(request.getParameter("datestart"));
                    c.setDays(Integer.parseInt(request.getParameter("days")));
                    c.setReason(request.getParameter("reason"));
                    c.setType(request.getParameter("type"));

                    LeaveDAO.updateLeave(log, c);
                    
                    sys.setActivity("update leave");
                    sys.setDeptID("");
                    sys.setLeaveID(request.getParameter("leaveID"));
                    sys.setLocID(log.getLocID());
                    sys.setName(log.getFullname());
                    sys.setSessionid(session.getId());
                    sys.setUserid(log.getUserID());
                    LeaveDAO.saveActivity(log, sys);


                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "validateleave": {

                    LeaveDAO.validateLeave(log, request.getParameter("leaveID"), request.getParameter("action"));
                    EmailDAO.sendNow(log, request.getParameter("action"), request.getParameter("leaveID"));
                    
                    sys.setActivity("validate leave (" + request.getParameter("action")+")");
                    sys.setDeptID("");
                    sys.setLeaveID(request.getParameter("leaveID"));
                    sys.setLocID(log.getLocID());
                    sys.setName(log.getFullname());
                    sys.setSessionid(session.getId());
                    sys.setUserid(log.getUserID());
                    LeaveDAO.saveActivity(log, sys);

                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "viewleavemodal":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/view_leave_modal.jsp?leaveID=" + request.getParameter("leaveID");
                    break;
                case "viewleavetab":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/leave_list_approval.jsp?tab=" + request.getParameter("tabid");
                    break;
                case "viewleavemodalcalendar":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/view_leave_modal_calendar.jsp?leaveID=" + request.getParameter("leaveID");
                    break;
                case "updateleavemodal":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/update_leave_modal.jsp?leaveID=" + request.getParameter("leaveID");
                    break;
                case "updateleavemodalrecord":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/update-leave-modal-record.jsp?id=" + request.getParameter("id");
                    break;
                case "updateleaveinfo": {

                    LeaveInfo c = new LeaveInfo();
                    Leave s = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));

                    c.setBf(Integer.parseInt(request.getParameter("bf")));
                    c.setMc(Integer.parseInt(request.getParameter("mc")));
                    c.setEligibleleave(Integer.parseInt(request.getParameter("eligibleleave")));
                    c.setStaffID(s.getStaffID());
                    c.setYear(AccountingPeriod.getCurYearByDate(s.getDatestart()));

                    LeaveDAO.insertLeave(log, c);

                    urlsend = "/conf_car.jsp";
                    break;
                }

                case "updateleaveinforecord": {

                    LeaveInfo c = new LeaveInfo();

                    c.setBf(Integer.parseInt(request.getParameter("bf")));
                    c.setMc(Integer.parseInt(request.getParameter("mc")));
                    c.setEligibleleave(Integer.parseInt(request.getParameter("eligibleleave")));
                    c.setStaffID(request.getParameter("id"));
                    c.setYear(AccountingPeriod.getCurYearByCurrentDate());

                    LeaveDAO.updateLeaveInfo(log, c);

                    urlsend = "/conf_car.jsp";
                    break;
                }
                case "addmodal":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_car_modal_view.jsp?carID=" + request.getParameter("carID");
                    break;
                case "deleteleave":
                    LeaveDAO.deleteLeave(log, request.getParameter("id"));
                    
                    sys.setActivity("delete leave");
                    sys.setDeptID("");
                    sys.setLeaveID(request.getParameter("id"));
                    sys.setLocID(log.getLocID());
                    sys.setName(log.getFullname());
                    sys.setSessionid(session.getId());
                    sys.setUserid(log.getUserID());
                    LeaveDAO.saveActivity(log, sys);
                    break;
                case "cancelleave":

                    LeaveRequest lr = new LeaveRequest();
                    lr.setLeaveID(request.getParameter("leaveID"));
                    lr.setDatetochange("0000-00-00");
                    lr.setRequestto("cancel");

                    LeaveDAO.saveRequest(log, lr);
                    
                    sys.setActivity("request to cancel leave");
                    sys.setDeptID("");
                    sys.setLeaveID(request.getParameter("leaveID"));
                    sys.setLocID(log.getLocID());
                    sys.setName(log.getFullname());
                    sys.setSessionid(session.getId());
                    sys.setUserid(log.getUserID());
                    LeaveDAO.saveActivity(log, sys);
                    
                    break;
                case "leavecomment":
                    LeaveComment lv = new LeaveComment();

                    lv.setComment(request.getParameter("comment"));
                    lv.setLeaveID(request.getParameter("leaveID"));

                    Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));

                    if (!log.getUserID().equals(l.getStaffID())) {

                        Notification n = new Notification();

                        n.setStaffID(l.getStaffID());
                        n.setType("comment");
                        n.setLeaveID(request.getParameter("leaveID"));

                        NotifyDAO.insertNoti(log, n);
                    }
                    LeaveDAO.saveComment(log, lv);
                    EmailDAO.sendCommentEmail(log, l.getLeaveID(), request.getParameter("commentID"));
                    
                    sys.setActivity("comment leave");
                    sys.setDeptID("");
                    sys.setLeaveID(request.getParameter("leaveID"));
                    sys.setLocID(log.getLocID());
                    sys.setName(log.getFullname());
                    sys.setSessionid(session.getId());
                    sys.setUserid(log.getUserID());
                    LeaveDAO.saveActivity(log, sys);
                    
                    break;
                    
                case "transferlocation":

                    StaffDAO.transferLocation(log, request.getParameter("id"),request.getParameter("newloc"));

                    urlsend = "leave_record_list.jsp?id=" + request.getParameter("id");
                    break;
                case "addnewdriver":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_driver_add.jsp";
                    break;
                case "savedriver": {

                    Driver c = new Driver();

                    //c.setDriverID(request.getParameter("driverID"));
                    c.setDriverName(request.getParameter("driverName"));
                    c.setEmail(request.getParameter("email"));
                    c.setStaffID(request.getParameter("staffID"));
                    c.setStatus(request.getParameter("status"));
                    

                    DriverDAO.saveNewDriver(log, c);

                    urlsend = "/conf_driver.jsp";
                    break;
                }
                case "saveexecutive": {

                    Executive e = new Executive();

                    //c.setDriverID(request.getParameter("driverID"));
                    e.setStaffid(request.getParameter("staffid"));
                    e.setNokp(request.getParameter("nokp"));
                    e.setDob(request.getParameter("dob"));
                    e.setBirthplace(request.getParameter("birthplace"));
                    e.setMarital(request.getParameter("marital"));
                    e.setBlood(request.getParameter("blood"));
                    e.setDepartment(request.getParameter("department"));
                    e.setAddress(request.getParameter("address"));
                    e.setFixaddress(request.getParameter("fixaddress"));
                    e.setRace(request.getParameter("race"));
                    e.setReligion(request.getParameter("religion"));
                    e.setTaxno(request.getParameter("taxno"));
                    e.setNoepf(request.getParameter("noepf"));
                    e.setNosocso(request.getParameter("nosocso"));
                    e.setPosition(request.getParameter("position"));
                    e.setWorkdate(request.getParameter("workdate"));
                    e.setSpouse(request.getParameter("spouse"));
                    e.setNokpspouse(request.getParameter("nokpspouse"));
                    e.setSpousework(request.getParameter("spousework"));
                    e.setNamawaris1(request.getParameter("namawaris1"));
                    e.setHubunganwaris1(request.getParameter("hubunganwaris1"));
                    e.setNotelwaris1(request.getParameter("notelwaris1"));
                    e.setAlamatwaris1(request.getParameter("alamatwaris1"));
                    e.setNamawaris2(request.getParameter("namawaris2"));
                    e.setHubunganwaris2(request.getParameter("hubunganwaris2"));
                    e.setAlamatwaris2(request.getParameter("alamatwaris2"));
                    e.setNotelwaris2(request.getParameter("notelwaris2"));
                    e.setBilanak(Integer.parseInt(request.getParameter("bilanak")));
                    
                    StaffDAO.saveInfoExec(log, e);
                   
                    String[] bilanak = request.getParameterValues("bilanak");

                    //String  = "";
                    if (bilanak != null) {
                        for (int i = 0; i < bilanak.length; i++) {
                            
                            ExecutiveChild ec = new ExecutiveChild();
                            
                            ec.setEducationlevel(request.getParameter("educationlevel"));
                            ec.setId(Integer.parseInt(request.getParameter("id")));
                            ec.setNamaanak(request.getParameter("namaanak"));
                            ec.setUmur(Integer.parseInt(request.getParameter("umur")));
                            ec.setNokpanak(request.getParameter("nokpanak"));
                            ec.setOku(request.getParameter("oku"));
                            ec.setStaffid(request.getParameter("staffid"));
                            
                            
                            StaffDAO.saveInfoExecChild(log, ec);
                            
                            //gejala += question1[i] + ",";
                        }
                    }
                   

                    //StaffDAO.saveexecutive(log, e);
                    

                    urlsend = "/conf_driver.jsp";
                    break;
                }
                case "updatedriver": {

                    Driver c = new Driver();

                    c.setDriverID(request.getParameter("driverID"));
                    c.setDriverName(request.getParameter("driverName"));
                    c.setEmail(request.getParameter("email"));
                    c.setStaffID(request.getParameter("staffID"));
                    c.setStatus(request.getParameter("status"));

                    DriverDAO.updateDriver(log, c);

                    urlsend = "/conf_driver.jsp";
                    break;
                }case "changeperson": {

                    

                    StaffDAO.changePerson(log, request.getParameter("staffID"));

                    urlsend = "/index.jsp";
                    break;
                }case "resetviewas": {

                    

                    StaffDAO.resetViewAs(log);

                    urlsend = "/index.jsp";
                    break;
                }
                case "addmodaldriver":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/conf_driver_modal_view.jsp?driverID=" + request.getParameter("driverID");
                    break;
                case "deletedriver":
                    CarDAO.deleteCar(log, request.getParameter("carID"));
                    break;
                case "leavetypelist": {
                   
                   LeaveType lt = new LeaveType();
                   
                   lt.setCode(request.getParameter("code"));
                   lt.setDescp(request.getParameter("descp"));
                   lt.setId(Integer.parseInt(request.getParameter("id")));
                   lt.setName(request.getParameter("name"));
                   lt.setSymbol(request.getParameter("symbol"));
                    
                    break;
                }
                case "deletebooking":
                    BookingDAO.deleteBook(log, request.getParameter("bookID"));
                    break;
                case "setdatefordestination":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/book_set_destination_date.jsp?id=" + request.getParameter("id");
                    break;
                case "viewmodalpassenger":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/staff_list_search_modal.jsp?sessionid=" + request.getParameter("sessionid");
                    break;
                case "viewstafflist":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/staff_list_search_modal.jsp?sessionid=" + request.getParameter("sessionid");
                    break;
                case "viewleavelistmodal":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/view_leave_list_modal.jsp?leaveID=" + request.getParameter("leaveID");
                    break;
                case "savebookmaster":
                    BookingDAO.saveBookingMaster(log, request.getParameter("sessionid"));
                    break;
                case "updatesupervisor":
                    StaffDAO.updateSupervisor(log, request.getParameter("staffID"), request.getParameter("supervisorID"));
                    urlsend = "/hirarki_page.jsp" ;
                    break; 
                case "savemultileave":
                    LeaveDAO.saveMultiLeave(log, request.getParameter("sessionid"));
                    urlsend = "/leave_list_approval.jsp?tab=1";
                    break;  
                default:

                    //urlsend = "/error.jsp?moduleid=" + moduleid + "&refer=" + request.getParameter("referno");
                    break;
            }

            Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PathController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PathController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
