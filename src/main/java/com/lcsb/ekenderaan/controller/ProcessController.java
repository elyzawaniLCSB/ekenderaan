/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.controller;

import com.lcsb.ekenderaan.dao.BookingDAO;
import com.lcsb.ekenderaan.dao.EmailDAO;
import com.lcsb.ekenderaan.dao.LeaveDAO;
import com.lcsb.ekenderaan.dao.NotifyDAO;
import com.lcsb.ekenderaan.dao.StaffDAO;
import com.lcsb.ekenderaan.model.BookingCar;
import com.lcsb.ekenderaan.model.BookingDestinationTemp;
import com.lcsb.ekenderaan.model.Leave;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.Members;
import com.lcsb.ekenderaan.model.SysLogActivity;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "ProcessController", urlPatterns = {"/ProcessController"})
public class ProcessController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String moduleid = request.getParameter("moduleid");
        String process = request.getParameter("process");
        HttpSession session = request.getSession();
        LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
        SysLogActivity sys = new SysLogActivity();

        try {
            switch (process) {
                case "checkDCnote":
                    //out.println(DebitCreditNoteDAO.checkDCNoteExist(log, request.getParameter("referno")));
                    break;
                case "savedestinationselected": {

                    BookingDestinationTemp c = new BookingDestinationTemp();

                    c.setDestcode(request.getParameter("destcode"));
                    c.setDestdescp(request.getParameter("destdescp"));
                    c.setDesttype(request.getParameter("desttype"));
                    c.setUserID(log.getUserID());
                    c.setSessionid(request.getParameter("sessionid"));

                    int i = BookingDAO.insertSelectedDestination(log, c);

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "savedatetime": {

                    BookingDestinationTemp c = new BookingDestinationTemp();

                    c.setEnddate(request.getParameter("enddate"));
                    c.setEndtime(request.getParameter("endtime"));
                    c.setStartdate(request.getParameter("startdate"));
                    c.setStarttime(request.getParameter("starttime"));
                    c.setId(Integer.parseInt(request.getParameter("id")));

                    int i = BookingDAO.updateDateTimeforSelectedDestination(log, c);

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "savepassenger": {

                    Members c = new Members();

                    c.setCompID(request.getParameter("compID"));

                    int i = BookingDAO.insertSelectedPassenger(log, c, request.getParameter("sessionid"));

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "deletedestinationselected": {

                    int i = BookingDAO.deleteSelectedDestination(log, request.getParameter("id"));

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "deletepassengerselected": {

                    int i = BookingDAO.deleteSelectedPassenger(log, request.getParameter("id"));

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                
                case "assigncarfordestination":{
                    
                    BookingCar b = new BookingCar();
                    
                    b.setBookID(request.getParameter("bookID"));
                    b.setCarID(request.getParameter("carID"));
                    b.setDrivetype(request.getParameter("drivetype"));
                    b.setDestID(Integer.parseInt(request.getParameter("destID")));
                    //b.setDriverID(process);

                    int i = BookingDAO.assignCarForDestination(log, b);

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "confirmbook": {

                    int i = BookingDAO.confirmBooking(log, request.getParameter("bookID"));

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                
                case "getLeaveForCalendar": {

                    //JSONObject i = BookingDAO.confirmUndo();

                    out.println(LeaveDAO.getAllLeaveForCalendar(log, request.getParameter("filter")));

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "getAnalysis": {

                    //JSONObject i = BookingDAO.confirmUndo();

                    out.println(LeaveDAO.getAnalysisData(log, request.getParameter("type")));

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "seennotify": {

                    int i = NotifyDAO.updateNotify(log, request.getParameter("id"));

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "checksupervisorforhirarchy": {

                    int i = StaffDAO.isSupervisorExist(log);

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "savemultileavetemp": {

                    String datestart = request.getParameter("datestart");
                    String dateend = request.getParameter("dateend");

                    datestart = datestart.substring(6, 10) + "-" + datestart.substring(3, 5) + "-" + datestart.substring(0, 2);
                    dateend = dateend.substring(6, 10) + "-" + dateend.substring(3, 5) + "-" + dateend.substring(0, 2);

                    Leave c = new Leave();

                    c.setDateapply(request.getParameter("dateapply"));
                    c.setDateend(dateend);
                    c.setDatestart(datestart);
                    c.setDays(Integer.parseInt(request.getParameter("days")));

                    if (request.getParameter("type").equals("C01")) {
                        if (request.getParameter("reason").equals("Lain-lain")) {
                            c.setReason(request.getParameter("reason-other"));
                        } else {
                            c.setReason(request.getParameter("reason"));
                        }
                    }else if (request.getParameter("type").equals("C05")){
                        c.setReason(request.getParameter("reason"));
                    }

                    c.setType(request.getParameter("type"));

                    String leaveID = LeaveDAO.saveMultiLeaveTemp(log, c, request.getParameter("sessionid"));


                    out.println(leaveID);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                case "deletetempmultileave": {

                    int i = LeaveDAO.deleteMultiLeave(log, request.getParameter("id"));

                    out.println(i);

                    //urlsend = "/conf_car.jsp";
                    break;
                }
                default:
                    break;
            }
        } finally {
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
