package com.lcsb.ekenderaan.controller;

import javax.servlet.annotation.WebServlet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.lcsb.ekenderaan.controller.Constants.*;
import com.lcsb.ekenderaan.dao.LeaveDAO;
import com.lcsb.ekenderaan.model.LoginProfile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;

@WebServlet(
        name = "UploadServlet",
        urlPatterns = {"/UploadServlet"}
)
public class UploadServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

        if (ServletFileUpload.isMultipartContent(request)) {

            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(MEMORY_THRESHOLD);
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setFileSizeMax(MAX_FILE_SIZE);
            upload.setSizeMax(MAX_REQUEST_SIZE);
            String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            try {
                List<FileItem> formItems = upload.parseRequest(request);

                if (formItems != null && formItems.size() > 0) {
                    for (FileItem item : formItems) {
                        if (!item.isFormField()) {

                            String name = new File(item.getName()).getName();

                            String ext = FilenameUtils.getExtension(name);
                            
                            

                            name = request.getParameter("sessionid") + "." + ext;

                            String fileName = request.getParameter("sessionid")+"."+ext;
                            String filePath = uploadPath + File.separator + fileName;
                            File storeFile = new File(filePath);
                            item.write(storeFile);
                            request.setAttribute("message", "File " + fileName + " has uploaded successfully!");
                            Logger.getLogger(PathController.class.getName()).log(Level.INFO, "File " + fileName + " has uploaded successfully!");
                            LeaveDAO.saveLeaveAttachment(log, fileName, request.getParameter("sessionid"));

                        }
                    }
                }
            } catch (Exception ex) {
                request.setAttribute("message", "There was an error: " + ex.getMessage());
                Logger.getLogger(PathController.class.getName()).log(Level.INFO, "There was an error: " + ex.getMessage());
            }
            //getServletContext().getRequestDispatcher("/result.jsp").forward(request, response);
        }
    }
}
