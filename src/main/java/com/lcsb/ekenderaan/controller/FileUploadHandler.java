/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.controller;

import com.lcsb.ekenderaan.dao.AccountingPeriod;
import com.lcsb.ekenderaan.dao.LeaveDAO;
import com.lcsb.ekenderaan.dao.StaffDAO;
import com.lcsb.ekenderaan.model.LoginProfile;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "FileUploadHandler", urlPatterns = {"/FileUploadHandler"})



public class FileUploadHandler extends HttpServlet {
    
    //private final String UPLOAD_DIRECTORY = "/Users/fadhilfahmi/PadelLab/smartbooking/eleave/src/main/webapp/uploaded_attachment";
    //private final String UPLOAD_DIRECTORY = "C:/Program Files/Apache Software Foundation/Tomcat 8.0/webapps/eleave/uploaded_attachment";
    private final String UPLOAD_DIRECTORY = "C:\\Users\\Administrator\\Desktop";
    
    //private final String UPLOAD_DIRECTORY = getServletContext().getRealPath( "/uploaded_attachment" );
    //String path = getServletContext().getRealPath( "/uploadedFiles" );
    
    //private final String UPLOAD_DIRECTORY = getServletContext().getRealPath( "/" )+"uploadedFiles";
    
    

   

    @Override

    protected void doPost(HttpServletRequest request, HttpServletResponse response)

            throws ServletException, IOException {
        
        Logger.getLogger(FileUploadHandler.class.getName()).log(Level.INFO, "---in"+UPLOAD_DIRECTORY);

       //out.println( "context = " + getServletContext().getRealPath( "/" ) );
        //process only if its multipart content
        HttpSession session = request.getSession();
        LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

        if(ServletFileUpload.isMultipartContent(request)){

            try {

                List<FileItem> multiparts = new ServletFileUpload(
                new DiskFileItemFactory()).parseRequest(request);

                for(FileItem item : multiparts){

                    if(!item.isFormField()){

                        //String name = new File(item.getName()).getName();
                        String name = new File(item.getName()).getName();
                        
                        String ext = FilenameUtils.getExtension(name);
                        
                        name = request.getParameter("sessionid")+"."+ext;
                        item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
                        
                        Logger.getLogger(FileUploadHandler.class.getName()).log(Level.INFO, "---in"+UPLOAD_DIRECTORY + File.separator + name);
                        
                        LeaveDAO.saveLeaveAttachment(log, name, request.getParameter("sessionid"));

                    }

                }

            

               //File uploaded successfully
               
               System.out.print("Hello World");

               

            } catch (Exception ex) {
                System.out.print("Hello World");


            }         

          

        }else{

            request.setAttribute("message","Sorry this Servlet only handles file upload request");

        }

     

        //request.getRequestDispatcher("/result.jsp").forward(request, response);

      

    }

    
}
