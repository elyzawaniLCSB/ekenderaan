/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author noorelyzawaniazizan
 */
@Entity
@Table(name = "executive_child")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExecutiveChild.findAll", query = "SELECT e FROM ExecutiveChild e"),
    @NamedQuery(name = "ExecutiveChild.findById", query = "SELECT e FROM ExecutiveChild e WHERE e.id = :id"),
    @NamedQuery(name = "ExecutiveChild.findByStaffid", query = "SELECT e FROM ExecutiveChild e WHERE e.staffid = :staffid"),
    @NamedQuery(name = "ExecutiveChild.findByNamaanak", query = "SELECT e FROM ExecutiveChild e WHERE e.namaanak = :namaanak"),
    @NamedQuery(name = "ExecutiveChild.findByNokpanak", query = "SELECT e FROM ExecutiveChild e WHERE e.nokpanak = :nokpanak"),
    @NamedQuery(name = "ExecutiveChild.findByUmur", query = "SELECT e FROM ExecutiveChild e WHERE e.umur = :umur"),
    @NamedQuery(name = "ExecutiveChild.findByOku", query = "SELECT e FROM ExecutiveChild e WHERE e.oku = :oku"),
    @NamedQuery(name = "ExecutiveChild.findByEducationlevel", query = "SELECT e FROM ExecutiveChild e WHERE e.educationlevel = :educationlevel")})
public class ExecutiveChild implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "staffid")
    private String staffid;
    @Size(max = 100)
    @Column(name = "namaanak")
    private String namaanak;
    @Size(max = 100)
    @Column(name = "nokpanak")
    private String nokpanak;
    @Column(name = "umur")
    private Integer umur;
    @Size(max = 10)
    @Column(name = "oku")
    private String oku;
    @Size(max = 10)
    @Column(name = "educationlevel")
    private String educationlevel;

    public ExecutiveChild() {
    }

    public ExecutiveChild(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getNamaanak() {
        return namaanak;
    }

    public void setNamaanak(String namaanak) {
        this.namaanak = namaanak;
    }

    public String getNokpanak() {
        return nokpanak;
    }

    public void setNokpanak(String nokpanak) {
        this.nokpanak = nokpanak;
    }

    public Integer getUmur() {
        return umur;
    }

    public void setUmur(Integer umur) {
        this.umur = umur;
    }

    public String getOku() {
        return oku;
    }

    public void setOku(String oku) {
        this.oku = oku;
    }

    public String getEducationlevel() {
        return educationlevel;
    }

    public void setEducationlevel(String educationlevel) {
        this.educationlevel = educationlevel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExecutiveChild)) {
            return false;
        }
        ExecutiveChild other = (ExecutiveChild) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.ExecutiveChild[ id=" + id + " ]";
    }
    
}
