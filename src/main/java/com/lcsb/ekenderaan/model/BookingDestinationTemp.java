/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "booking_destination_temp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookingDestinationTemp.findAll", query = "SELECT b FROM BookingDestinationTemp b"),
    @NamedQuery(name = "BookingDestinationTemp.findById", query = "SELECT b FROM BookingDestinationTemp b WHERE b.id = :id"),
    @NamedQuery(name = "BookingDestinationTemp.findBySessionid", query = "SELECT b FROM BookingDestinationTemp b WHERE b.sessionid = :sessionid"),
    @NamedQuery(name = "BookingDestinationTemp.findByDesttype", query = "SELECT b FROM BookingDestinationTemp b WHERE b.desttype = :desttype"),
    @NamedQuery(name = "BookingDestinationTemp.findByDestcode", query = "SELECT b FROM BookingDestinationTemp b WHERE b.destcode = :destcode"),
    @NamedQuery(name = "BookingDestinationTemp.findByDestdescp", query = "SELECT b FROM BookingDestinationTemp b WHERE b.destdescp = :destdescp"),
    @NamedQuery(name = "BookingDestinationTemp.findByUserID", query = "SELECT b FROM BookingDestinationTemp b WHERE b.userID = :userID"),
    @NamedQuery(name = "BookingDestinationTemp.findByStartdate", query = "SELECT b FROM BookingDestinationTemp b WHERE b.startdate = :startdate"),
    @NamedQuery(name = "BookingDestinationTemp.findByEnddate", query = "SELECT b FROM BookingDestinationTemp b WHERE b.enddate = :enddate"),
    @NamedQuery(name = "BookingDestinationTemp.findByStarttime", query = "SELECT b FROM BookingDestinationTemp b WHERE b.starttime = :starttime"),
    @NamedQuery(name = "BookingDestinationTemp.findByEndtime", query = "SELECT b FROM BookingDestinationTemp b WHERE b.endtime = :endtime")})
public class BookingDestinationTemp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "sessionid")
    private String sessionid;
    @Size(max = 20)
    @Column(name = "desttype")
    private String desttype;
    @Size(max = 10)
    @Column(name = "destcode")
    private String destcode;
    @Size(max = 200)
    @Column(name = "destdescp")
    private String destdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "userID")
    private String userID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "startdate")
    private String startdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "enddate")
    private String enddate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "starttime")
    private String starttime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "endtime")
    private String endtime;

    public BookingDestinationTemp() {
    }

    public BookingDestinationTemp(Integer id) {
        this.id = id;
    }

    public BookingDestinationTemp(Integer id, String sessionid, String userID, String startdate, String enddate, String starttime, String endtime) {
        this.id = id;
        this.sessionid = sessionid;
        this.userID = userID;
        this.startdate = startdate;
        this.enddate = enddate;
        this.starttime = starttime;
        this.endtime = endtime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getDesttype() {
        return desttype;
    }

    public void setDesttype(String desttype) {
        this.desttype = desttype;
    }

    public String getDestcode() {
        return destcode;
    }

    public void setDestcode(String destcode) {
        this.destcode = destcode;
    }

    public String getDestdescp() {
        return destdescp;
    }

    public void setDestdescp(String destdescp) {
        this.destdescp = destdescp;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingDestinationTemp)) {
            return false;
        }
        BookingDestinationTemp other = (BookingDestinationTemp) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.smartbooking.model.BookingDestinationTemp[ id=" + id + " ]";
    }
    
}
