/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "leave_master")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Leave.findAll", query = "SELECT l FROM Leave l"),
    @NamedQuery(name = "Leave.findByLeaveID", query = "SELECT l FROM Leave l WHERE l.leaveID = :leaveID"),
    @NamedQuery(name = "Leave.findByStaffID", query = "SELECT l FROM Leave l WHERE l.staffID = :staffID"),
    @NamedQuery(name = "Leave.findByType", query = "SELECT l FROM Leave l WHERE l.type = :type"),
    @NamedQuery(name = "Leave.findByReason", query = "SELECT l FROM Leave l WHERE l.reason = :reason"),
    @NamedQuery(name = "Leave.findByDays", query = "SELECT l FROM Leave l WHERE l.days = :days"),
    @NamedQuery(name = "Leave.findByDateapply", query = "SELECT l FROM Leave l WHERE l.dateapply = :dateapply"),
    @NamedQuery(name = "Leave.findByDatestart", query = "SELECT l FROM Leave l WHERE l.datestart = :datestart"),
    @NamedQuery(name = "Leave.findByDateend", query = "SELECT l FROM Leave l WHERE l.dateend = :dateend"),
    @NamedQuery(name = "Leave.findBySupervisorID", query = "SELECT l FROM Leave l WHERE l.supervisorID = :supervisorID"),
    @NamedQuery(name = "Leave.findBySupervisorDate", query = "SELECT l FROM Leave l WHERE l.supervisorDate = :supervisorDate"),
    @NamedQuery(name = "Leave.findByHeadID", query = "SELECT l FROM Leave l WHERE l.headID = :headID"),
    @NamedQuery(name = "Leave.findByHeadDate", query = "SELECT l FROM Leave l WHERE l.headDate = :headDate"),
    @NamedQuery(name = "Leave.findByHrID", query = "SELECT l FROM Leave l WHERE l.hrID = :hrID"),
    @NamedQuery(name = "Leave.findByHrDate", query = "SELECT l FROM Leave l WHERE l.hrDate = :hrDate"),
    @NamedQuery(name = "Leave.findByCheckID", query = "SELECT l FROM Leave l WHERE l.checkID = :checkID"),
    @NamedQuery(name = "Leave.findByCheckDate", query = "SELECT l FROM Leave l WHERE l.checkDate = :checkDate"),
    @NamedQuery(name = "Leave.findByStatus", query = "SELECT l FROM Leave l WHERE l.status = :status"),
    @NamedQuery(name = "Leave.findByYear", query = "SELECT l FROM Leave l WHERE l.year = :year"),
    @NamedQuery(name = "Leave.findByPeriod", query = "SELECT l FROM Leave l WHERE l.period = :period"),
    @NamedQuery(name = "Leave.findByStafflevel", query = "SELECT l FROM Leave l WHERE l.stafflevel = :stafflevel"),
    @NamedQuery(name = "Leave.findByTimeapply", query = "SELECT l FROM Leave l WHERE l.timeapply = :timeapply"),
    @NamedQuery(name = "Leave.findByChecktime", query = "SELECT l FROM Leave l WHERE l.checktime = :checktime"),
    @NamedQuery(name = "Leave.findBySpvtime", query = "SELECT l FROM Leave l WHERE l.spvtime = :spvtime"),
    @NamedQuery(name = "Leave.findByHeadtime", query = "SELECT l FROM Leave l WHERE l.headtime = :headtime")})
public class Leave implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "leaveID")
    private String leaveID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "staffID")
    private String staffID;
    @Size(max = 20)
    @Column(name = "type")
    private String type;
    @Size(max = 300)
    @Column(name = "reason")
    private String reason;
    @Column(name = "days")
    private Integer days;
    @Size(max = 10)
    @Column(name = "dateapply")
    private String dateapply;
    @Size(max = 10)
    @Column(name = "datestart")
    private String datestart;
    @Size(max = 10)
    @Column(name = "dateend")
    private String dateend;
    @Size(max = 19)
    @Column(name = "supervisorID")
    private String supervisorID;
    @Size(max = 10)
    @Column(name = "supervisor_date")
    private String supervisorDate;
    @Size(max = 10)
    @Column(name = "headID")
    private String headID;
    @Size(max = 10)
    @Column(name = "head_date")
    private String headDate;
    @Size(max = 10)
    @Column(name = "hrID")
    private String hrID;
    @Size(max = 10)
    @Column(name = "hr_date")
    private String hrDate;
    @Size(max = 10)
    @Column(name = "checkID")
    private String checkID;
    @Size(max = 10)
    @Column(name = "check_date")
    private String checkDate;
    @Size(max = 30)
    @Column(name = "status")
    private String status;
    @Size(max = 4)
    @Column(name = "year")
    private String year;
    @Size(max = 2)
    @Column(name = "period")
    private String period;
    @Column(name = "stafflevel")
    private Integer stafflevel;
    @Size(max = 8)
    @Column(name = "timeapply")
    private String timeapply;
    @Size(max = 8)
    @Column(name = "checktime")
    private String checktime;
    @Size(max = 8)
    @Column(name = "spvtime")
    private String spvtime;
    @Size(max = 8)
    @Column(name = "headtime")
    private String headtime;

    public Leave() {
    }

    public Leave(String leaveID) {
        this.leaveID = leaveID;
    }

    public Leave(String leaveID, String staffID) {
        this.leaveID = leaveID;
        this.staffID = staffID;
    }

    public String getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(String leaveID) {
        this.leaveID = leaveID;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public String getDateapply() {
        return dateapply;
    }

    public void setDateapply(String dateapply) {
        this.dateapply = dateapply;
    }

    public String getDatestart() {
        return datestart;
    }

    public void setDatestart(String datestart) {
        this.datestart = datestart;
    }

    public String getDateend() {
        return dateend;
    }

    public void setDateend(String dateend) {
        this.dateend = dateend;
    }

    public String getSupervisorID() {
        return supervisorID;
    }

    public void setSupervisorID(String supervisorID) {
        this.supervisorID = supervisorID;
    }

    public String getSupervisorDate() {
        return supervisorDate;
    }

    public void setSupervisorDate(String supervisorDate) {
        this.supervisorDate = supervisorDate;
    }

    public String getHeadID() {
        return headID;
    }

    public void setHeadID(String headID) {
        this.headID = headID;
    }

    public String getHeadDate() {
        return headDate;
    }

    public void setHeadDate(String headDate) {
        this.headDate = headDate;
    }

    public String getHrID() {
        return hrID;
    }

    public void setHrID(String hrID) {
        this.hrID = hrID;
    }

    public String getHrDate() {
        return hrDate;
    }

    public void setHrDate(String hrDate) {
        this.hrDate = hrDate;
    }

    public String getCheckID() {
        return checkID;
    }

    public void setCheckID(String checkID) {
        this.checkID = checkID;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Integer getStafflevel() {
        return stafflevel;
    }

    public void setStafflevel(Integer stafflevel) {
        this.stafflevel = stafflevel;
    }

    public String getTimeapply() {
        return timeapply;
    }

    public void setTimeapply(String timeapply) {
        this.timeapply = timeapply;
    }

    public String getChecktime() {
        return checktime;
    }

    public void setChecktime(String checktime) {
        this.checktime = checktime;
    }

    public String getSpvtime() {
        return spvtime;
    }

    public void setSpvtime(String spvtime) {
        this.spvtime = spvtime;
    }

    public String getHeadtime() {
        return headtime;
    }

    public void setHeadtime(String headtime) {
        this.headtime = headtime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leaveID != null ? leaveID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Leave)) {
            return false;
        }
        Leave other = (Leave) object;
        if ((this.leaveID == null && other.leaveID != null) || (this.leaveID != null && !this.leaveID.equals(other.leaveID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.Leave[ leaveID=" + leaveID + " ]";
    }
    
}
