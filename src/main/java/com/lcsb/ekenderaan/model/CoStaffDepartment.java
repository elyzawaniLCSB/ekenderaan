/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff_department")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CoStaffDepartment.findAll", query = "SELECT c FROM CoStaffDepartment c"),
    @NamedQuery(name = "CoStaffDepartment.findById", query = "SELECT c FROM CoStaffDepartment c WHERE c.id = :id"),
    @NamedQuery(name = "CoStaffDepartment.findByDescp", query = "SELECT c FROM CoStaffDepartment c WHERE c.descp = :descp"),
    @NamedQuery(name = "CoStaffDepartment.findByHeadID", query = "SELECT c FROM CoStaffDepartment c WHERE c.headID = :headID")})
public class CoStaffDepartment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "id")
    private String id;
    @Size(max = 100)
    @Column(name = "descp")
    private String descp;
    @Size(max = 10)
    @Column(name = "headID")
    private String headID;
    private String locID;

    public String getLocID() {
        return locID;
    }

    public void setLocID(String locID) {
        this.locID = locID;
    }

    public CoStaffDepartment() {
    }

    public CoStaffDepartment(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getHeadID() {
        return headID;
    }

    public void setHeadID(String headID) {
        this.headID = headID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CoStaffDepartment)) {
            return false;
        }
        CoStaffDepartment other = (CoStaffDepartment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.CoStaffDepartment[ id=" + id + " ]";
    }
    
}
