/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sys_log_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SysLogActivity.findAll", query = "SELECT s FROM SysLogActivity s"),
    @NamedQuery(name = "SysLogActivity.findById", query = "SELECT s FROM SysLogActivity s WHERE s.id = :id"),
    @NamedQuery(name = "SysLogActivity.findBySessionid", query = "SELECT s FROM SysLogActivity s WHERE s.sessionid = :sessionid"),
    @NamedQuery(name = "SysLogActivity.findByName", query = "SELECT s FROM SysLogActivity s WHERE s.name = :name"),
    @NamedQuery(name = "SysLogActivity.findByUserid", query = "SELECT s FROM SysLogActivity s WHERE s.userid = :userid"),
    @NamedQuery(name = "SysLogActivity.findByDate", query = "SELECT s FROM SysLogActivity s WHERE s.date = :date"),
    @NamedQuery(name = "SysLogActivity.findByTime", query = "SELECT s FROM SysLogActivity s WHERE s.time = :time"),
    @NamedQuery(name = "SysLogActivity.findByLocID", query = "SELECT s FROM SysLogActivity s WHERE s.locID = :locID"),
    @NamedQuery(name = "SysLogActivity.findByLocName", query = "SELECT s FROM SysLogActivity s WHERE s.locName = :locName"),
    @NamedQuery(name = "SysLogActivity.findByDeptID", query = "SELECT s FROM SysLogActivity s WHERE s.deptID = :deptID"),
    @NamedQuery(name = "SysLogActivity.findByDeptName", query = "SELECT s FROM SysLogActivity s WHERE s.deptName = :deptName"),
    @NamedQuery(name = "SysLogActivity.findByActivity", query = "SELECT s FROM SysLogActivity s WHERE s.activity = :activity"),
    @NamedQuery(name = "SysLogActivity.findByLeaveID", query = "SELECT s FROM SysLogActivity s WHERE s.leaveID = :leaveID")})
public class SysLogActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sessionid")
    private String sessionid;
    @Size(max = 200)
    @Column(name = "name")
    private String name;
    @Size(max = 6)
    @Column(name = "userid")
    private String userid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "time")
    private String time;
    @Size(max = 4)
    @Column(name = "locID")
    private String locID;
    @Size(max = 200)
    @Column(name = "locName")
    private String locName;
    @Size(max = 4)
    @Column(name = "deptID")
    private String deptID;
    @Size(max = 200)
    @Column(name = "deptName")
    private String deptName;
    @Size(max = 200)
    @Column(name = "activity")
    private String activity;
    @Size(max = 10)
    @Column(name = "leaveID")
    private String leaveID;

    public SysLogActivity() {
    }

    public SysLogActivity(Integer id) {
        this.id = id;
    }

    public SysLogActivity(Integer id, String sessionid, String date, String time) {
        this.id = id;
        this.sessionid = sessionid;
        this.date = date;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocID() {
        return locID;
    }

    public void setLocID(String locID) {
        this.locID = locID;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public String getDeptID() {
        return deptID;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(String leaveID) {
        this.leaveID = leaveID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysLogActivity)) {
            return false;
        }
        SysLogActivity other = (SysLogActivity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.SysLogActivity[ id=" + id + " ]";
    }
    
}
