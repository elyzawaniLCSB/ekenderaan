/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "executive")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Executive.findAll", query = "SELECT e FROM Executive e"),
    @NamedQuery(name = "Executive.findById", query = "SELECT e FROM Executive e WHERE e.id = :id"),
    @NamedQuery(name = "Executive.findByDob", query = "SELECT e FROM Executive e WHERE e.dob = :dob"),
    @NamedQuery(name = "Executive.findByNokp", query = "SELECT e FROM Executive e WHERE e.nokp = :nokp"),
    @NamedQuery(name = "Executive.findByBirthplace", query = "SELECT e FROM Executive e WHERE e.birthplace = :birthplace"),
    @NamedQuery(name = "Executive.findByMarital", query = "SELECT e FROM Executive e WHERE e.marital = :marital"),
    @NamedQuery(name = "Executive.findByBlood", query = "SELECT e FROM Executive e WHERE e.blood = :blood"),
    @NamedQuery(name = "Executive.findByDepartment", query = "SELECT e FROM Executive e WHERE e.department = :department"),
    @NamedQuery(name = "Executive.findByRace", query = "SELECT e FROM Executive e WHERE e.race = :race"),
    @NamedQuery(name = "Executive.findByReligion", query = "SELECT e FROM Executive e WHERE e.religion = :religion"),
    @NamedQuery(name = "Executive.findByTaxno", query = "SELECT e FROM Executive e WHERE e.taxno = :taxno"),
    @NamedQuery(name = "Executive.findByNoepf", query = "SELECT e FROM Executive e WHERE e.noepf = :noepf"),
    @NamedQuery(name = "Executive.findByNosocso", query = "SELECT e FROM Executive e WHERE e.nosocso = :nosocso"),
    @NamedQuery(name = "Executive.findByPosition", query = "SELECT e FROM Executive e WHERE e.position = :position"),
    @NamedQuery(name = "Executive.findByWorkdate", query = "SELECT e FROM Executive e WHERE e.workdate = :workdate"),
    @NamedQuery(name = "Executive.findBySpouse", query = "SELECT e FROM Executive e WHERE e.spouse = :spouse"),
    @NamedQuery(name = "Executive.findByNokpspouse", query = "SELECT e FROM Executive e WHERE e.nokpspouse = :nokpspouse"),
    @NamedQuery(name = "Executive.findBySpousework", query = "SELECT e FROM Executive e WHERE e.spousework = :spousework"),
    @NamedQuery(name = "Executive.findByNamawaris1", query = "SELECT e FROM Executive e WHERE e.namawaris1 = :namawaris1"),
    @NamedQuery(name = "Executive.findByHubunganwaris1", query = "SELECT e FROM Executive e WHERE e.hubunganwaris1 = :hubunganwaris1"),
    @NamedQuery(name = "Executive.findByNotelwaris1", query = "SELECT e FROM Executive e WHERE e.notelwaris1 = :notelwaris1"),
    @NamedQuery(name = "Executive.findByNamawaris2", query = "SELECT e FROM Executive e WHERE e.namawaris2 = :namawaris2"),
    @NamedQuery(name = "Executive.findByHubunganwaris2", query = "SELECT e FROM Executive e WHERE e.hubunganwaris2 = :hubunganwaris2"),
    @NamedQuery(name = "Executive.findByNotelwaris2", query = "SELECT e FROM Executive e WHERE e.notelwaris2 = :notelwaris2"),
    @NamedQuery(name = "Executive.findByBilanak", query = "SELECT e FROM Executive e WHERE e.bilanak = :bilanak"),
    @NamedQuery(name = "Executive.findByAnakover18", query = "SELECT e FROM Executive e WHERE e.anakover18 = :anakover18"),
    @NamedQuery(name = "Executive.findByAnakbelow18", query = "SELECT e FROM Executive e WHERE e.anakbelow18 = :anakbelow18"),
    @NamedQuery(name = "Executive.findByAnakipt18", query = "SELECT e FROM Executive e WHERE e.anakipt18 = :anakipt18"),
    @NamedQuery(name = "Executive.findByAnakoku", query = "SELECT e FROM Executive e WHERE e.anakoku = :anakoku"),
    @NamedQuery(name = "Executive.findByAnakokuipt", query = "SELECT e FROM Executive e WHERE e.anakokuipt = :anakokuipt"),
    @NamedQuery(name = "Executive.findByStaffid", query = "SELECT e FROM Executive e WHERE e.staffid = :staffid"),
    @NamedQuery(name = "Executive.findByEducationlevel", query = "SELECT e FROM Executive e WHERE e.educationlevel = :educationlevel")})
public class Executive implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 10)
    @Column(name = "dob")
    private String dob;
    @Size(max = 100)
    @Column(name = "nokp")
    private String nokp;
    @Size(max = 100)
    @Column(name = "birthplace")
    private String birthplace;
    @Size(max = 100)
    @Column(name = "marital")
    private String marital;
    @Size(max = 100)
    @Column(name = "blood")
    private String blood;
    @Size(max = 100)
    @Column(name = "department")
    private String department;
    @Lob
    @Size(max = 65535)
    @Column(name = "address")
    private String address;
    @Lob
    @Size(max = 65535)
    @Column(name = "fixaddress")
    private String fixaddress;
    @Size(max = 100)
    @Column(name = "race")
    private String race;
    @Size(max = 100)
    @Column(name = "religion")
    private String religion;
    @Size(max = 100)
    @Column(name = "taxno")
    private String taxno;
    @Size(max = 100)
    @Column(name = "noepf")
    private String noepf;
    @Size(max = 100)
    @Column(name = "nosocso")
    private String nosocso;
    @Size(max = 100)
    @Column(name = "position")
    private String position;
    @Size(max = 10)
    @Column(name = "workdate")
    private String workdate;
    @Size(max = 100)
    @Column(name = "spouse")
    private String spouse;
    @Size(max = 100)
    @Column(name = "nokpspouse")
    private String nokpspouse;
    @Size(max = 100)
    @Column(name = "spousework")
    private String spousework;
    @Size(max = 100)
    @Column(name = "namawaris1")
    private String namawaris1;
    @Size(max = 100)
    @Column(name = "hubunganwaris1")
    private String hubunganwaris1;
    @Size(max = 100)
    @Column(name = "notelwaris1")
    private String notelwaris1;
    @Lob
    @Size(max = 65535)
    @Column(name = "alamatwaris1")
    private String alamatwaris1;
    @Size(max = 100)
    @Column(name = "namawaris2")
    private String namawaris2;
    @Size(max = 100)
    @Column(name = "hubunganwaris2")
    private String hubunganwaris2;
    @Size(max = 100)
    @Column(name = "notelwaris2")
    private String notelwaris2;
    @Lob
    @Size(max = 65535)
    @Column(name = "alamatwaris2")
    private String alamatwaris2;
    @Column(name = "bilanak")
    private Integer bilanak;
    @Column(name = "anakover18")
    private Integer anakover18;
    @Column(name = "anakbelow18")
    private Integer anakbelow18;
    @Column(name = "anakipt18")
    private Integer anakipt18;
    @Column(name = "anakoku")
    private Integer anakoku;
    @Column(name = "anakokuipt")
    private Integer anakokuipt;
    @Size(max = 100)
    @Column(name = "staffid")
    private String staffid;
    @Size(max = 50)
    @Column(name = "educationlevel")
    private String educationlevel;

    public Executive() {
    }

    public Executive(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNokp() {
        return nokp;
    }

    public void setNokp(String nokp) {
        this.nokp = nokp;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFixaddress() {
        return fixaddress;
    }

    public void setFixaddress(String fixaddress) {
        this.fixaddress = fixaddress;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getTaxno() {
        return taxno;
    }

    public void setTaxno(String taxno) {
        this.taxno = taxno;
    }

    public String getNoepf() {
        return noepf;
    }

    public void setNoepf(String noepf) {
        this.noepf = noepf;
    }

    public String getNosocso() {
        return nosocso;
    }

    public void setNosocso(String nosocso) {
        this.nosocso = nosocso;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getWorkdate() {
        return workdate;
    }

    public void setWorkdate(String workdate) {
        this.workdate = workdate;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getNokpspouse() {
        return nokpspouse;
    }

    public void setNokpspouse(String nokpspouse) {
        this.nokpspouse = nokpspouse;
    }

    public String getSpousework() {
        return spousework;
    }

    public void setSpousework(String spousework) {
        this.spousework = spousework;
    }

    public String getNamawaris1() {
        return namawaris1;
    }

    public void setNamawaris1(String namawaris1) {
        this.namawaris1 = namawaris1;
    }

    public String getHubunganwaris1() {
        return hubunganwaris1;
    }

    public void setHubunganwaris1(String hubunganwaris1) {
        this.hubunganwaris1 = hubunganwaris1;
    }

    public String getNotelwaris1() {
        return notelwaris1;
    }

    public void setNotelwaris1(String notelwaris1) {
        this.notelwaris1 = notelwaris1;
    }

    public String getAlamatwaris1() {
        return alamatwaris1;
    }

    public void setAlamatwaris1(String alamatwaris1) {
        this.alamatwaris1 = alamatwaris1;
    }

    public String getNamawaris2() {
        return namawaris2;
    }

    public void setNamawaris2(String namawaris2) {
        this.namawaris2 = namawaris2;
    }

    public String getHubunganwaris2() {
        return hubunganwaris2;
    }

    public void setHubunganwaris2(String hubunganwaris2) {
        this.hubunganwaris2 = hubunganwaris2;
    }

    public String getNotelwaris2() {
        return notelwaris2;
    }

    public void setNotelwaris2(String notelwaris2) {
        this.notelwaris2 = notelwaris2;
    }

    public String getAlamatwaris2() {
        return alamatwaris2;
    }

    public void setAlamatwaris2(String alamatwaris2) {
        this.alamatwaris2 = alamatwaris2;
    }

    public Integer getBilanak() {
        return bilanak;
    }

    public void setBilanak(Integer bilanak) {
        this.bilanak = bilanak;
    }

    public Integer getAnakover18() {
        return anakover18;
    }

    public void setAnakover18(Integer anakover18) {
        this.anakover18 = anakover18;
    }

    public Integer getAnakbelow18() {
        return anakbelow18;
    }

    public void setAnakbelow18(Integer anakbelow18) {
        this.anakbelow18 = anakbelow18;
    }

    public Integer getAnakipt18() {
        return anakipt18;
    }

    public void setAnakipt18(Integer anakipt18) {
        this.anakipt18 = anakipt18;
    }

    public Integer getAnakoku() {
        return anakoku;
    }

    public void setAnakoku(Integer anakoku) {
        this.anakoku = anakoku;
    }

    public Integer getAnakokuipt() {
        return anakokuipt;
    }

    public void setAnakokuipt(Integer anakokuipt) {
        this.anakokuipt = anakokuipt;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getEducationlevel() {
        return educationlevel;
    }

    public void setEducationlevel(String educationlevel) {
        this.educationlevel = educationlevel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Executive)) {
            return false;
        }
        Executive other = (Executive) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Executive[ id=" + id + " ]";
    }
    
}
