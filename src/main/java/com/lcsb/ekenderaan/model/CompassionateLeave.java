/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author noorelyzawaniazizan
 */
@Entity
@Table(name = "compassionate_leave")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompassionateLeave.findAll", query = "SELECT c FROM CompassionateLeave c"),
    @NamedQuery(name = "CompassionateLeave.findById", query = "SELECT c FROM CompassionateLeave c WHERE c.id = :id"),
    @NamedQuery(name = "CompassionateLeave.findByLeaveID", query = "SELECT c FROM CompassionateLeave c WHERE c.leaveID = :leaveID"),
    @NamedQuery(name = "CompassionateLeave.findByType", query = "SELECT c FROM CompassionateLeave c WHERE c.type = :type"),
    @NamedQuery(name = "CompassionateLeave.findByBil", query = "SELECT c FROM CompassionateLeave c WHERE c.bil = :bil"),
    @NamedQuery(name = "CompassionateLeave.findByRemark", query = "SELECT c FROM CompassionateLeave c WHERE c.remark = :remark"),
    @NamedQuery(name = "CompassionateLeave.findByActive", query = "SELECT c FROM CompassionateLeave c WHERE c.active = :active")})
public class CompassionateLeave implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "leaveID")
    private String leaveID;
    @Size(max = 100)
    @Column(name = "type")
    private String type;
    @Size(max = 100)
    @Column(name = "bil")
    private String bil;
    @Size(max = 1000)
    @Column(name = "remark")
    private String remark;
    @Column(name = "active")
    private Integer active;

    public CompassionateLeave() {
    }

    public CompassionateLeave(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(String leaveID) {
        this.leaveID = leaveID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBil() {
        return bil;
    }

    public void setBil(String bil) {
        this.bil = bil;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompassionateLeave)) {
            return false;
        }
        CompassionateLeave other = (CompassionateLeave) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.CompassionateLeave[ id=" + id + " ]";
    }
    
}
