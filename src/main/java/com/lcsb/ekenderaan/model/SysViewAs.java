/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sys_view_as")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SysViewAs.findAll", query = "SELECT s FROM SysViewAs s"),
    @NamedQuery(name = "SysViewAs.findById", query = "SELECT s FROM SysViewAs s WHERE s.id = :id"),
    @NamedQuery(name = "SysViewAs.findByStaffID", query = "SELECT s FROM SysViewAs s WHERE s.staffID = :staffID"),
    @NamedQuery(name = "SysViewAs.findByDeptID", query = "SELECT s FROM SysViewAs s WHERE s.deptID = :deptID"),
    @NamedQuery(name = "SysViewAs.findByLocID", query = "SELECT s FROM SysViewAs s WHERE s.locID = :locID"),
    @NamedQuery(name = "SysViewAs.findByEmail", query = "SELECT s FROM SysViewAs s WHERE s.email = :email")})
public class SysViewAs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Size(max = 10)
    @Column(name = "deptID")
    private String deptID;
    @Size(max = 10)
    @Column(name = "locID")
    private String locID;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;

    public SysViewAs() {
    }

    public SysViewAs(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getDeptID() {
        return deptID;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }

    public String getLocID() {
        return locID;
    }

    public void setLocID(String locID) {
        this.locID = locID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysViewAs)) {
            return false;
        }
        SysViewAs other = (SysViewAs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.SysViewAs[ id=" + id + " ]";
    }
    
}
