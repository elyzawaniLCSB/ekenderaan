/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "booking_destination")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookingDestination.findAll", query = "SELECT b FROM BookingDestination b"),
    @NamedQuery(name = "BookingDestination.findById", query = "SELECT b FROM BookingDestination b WHERE b.id = :id"),
    @NamedQuery(name = "BookingDestination.findByBookID", query = "SELECT b FROM BookingDestination b WHERE b.bookID = :bookID"),
    @NamedQuery(name = "BookingDestination.findByDesttype", query = "SELECT b FROM BookingDestination b WHERE b.desttype = :desttype"),
    @NamedQuery(name = "BookingDestination.findByDestcode", query = "SELECT b FROM BookingDestination b WHERE b.destcode = :destcode"),
    @NamedQuery(name = "BookingDestination.findByDestdescp", query = "SELECT b FROM BookingDestination b WHERE b.destdescp = :destdescp"),
    @NamedQuery(name = "BookingDestination.findByUserID", query = "SELECT b FROM BookingDestination b WHERE b.userID = :userID"),
    @NamedQuery(name = "BookingDestination.findByStartdate", query = "SELECT b FROM BookingDestination b WHERE b.startdate = :startdate"),
    @NamedQuery(name = "BookingDestination.findByEnddate", query = "SELECT b FROM BookingDestination b WHERE b.enddate = :enddate"),
    @NamedQuery(name = "BookingDestination.findByStarttime", query = "SELECT b FROM BookingDestination b WHERE b.starttime = :starttime"),
    @NamedQuery(name = "BookingDestination.findByEndtime", query = "SELECT b FROM BookingDestination b WHERE b.endtime = :endtime"),
    @NamedQuery(name = "BookingDestination.findByDaycount", query = "SELECT b FROM BookingDestination b WHERE b.daycount = :daycount")})
public class BookingDestination implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "bookID")
    private String bookID;
    @Size(max = 20)
    @Column(name = "desttype")
    private String desttype;
    @Size(max = 10)
    @Column(name = "destcode")
    private String destcode;
    @Size(max = 200)
    @Column(name = "destdescp")
    private String destdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "userID")
    private String userID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "startdate")
    private String startdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "enddate")
    private String enddate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "starttime")
    private String starttime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "endtime")
    private String endtime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "daycount")
    private int daycount;

    public BookingDestination() {
    }

    public BookingDestination(Integer id) {
        this.id = id;
    }

    public BookingDestination(Integer id, String bookID, String userID, String startdate, String enddate, String starttime, String endtime, int daycount) {
        this.id = id;
        this.bookID = bookID;
        this.userID = userID;
        this.startdate = startdate;
        this.enddate = enddate;
        this.starttime = starttime;
        this.endtime = endtime;
        this.daycount = daycount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getDesttype() {
        return desttype;
    }

    public void setDesttype(String desttype) {
        this.desttype = desttype;
    }

    public String getDestcode() {
        return destcode;
    }

    public void setDestcode(String destcode) {
        this.destcode = destcode;
    }

    public String getDestdescp() {
        return destdescp;
    }

    public void setDestdescp(String destdescp) {
        this.destdescp = destdescp;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public int getDaycount() {
        return daycount;
    }

    public void setDaycount(int daycount) {
        this.daycount = daycount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingDestination)) {
            return false;
        }
        BookingDestination other = (BookingDestination) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.smartbooking.model.BookingDestination[ id=" + id + " ]";
    }
    
}
