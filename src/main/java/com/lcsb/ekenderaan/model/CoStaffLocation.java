/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff_location")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CoStaffLocation.findAll", query = "SELECT c FROM CoStaffLocation c"),
    @NamedQuery(name = "CoStaffLocation.findById", query = "SELECT c FROM CoStaffLocation c WHERE c.id = :id"),
    @NamedQuery(name = "CoStaffLocation.findByDescp", query = "SELECT c FROM CoStaffLocation c WHERE c.descp = :descp"),
    @NamedQuery(name = "CoStaffLocation.findByLocID", query = "SELECT c FROM CoStaffLocation c WHERE c.locID = :locID")})
public class CoStaffLocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "descp")
    private String descp;
    @Size(max = 4)
    @Column(name = "locID")
    private String locID;

    public CoStaffLocation() {
    }

    public CoStaffLocation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getLocID() {
        return locID;
    }

    public void setLocID(String locID) {
        this.locID = locID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CoStaffLocation)) {
            return false;
        }
        CoStaffLocation other = (CoStaffLocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.CoStaffLocation[ id=" + id + " ]";
    }
    
}
