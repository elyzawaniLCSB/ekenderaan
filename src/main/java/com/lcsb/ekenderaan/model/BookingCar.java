/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "booking_car")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookingCar.findAll", query = "SELECT b FROM BookingCar b"),
    @NamedQuery(name = "BookingCar.findById", query = "SELECT b FROM BookingCar b WHERE b.id = :id"),
    @NamedQuery(name = "BookingCar.findByBookID", query = "SELECT b FROM BookingCar b WHERE b.bookID = :bookID"),
    @NamedQuery(name = "BookingCar.findByCarID", query = "SELECT b FROM BookingCar b WHERE b.carID = :carID"),
    @NamedQuery(name = "BookingCar.findByDriverID", query = "SELECT b FROM BookingCar b WHERE b.driverID = :driverID"),
    @NamedQuery(name = "BookingCar.findByReason", query = "SELECT b FROM BookingCar b WHERE b.reason = :reason"),
    @NamedQuery(name = "BookingCar.findByDrivetype", query = "SELECT b FROM BookingCar b WHERE b.drivetype = :drivetype"),
    @NamedQuery(name = "BookingCar.findByStatus", query = "SELECT b FROM BookingCar b WHERE b.status = :status"),
    @NamedQuery(name = "BookingCar.findByAppdate", query = "SELECT b FROM BookingCar b WHERE b.appdate = :appdate"),
    @NamedQuery(name = "BookingCar.findByDestID", query = "SELECT b FROM BookingCar b WHERE b.destID = :destID")})
public class BookingCar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "bookID")
    private String bookID;
    @Size(max = 4)
    @Column(name = "carID")
    private String carID;
    @Size(max = 4)
    @Column(name = "driverID")
    private String driverID;
    @Size(max = 200)
    @Column(name = "reason")
    private String reason;
    @Size(max = 20)
    @Column(name = "drivetype")
    private String drivetype;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "appdate")
    private String appdate;
    @Column(name = "destID")
    private Integer destID;

    public BookingCar() {
    }

    public BookingCar(Integer id) {
        this.id = id;
    }

    public BookingCar(Integer id, String bookID, String appdate) {
        this.id = id;
        this.bookID = bookID;
        this.appdate = appdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDrivetype() {
        return drivetype;
    }

    public void setDrivetype(String drivetype) {
        this.drivetype = drivetype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public Integer getDestID() {
        return destID;
    }

    public void setDestID(Integer destID) {
        this.destID = destID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingCar)) {
            return false;
        }
        BookingCar other = (BookingCar) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.smartbooking.model.BookingCar[ id=" + id + " ]";
    }
    
}
