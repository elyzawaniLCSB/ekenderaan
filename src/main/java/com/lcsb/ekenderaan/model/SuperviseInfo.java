/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "supervise_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SuperviseInfo.findAll", query = "SELECT s FROM SuperviseInfo s"),
    @NamedQuery(name = "SuperviseInfo.findById", query = "SELECT s FROM SuperviseInfo s WHERE s.id = :id"),
    @NamedQuery(name = "SuperviseInfo.findByStaffID", query = "SELECT s FROM SuperviseInfo s WHERE s.staffID = :staffID"),
    @NamedQuery(name = "SuperviseInfo.findBySupervisorID", query = "SELECT s FROM SuperviseInfo s WHERE s.supervisorID = :supervisorID"),
    @NamedQuery(name = "SuperviseInfo.findByHeadID", query = "SELECT s FROM SuperviseInfo s WHERE s.headID = :headID")})
public class SuperviseInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Size(max = 10)
    @Column(name = "supervisorID")
    private String supervisorID;
    @Size(max = 10)
    @Column(name = "headID")
    private String headID;

    public SuperviseInfo() {
    }

    public SuperviseInfo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getSupervisorID() {
        return supervisorID;
    }

    public void setSupervisorID(String supervisorID) {
        this.supervisorID = supervisorID;
    }

    public String getHeadID() {
        return headID;
    }

    public void setHeadID(String headID) {
        this.headID = headID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuperviseInfo)) {
            return false;
        }
        SuperviseInfo other = (SuperviseInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.SuperviseInfo[ id=" + id + " ]";
    }
    
}
