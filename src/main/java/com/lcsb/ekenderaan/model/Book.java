/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "booking")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b"),
    @NamedQuery(name = "Book.findByBookID", query = "SELECT b FROM Book b WHERE b.bookID = :bookID"),
    @NamedQuery(name = "Book.findByDaycount", query = "SELECT b FROM Book b WHERE b.daycount = :daycount"),
    @NamedQuery(name = "Book.findByReason", query = "SELECT b FROM Book b WHERE b.reason = :reason"),
    @NamedQuery(name = "Book.findByDrivetype", query = "SELECT b FROM Book b WHERE b.drivetype = :drivetype"),
    @NamedQuery(name = "Book.findByStaffname", query = "SELECT b FROM Book b WHERE b.staffname = :staffname"),
    @NamedQuery(name = "Book.findByStaffID", query = "SELECT b FROM Book b WHERE b.staffID = :staffID"),
    @NamedQuery(name = "Book.findByEmail", query = "SELECT b FROM Book b WHERE b.email = :email"),
    @NamedQuery(name = "Book.findByDateapply", query = "SELECT b FROM Book b WHERE b.dateapply = :dateapply"),
    @NamedQuery(name = "Book.findByStatus", query = "SELECT b FROM Book b WHERE b.status = :status"),
    @NamedQuery(name = "Book.findByAppID", query = "SELECT b FROM Book b WHERE b.appID = :appID"),
    @NamedQuery(name = "Book.findByAppname", query = "SELECT b FROM Book b WHERE b.appname = :appname"),
    @NamedQuery(name = "Book.findByAppemail", query = "SELECT b FROM Book b WHERE b.appemail = :appemail"),
    @NamedQuery(name = "Book.findByAppdate", query = "SELECT b FROM Book b WHERE b.appdate = :appdate")})
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "bookID")
    private String bookID;
    @Column(name = "daycount")
    private Integer daycount;
    @Size(max = 200)
    @Column(name = "reason")
    private String reason;
    @Size(max = 20)
    @Column(name = "drivetype")
    private String drivetype;
    @Size(max = 100)
    @Column(name = "staffname")
    private String staffname;
    @Size(max = 10)
    @Column(name = "staffID")
    private String staffID;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 10)
    @Column(name = "dateapply")
    private String dateapply;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Size(max = 20)
    @Column(name = "appID")
    private String appID;
    @Size(max = 100)
    @Column(name = "appname")
    private String appname;
    @Size(max = 30)
    @Column(name = "appemail")
    private String appemail;
    @Size(max = 10)
    @Column(name = "appdate")
    private String appdate;

    public Book() {
    }

    public Book(String bookID) {
        this.bookID = bookID;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public Integer getDaycount() {
        return daycount;
    }

    public void setDaycount(Integer daycount) {
        this.daycount = daycount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDrivetype() {
        return drivetype;
    }

    public void setDrivetype(String drivetype) {
        this.drivetype = drivetype;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateapply() {
        return dateapply;
    }

    public void setDateapply(String dateapply) {
        this.dateapply = dateapply;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppemail() {
        return appemail;
    }

    public void setAppemail(String appemail) {
        this.appemail = appemail;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookID != null ? bookID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.bookID == null && other.bookID != null) || (this.bookID != null && !this.bookID.equals(other.bookID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.smartbooking.model.Book[ bookID=" + bookID + " ]";
    }
    
}
