/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "leave_multi_temp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveMultiTemp.findAll", query = "SELECT l FROM LeaveMultiTemp l"),
    @NamedQuery(name = "LeaveMultiTemp.findByLeaveID", query = "SELECT l FROM LeaveMultiTemp l WHERE l.leaveID = :leaveID"),
    @NamedQuery(name = "LeaveMultiTemp.findByStaffID", query = "SELECT l FROM LeaveMultiTemp l WHERE l.staffID = :staffID"),
    @NamedQuery(name = "LeaveMultiTemp.findByType", query = "SELECT l FROM LeaveMultiTemp l WHERE l.type = :type"),
    @NamedQuery(name = "LeaveMultiTemp.findByReason", query = "SELECT l FROM LeaveMultiTemp l WHERE l.reason = :reason"),
    @NamedQuery(name = "LeaveMultiTemp.findByDays", query = "SELECT l FROM LeaveMultiTemp l WHERE l.days = :days"),
    @NamedQuery(name = "LeaveMultiTemp.findByDateapply", query = "SELECT l FROM LeaveMultiTemp l WHERE l.dateapply = :dateapply"),
    @NamedQuery(name = "LeaveMultiTemp.findByDatestart", query = "SELECT l FROM LeaveMultiTemp l WHERE l.datestart = :datestart"),
    @NamedQuery(name = "LeaveMultiTemp.findByDateend", query = "SELECT l FROM LeaveMultiTemp l WHERE l.dateend = :dateend"),
    @NamedQuery(name = "LeaveMultiTemp.findByStatus", query = "SELECT l FROM LeaveMultiTemp l WHERE l.status = :status"),
    @NamedQuery(name = "LeaveMultiTemp.findByYear", query = "SELECT l FROM LeaveMultiTemp l WHERE l.year = :year"),
    @NamedQuery(name = "LeaveMultiTemp.findByPeriod", query = "SELECT l FROM LeaveMultiTemp l WHERE l.period = :period"),
    @NamedQuery(name = "LeaveMultiTemp.findByTimeapply", query = "SELECT l FROM LeaveMultiTemp l WHERE l.timeapply = :timeapply"),
    @NamedQuery(name = "LeaveMultiTemp.findBySessionID", query = "SELECT l FROM LeaveMultiTemp l WHERE l.sessionID = :sessionID")})
public class LeaveMultiTemp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "leaveID")
    private String leaveID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "staffID")
    private String staffID;
    @Size(max = 5)
    @Column(name = "type")
    private String type;
    @Size(max = 300)
    @Column(name = "reason")
    private String reason;
    @Column(name = "days")
    private Integer days;
    @Size(max = 10)
    @Column(name = "dateapply")
    private String dateapply;
    @Size(max = 10)
    @Column(name = "datestart")
    private String datestart;
    @Size(max = 10)
    @Column(name = "dateend")
    private String dateend;
    @Size(max = 30)
    @Column(name = "status")
    private String status;
    @Size(max = 4)
    @Column(name = "year")
    private String year;
    @Size(max = 2)
    @Column(name = "period")
    private String period;
    @Size(max = 8)
    @Column(name = "timeapply")
    private String timeapply;
    @Size(max = 200)
    @Column(name = "sessionID")
    private String sessionID;

    public LeaveMultiTemp() {
    }

    public LeaveMultiTemp(String leaveID) {
        this.leaveID = leaveID;
    }

    public LeaveMultiTemp(String leaveID, String staffID) {
        this.leaveID = leaveID;
        this.staffID = staffID;
    }

    public String getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(String leaveID) {
        this.leaveID = leaveID;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public String getDateapply() {
        return dateapply;
    }

    public void setDateapply(String dateapply) {
        this.dateapply = dateapply;
    }

    public String getDatestart() {
        return datestart;
    }

    public void setDatestart(String datestart) {
        this.datestart = datestart;
    }

    public String getDateend() {
        return dateend;
    }

    public void setDateend(String dateend) {
        this.dateend = dateend;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTimeapply() {
        return timeapply;
    }

    public void setTimeapply(String timeapply) {
        this.timeapply = timeapply;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leaveID != null ? leaveID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveMultiTemp)) {
            return false;
        }
        LeaveMultiTemp other = (LeaveMultiTemp) object;
        if ((this.leaveID == null && other.leaveID != null) || (this.leaveID != null && !this.leaveID.equals(other.leaveID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.LeaveMultiTemp[ leaveID=" + leaveID + " ]";
    }
    
}
