/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class BookingMasterTemp {
    
    private String sessionid;
    private String compID;
    private List<BookingPassengerTemp> listPassenger;
    private List<BookingDestinationTemp> listDestination;

    public String getCompID() {
        return compID;
    }

    public void setCompID(String compID) {
        this.compID = compID;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public List<BookingPassengerTemp> getListPassenger() {
        return listPassenger;
    }

    public void setListPassenger(List<BookingPassengerTemp> listPassenger) {
        this.listPassenger = listPassenger;
    }

    public List<BookingDestinationTemp> getListDestination() {
        return listDestination;
    }

    public void setListDestination(List<BookingDestinationTemp> listDestination) {
        this.listDestination = listDestination;
    }
    
    
    
    
}
