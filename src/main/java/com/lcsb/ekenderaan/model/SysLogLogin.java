/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sys_log_login")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SysLogLogin.findAll", query = "SELECT s FROM SysLogLogin s"),
    @NamedQuery(name = "SysLogLogin.findById", query = "SELECT s FROM SysLogLogin s WHERE s.id = :id"),
    @NamedQuery(name = "SysLogLogin.findBySessionid", query = "SELECT s FROM SysLogLogin s WHERE s.sessionid = :sessionid"),
    @NamedQuery(name = "SysLogLogin.findByName", query = "SELECT s FROM SysLogLogin s WHERE s.name = :name"),
    @NamedQuery(name = "SysLogLogin.findByUserid", query = "SELECT s FROM SysLogLogin s WHERE s.userid = :userid"),
    @NamedQuery(name = "SysLogLogin.findByDate", query = "SELECT s FROM SysLogLogin s WHERE s.date = :date"),
    @NamedQuery(name = "SysLogLogin.findByTime", query = "SELECT s FROM SysLogLogin s WHERE s.time = :time"),
    @NamedQuery(name = "SysLogLogin.findByConnection", query = "SELECT s FROM SysLogLogin s WHERE s.connection = :connection"),
    @NamedQuery(name = "SysLogLogin.findByDateout", query = "SELECT s FROM SysLogLogin s WHERE s.dateout = :dateout"),
    @NamedQuery(name = "SysLogLogin.findByTimeout", query = "SELECT s FROM SysLogLogin s WHERE s.timeout = :timeout"),
    @NamedQuery(name = "SysLogLogin.findByLocID", query = "SELECT s FROM SysLogLogin s WHERE s.locID = :locID"),
    @NamedQuery(name = "SysLogLogin.findByLocName", query = "SELECT s FROM SysLogLogin s WHERE s.locName = :locName"),
    @NamedQuery(name = "SysLogLogin.findByDeptID", query = "SELECT s FROM SysLogLogin s WHERE s.deptID = :deptID"),
    @NamedQuery(name = "SysLogLogin.findByDeptName", query = "SELECT s FROM SysLogLogin s WHERE s.deptName = :deptName"),
    @NamedQuery(name = "SysLogLogin.findByLastAccessTime", query = "SELECT s FROM SysLogLogin s WHERE s.lastAccessTime = :lastAccessTime"),
    @NamedQuery(name = "SysLogLogin.findByTypeoflogin", query = "SELECT s FROM SysLogLogin s WHERE s.typeoflogin = :typeoflogin")})
public class SysLogLogin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sessionid")
    private String sessionid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "userid")
    private String userid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "time")
    private String time;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "connection")
    private String connection;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "dateout")
    private String dateout;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "timeout")
    private String timeout;
    @Size(max = 4)
    @Column(name = "locID")
    private String locID;
    @Size(max = 200)
    @Column(name = "locName")
    private String locName;
    @Size(max = 4)
    @Column(name = "deptID")
    private String deptID;
    @Size(max = 200)
    @Column(name = "deptName")
    private String deptName;
    @Size(max = 50)
    @Column(name = "lastAccessTime")
    private String lastAccessTime;
    @Size(max = 20)
    @Column(name = "typeoflogin")
    private String typeoflogin;

    public SysLogLogin() {
    }

    public SysLogLogin(Integer id) {
        this.id = id;
    }

    public SysLogLogin(Integer id, String sessionid, String name, String userid, String date, String time, String connection, String dateout, String timeout) {
        this.id = id;
        this.sessionid = sessionid;
        this.name = name;
        this.userid = userid;
        this.date = date;
        this.time = time;
        this.connection = connection;
        this.dateout = dateout;
        this.timeout = timeout;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getDateout() {
        return dateout;
    }

    public void setDateout(String dateout) {
        this.dateout = dateout;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getLocID() {
        return locID;
    }

    public void setLocID(String locID) {
        this.locID = locID;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public String getDeptID() {
        return deptID;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public String getTypeoflogin() {
        return typeoflogin;
    }

    public void setTypeoflogin(String typeoflogin) {
        this.typeoflogin = typeoflogin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysLogLogin)) {
            return false;
        }
        SysLogLogin other = (SysLogLogin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.SysLogLogin[ id=" + id + " ]";
    }
    
}
