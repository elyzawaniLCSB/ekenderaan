/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff_position")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CoStaffPosition.findAll", query = "SELECT c FROM CoStaffPosition c"),
    @NamedQuery(name = "CoStaffPosition.findById", query = "SELECT c FROM CoStaffPosition c WHERE c.id = :id"),
    @NamedQuery(name = "CoStaffPosition.findByDescp", query = "SELECT c FROM CoStaffPosition c WHERE c.descp = :descp"),
    @NamedQuery(name = "CoStaffPosition.findByDorder", query = "SELECT c FROM CoStaffPosition c WHERE c.dorder = :dorder"),
    @NamedQuery(name = "CoStaffPosition.findByLevel", query = "SELECT c FROM CoStaffPosition c WHERE c.level = :level")})
public class CoStaffPosition implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "descp")
    private String descp;
    @Column(name = "dorder")
    private Integer dorder;
    @Column(name = "level")
    private Integer level;
    private String code;

    public CoStaffPosition() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CoStaffPosition(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public Integer getDorder() {
        return dorder;
    }

    public void setDorder(Integer dorder) {
        this.dorder = dorder;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CoStaffPosition)) {
            return false;
        }
        CoStaffPosition other = (CoStaffPosition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.CoStaffPosition[ id=" + id + " ]";
    }
    
}
