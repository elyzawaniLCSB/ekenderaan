/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "booking_passenger")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookingPassenger.findAll", query = "SELECT b FROM BookingPassenger b"),
    @NamedQuery(name = "BookingPassenger.findById", query = "SELECT b FROM BookingPassenger b WHERE b.id = :id"),
    @NamedQuery(name = "BookingPassenger.findByBookID", query = "SELECT b FROM BookingPassenger b WHERE b.bookID = :bookID"),
    @NamedQuery(name = "BookingPassenger.findByCompID", query = "SELECT b FROM BookingPassenger b WHERE b.compID = :compID")})
public class BookingPassenger implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "bookID")
    private String bookID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "compID")
    private String compID;

    public BookingPassenger() {
    }

    public BookingPassenger(Integer id) {
        this.id = id;
    }

    public BookingPassenger(Integer id, String bookID, String compID) {
        this.id = id;
        this.bookID = bookID;
        this.compID = compID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getCompID() {
        return compID;
    }

    public void setCompID(String compID) {
        this.compID = compID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingPassenger)) {
            return false;
        }
        BookingPassenger other = (BookingPassenger) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.smartbooking.model.BookingPassenger[ id=" + id + " ]";
    }
    
}
