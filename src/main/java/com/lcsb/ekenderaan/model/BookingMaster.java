/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.model;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class BookingMaster {
    
    private Book booking;
    private List<BookingPassenger> listPassenger;
    private List<BookingDestination> listDestination;

    public Book getBooking() {
        return booking;
    }

    public void setBooking(Book booking) {
        this.booking = booking;
    }

    public List<BookingPassenger> getListPassenger() {
        return listPassenger;
    }

    public void setListPassenger(List<BookingPassenger> listPassenger) {
        this.listPassenger = listPassenger;
    }

    public List<BookingDestination> getListDestination() {
        return listDestination;
    }

    public void setListDestination(List<BookingDestination> listDestination) {
        this.listDestination = listDestination;
    }
    
    
}
