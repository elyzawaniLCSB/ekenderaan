/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.dao.StaffDAO;
import com.lcsb.ekenderaan.model.CoStaff;
import com.lcsb.ekenderaan.model.Leave;
import com.lcsb.ekenderaan.model.LeaveComment;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.Notification;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

/**
 *
 * @author fadhilfahmi
 */
public class EmailDAO {

    public static Email emailTemplate(LoginProfile log, Leave ac, CoStaff toSt, String action) throws Exception {

        Email email = new Email();

        CoStaff st = StaffDAO.getInfo(log, ac.getStaffID());


        String html = "";

        if (action.equals("Approved") || action.equals("Rejected")) {
            html = EmailDAO.emailTemplatePemohon(log, toSt.getName(), action, ac, toSt);
        }else if(action.equals("comment")){
            html = EmailDAO.emailTemplateKomen(log, ac, toSt);
        }else {
            html = EmailDAO.emailTemplatePenyelia(log, toSt.getName(), action, ac, toSt);
        }

        email.setBody(html);

        return email;
    }

    public static void sendNow(LoginProfile log, String action, String leaveID) throws IOException, Exception {

        //List<AuthActionTable> ac = (List<AuthActionTable>) EmailDAO.getAuthInfo(log, moduleid, action);//multiple receiver
        String emailadd = "fadhilfahmi@lcsb.com.my";

        //StringUtils.capitalize(toSt.getName());
        CoStaff st = null;
        Notification n = new Notification();

        Leave lv = (Leave) LeaveDAO.getLeaveInfoDetail(log, leaveID);

        if (action.equals("Preparing")) {

            st = (CoStaff) StaffDAO.getInfo(log, StaffDAO.getUserAccessByLevelForEmail(log, 1, true).getStaffID());

            emailadd = st.getEmail();

            n.setStaffID(st.getStaffid());
            n.setType("prepareleave");
            n.setLeaveID(leaveID);

            NotifyDAO.insertNoti(log, n);

        } else if (action.equals("Checked") && lv.getType().equals("C01")) {

            st = (CoStaff) StaffDAO.getInfoSupervisor(log, lv.getStaffID());

            n.setStaffID(st.getStaffid());
            n.setType("checkedleave");
            n.setLeaveID(leaveID);

            NotifyDAO.insertNoti(log, n);

            emailadd = st.getEmail();

        } else if (action.equals("Checked") && lv.getType().equals("C02")) {//ke head dept

            st = (CoStaff) StaffDAO.getInfoHead(log, lv.getStaffID());

            n.setStaffID(st.getStaffid());
            n.setType("checkedleave");
            n.setLeaveID(leaveID);

            NotifyDAO.insertNoti(log, n);

            emailadd = st.getEmail();

        } else if (action.equals("Supported")) {//ke head dept

            st = (CoStaff) StaffDAO.getInfoHead(log, lv.getStaffID());

            n.setStaffID(st.getStaffid());
            n.setType("supportedleave");
            n.setLeaveID(leaveID);

            NotifyDAO.insertNoti(log, n);

            emailadd = st.getEmail();

        }/*else if(action.equals("Approved")){//ke hr head
            
            st = (CoStaff) StaffDAO.getInfoHRHead(log);
            
            emailadd = st.getEmail();
            
        }*/ else if (action.equals("Approved")) {//ke staff yang apply semula

            st = (CoStaff) StaffDAO.getInfo(log, lv.getStaffID());

            n.setStaffID(st.getStaffid());
            n.setType("approvedleave");
            n.setLeaveID(leaveID);

            NotifyDAO.insertNoti(log, n);

            emailadd = st.getEmail();

        } else if (action.equals("Rejected")) {//ke staff yang apply semula

            st = (CoStaff) StaffDAO.getInfo(log, lv.getStaffID());

            n.setStaffID(st.getStaffid());
            n.setType("rejectedleave");
            n.setLeaveID(leaveID);

            NotifyDAO.insertNoti(log, n);

            emailadd = st.getEmail();

        } else if (action.equals("comment")) {

            st = (CoStaff) StaffDAO.getInfo(log, lv.getStaffID());

            n.setStaffID(st.getStaffid());
            n.setType("comment");
            n.setLeaveID(leaveID);

            NotifyDAO.insertNoti(log, n);

            emailadd = st.getEmail();

        }

        //for (AuthActionTable j : ac) {
        Email email = (Email) EmailDAO.emailTemplate(log, lv, st, action);

        String escaped = StringEscapeUtils.escapeJson(email.getBody());

        //JSON String need to be constructed for the specific resource. 
        //We may construct complex JSON using any third-party JSON libraries such as jackson or org.json
        String jsonInputString = "{\"to_address\": \"" + emailadd + "\", \"subject\": \"[Notifikasi] Permohonan Cuti \", \"senderEmail\": \"ecuti@lcsb.com.my\", \"senderPassword\": \"@dminEcuti\", \"body\": \"" + escaped + "\"}";

        //Logger.getLogger(EmailDAO.class.getName()).log(Level.INFO, jsonInputString);
        //Change the URL with any other publicly accessible POST resource, which accepts JSON request body
        URL url = new URL("http://192.168.254.200:8087/send");

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");

        con.setDoOutput(true);

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int code = con.getResponseCode();
        System.out.println(code);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }

        // }
    }

//    public static AuthActionTable getAuthInfoRS(ResultSet rs) throws SQLException {
//        AuthActionTable am = new AuthActionTable();
//        am.setAction(rs.getString("action"));
//        am.setActionby(rs.getString("actionby"));
//        am.setEmail(rs.getString("email"));
//        am.setId(rs.getInt("id"));
//        am.setModuleid(rs.getString("moduleid"));
//        return am;
//    }
//
//    public static List<AuthActionTable> getAuthInfo(LoginProfile log, String moduleid, String action) throws Exception {
//
//        List<AuthActionTable> CV;
//        CV = new ArrayList();
//
//        AuthActionTable am = new AuthActionTable();
//
//        ResultSet rs = null;
//        PreparedStatement stmt = log.getCon().prepareStatement("select * from auth_action_table where moduleid=? and action=?");
//        stmt.setString(1, moduleid);
//        stmt.setString(2, action);
//        rs = stmt.executeQuery();
//        while (rs.next()) {
//            CV.add(getAuthInfoRS(rs));
//        }
//
//        return CV;
//    }
    public static void sendFromMailer(LoginProfile log, Email emel) throws IOException, Exception {

    }

    public static Email emailTemplate2(LoginProfile log, Email email) throws Exception {

        //String receiverName = StaffDAO.getInfo(log, ac.getActionby()).getName();
        String html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> <html> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" > <title>Mailto</title> <link href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700\" rel=\"stylesheet\"> <style type=\"text/css\"> html { -webkit-text-size-adjust: none; -ms-text-size-adjust: none;} @media only screen and (min-device-width: 750px) { .table750 {width: 750px !important;} } @media only screen and (max-device-width: 750px), only screen and (max-width: 750px){ table[class=\"table750\"] {width: 100% !important;} .mob_b {width: 93% !important; max-width: 93% !important; min-width: 93% !important;} .mob_b1 {width: 100% !important; max-width: 100% !important; min-width: 100% !important;} .mob_left {text-align: left !important;} .mob_soc {width: 50% !important; max-width: 50% !important; min-width: 50% !important;} .mob_menu {width: 50% !important; max-width: 50% !important; min-width: 50% !important; box-shadow: inset -1px -1px 0 0 rgba(255, 255, 255, 0.2); } .mob_btn {width: 100% !important; max-width: 100% !important; min-width: 100% !important;} .mob_card {width: 88% !important; max-width: 88% !important; min-width: 88% !important;} .mob_title1 {font-size: 36px !important; line-height: 40px !important;} .mob_title2 {font-size: 26px !important; line-height: 33px !important;} .top_pad {height: 15px !important; max-height: 15px !important; min-height: 15px !important;} .mob_pad {width: 15px !important; max-width: 15px !important; min-width: 15px !important;} .top_pad2 {height: 40px !important; max-height: 40px !important; min-height: 40px !important;} } @media only screen and (max-device-width: 550px), only screen and (max-width: 550px){ .mod_div {display: block !important;} } .table750 {width: 750px;} .flatTable{ width:100%; min-width:500px; border-collapse:collapse; font-weight:bold; color:#6b6b6b; font-family:Lato; } .flatTable tr{ height:50px; border-bottom:rgba(0,0,0,.05) 1px solid; } .flatTable td{ box-sizing:border-box; padding-left:30px; } .titleTr{ height:70px; color:#f6f3f7; background:#418a95; border:0px solid; } .plusTd{ background:url(https://i.imgur.com/3hSkhay.png) center center no-repeat, rgba(0,0,0,.1); } .controlTd{ position:relative; width:80px; background:url(https://i.imgur.com/9Q5f6cv.png) center center no-repeat; cursor:pointer; } .headingTr{ height:30px; background:#418a95; color:#f6f3f7; font-size:8pt; border:0px solid; } } .button{ text-align:center; cursor:pointer; } .sForm{ position:absolute; top:0; right:-400px; width:400px; height:100%; background:#f6f3f7; overflow:hidden; transition:width 1s, right .3s; padding:0px; box-sizing:border-box; .close{ float:right; height:70px; width:80px; padding-top:25px; box-sizing:border-box; background:rgba(255,0,0,.4); } .title{ width:100%; height:70px; padding-top:20px; padding-left:20px; box-sizing:border-box; background:rgba(0,0,0,.1); } } .open{ right:0; width:400px !important; } .settingsIcons{ position:absolute; top:0; right:0; width:0; overflow:hidden; } .display{ width:300px; } .settingsIcon{ float:right; background:#418a95; color:#f6f3f7; height:50px; width:80px; padding-top:15px; box-sizing:border-box; text-align:center; overflow:hidden; transition:width 1s; } .settingsIcon:hover{ background:#418a95; } tr:nth-child(3){ .settingsIcon{ height:51px; } } .openIcon{ width:80px; } </style> </head> <body style=\"margin: 0; padding: 0;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"background: #f5f8fa; min-width: 340px; font-size: 12px; line-height: normal;\"> <tr> <td align=\"center\" valign=\"top\"> <!--[if (gte mso 9)|(IE)]> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr><td align=\"center\" valign=\"top\" width=\"750\"><![endif]--> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"750\" class=\"table750\" style=\"width: 100%; max-width: 750px; min-width: 340px; background: #f5f8fa;\"> <tr> <td class=\"mob_pad\" width=\"25\" style=\"width: 25px; max-width: 25px; min-width: 25px;\">&nbsp;</td> <td align=\"center\" valign=\"top\" style=\"background: #ffffff;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"right\" valign=\"top\"> <div class=\"top_pad\" style=\"height: 25px; line-height: 25px; font-size: 23px;\">&nbsp;</div> </td> </tr> </table> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"left\" valign=\"top\"> <div style=\"height: 30px; line-height: 30px; font-size: 28px;\">&nbsp;</div> <!--[if (gte mso 9)|(IE)]> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr><td align=\"center\" valign=\"top\" width=\"460\"><![endif]--> <div style=\"display: inline-block; vertical-align: top; width: 74%; min-width: 270px;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%;\"> <tr> <td align=\"left\" valign=\"top\"> <div style=\"height: 10px; line-height: 10px; font-size: 20px;\">&nbsp;</div> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 128px;\"> <img src=\"https://docs.google.com/uc?id=1oJiHf9R7Aeby8zwlIGmVU7A9BTtl9zd2\" alt=\"img\" width=\"200\" border=\"0\" style=\"display: block; width: 200;\" /> </a> </td> </tr> </table> </div><!--[if (gte mso 9)|(IE)]></td><td align=\"center\" valign=\"top\" width=\"150\"><![endif]--><div style=\"display: inline-block; vertical-align: top; width: 150px;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%;\"> <tr> <td class=\"mob_left\" align=\"right\" valign=\"top\"> <div style=\"height: 15px; line-height: 15px; font-size: 13px;\">&nbsp;</div> </td> </tr> </table> </div> <!--[if (gte mso 9)|(IE)]> </td></tr> </table><![endif]--> <div class=\"top_pad2\" style=\"height: 20px; line-height: 20px; font-size: 68px;\">&nbsp;</div> </td> </tr> </table> <hr size=\"2\" width=\"90%\" noshade=\"\" style=\"margin:0;margin:0!important;color:#06acc7;border-color:#06acc7;background-color:#06acc7;\"> <div class=\"top_pad2\" style=\"height: 20px; line-height: 20px; font-size: 68px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"left\" valign=\"top\"> <font class=\"mob_title1\" face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 35px; line-height: 55px; font-weight: 300; letter-spacing: -1.5px;\"> <span class=\"mob_title1\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 35px; line-height: 55px; font-weight: 300; letter-spacing: -1.5px;\">Assalamualaikum, </span> </font> <div style=\"height: 22px; line-height: 22px; font-size: 20px;\">&nbsp;</div>  </td> </tr> </table>";

        html += "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"><tr><td>\n";
        html += email.getBody();

        html += "</td></tr></table>";

        html += "<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 40px; line-height: 40px; font-size: 38px;\">&nbsp;</div> <table class=\"mob_btn\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background: #27cbcc; border-radius: 4px;\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"https://fms.lcsb.com.my\" target=\"_blank\" style=\"display: block; border: 1px solid #27cbcc; border-radius: 4px; padding: 19px 26px; font-family: 'Source Sans Pro', Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 26px; line-height: 30px; text-decoration: none; white-space: nowrap; font-weight: 600;\"> <font face=\"'Source Sans Pro', sans-serif\" color=\"#ffffff\" style=\"font-size: 26px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: 600;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 26px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: 600;\">Go To FMS</span> </font> </a> </td> </tr> </table> <div style=\"height: 50px; line-height: 50px; font-size: 48px;\">&nbsp;</div> </td> </tr> </table> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td class=\"mob_left\" align=\"center\" valign=\"top\"> <div style=\"height: 25px; line-height: 25px; font-size: 23px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#767676\" style=\"font-size: 17px; line-height: 23px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #767676; font-size: 17px; line-height: 23px;\">Terima kasih di atas kerjasama anda. Sebarang pertanyaan dan kesulitan boleh diajukan melalui emel dan telefon seperti yang tertera di bawah.</span> </font> <div style=\"height: 28px; line-height: 28px; font-size: 26px;\">&nbsp;</div> </td> </tr> </table> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table> </td> </tr> </table> </td> <td class=\"mob_pad\" width=\"25\" style=\"width: 25px; max-width: 25px; min-width: 25px;\">&nbsp;</td> </tr> </table> <!--[if (gte mso 9)|(IE)]> </td></tr> </table><![endif]--> </td> </tr> </table> </body> </html>";

        String header = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> <html> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" > <title>Mailto</title> <link href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700\" rel=\"stylesheet\"> <style type=\"text/css\"> html { -webkit-text-size-adjust: none; -ms-text-size-adjust: none;} @media only screen and (min-device-width: 750px) { .table750 {width: 750px !important;} } @media only screen and (max-device-width: 750px), only screen and (max-width: 750px){ table[class=\"table750\"] {width: 100% !important;} .mob_b {width: 93% !important; max-width: 93% !important; min-width: 93% !important;} .mob_b1 {width: 100% !important; max-width: 100% !important; min-width: 100% !important;} .mob_left {text-align: left !important;} .mob_soc {width: 50% !important; max-width: 50% !important; min-width: 50% !important;} .mob_menu {width: 50% !important; max-width: 50% !important; min-width: 50% !important; box-shadow: inset -1px -1px 0 0 rgba(255, 255, 255, 0.2); } .mob_btn {width: 100% !important; max-width: 100% !important; min-width: 100% !important;} .mob_card {width: 88% !important; max-width: 88% !important; min-width: 88% !important;} .mob_title1 {font-size: 36px !important; line-height: 40px !important;} .mob_title2 {font-size: 26px !important; line-height: 33px !important;} .top_pad {height: 15px !important; max-height: 15px !important; min-height: 15px !important;} .mob_pad {width: 15px !important; max-width: 15px !important; min-width: 15px !important;} .top_pad2 {height: 40px !important; max-height: 40px !important; min-height: 40px !important;} } @media only screen and (max-device-width: 550px), only screen and (max-width: 550px){ .mod_div {display: block !important;} } .table750 {width: 750px;} .flatTable{ width:100%; min-width:500px; border-collapse:collapse; font-weight:bold; color:#6b6b6b; font-family:Lato; } .flatTable tr{ height:50px; border-bottom:rgba(0,0,0,.05) 1px solid; } .flatTable td{ box-sizing:border-box; padding-left:30px; } .titleTr{ height:70px; color:#f6f3f7; background:#418a95; border:0px solid; } .plusTd{ background:url(https://i.imgur.com/3hSkhay.png) center center no-repeat, rgba(0,0,0,.1); } .controlTd{ position:relative; width:80px; background:url(https://i.imgur.com/9Q5f6cv.png) center center no-repeat; cursor:pointer; } .headingTr{ height:30px; background:#418a95; color:#f6f3f7; font-size:8pt; border:0px solid; } } .button{ text-align:center; cursor:pointer; } .sForm{ position:absolute; top:0; right:-400px; width:400px; height:100%; background:#f6f3f7; overflow:hidden; transition:width 1s, right .3s; padding:0px; box-sizing:border-box; .close{ float:right; height:70px; width:80px; padding-top:25px; box-sizing:border-box; background:rgba(255,0,0,.4); } .title{ width:100%; height:70px; padding-top:20px; padding-left:20px; box-sizing:border-box; background:rgba(0,0,0,.1); } } .open{ right:0; width:400px !important; } .settingsIcons{ position:absolute; top:0; right:0; width:0; overflow:hidden; } .display{ width:300px; } .settingsIcon{ float:right; background:#418a95; color:#f6f3f7; height:50px; width:80px; padding-top:15px; box-sizing:border-box; text-align:center; overflow:hidden; transition:width 1s; } .settingsIcon:hover{ background:#418a95; } tr:nth-child(3){ .settingsIcon{ height:51px; } } .openIcon{ width:80px; } </style> </head>";

        String body1 = "<body style=\"margin: 0; padding: 0;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"background: #f5f8fa; min-width: 340px; font-size: 12px; line-height: normal;\"> <tr> <td align=\"center\" valign=\"top\"> <!--[if (gte mso 9)|(IE)]> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr><td align=\"center\" valign=\"top\" width=\"750\"><![endif]--> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"750\" class=\"table750\" style=\"width: 100%; max-width: 750px; min-width: 340px; background: #f5f8fa;\"> <tr> <td class=\"mob_pad\" width=\"25\" style=\"width: 25px; max-width: 25px; min-width: 25px;\">&nbsp;</td> <td align=\"center\" valign=\"top\" style=\"background: #ffffff;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"right\" valign=\"top\"> <div class=\"top_pad\" style=\"height: 25px; line-height: 25px; font-size: 23px;\">&nbsp;</div> </td> </tr> </table> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"left\" valign=\"top\"> <div style=\"height: 30px; line-height: 30px; font-size: 28px;\">&nbsp;</div> <!--[if (gte mso 9)|(IE)]> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr><td align=\"center\" valign=\"top\" width=\"460\"><![endif]--> <div style=\"display: inline-block; vertical-align: top; width: 74%; min-width: 270px;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%;\"> <tr> <td align=\"left\" valign=\"top\"> <div style=\"height: 10px; line-height: 10px; font-size: 20px;\">&nbsp;</div> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 128px;\"> <img src=\"https://docs.google.com/uc?id=1oJiHf9R7Aeby8zwlIGmVU7A9BTtl9zd2\" alt=\"img\" width=\"200\" border=\"0\" style=\"display: block; width: 200;\" /> </a> </td> </tr> </table> </div><!--[if (gte mso 9)|(IE)]></td><td align=\"center\" valign=\"top\" width=\"150\"><![endif]--><div style=\"display: inline-block; vertical-align: top; width: 150px;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%;\"> <tr> <td class=\"mob_left\" align=\"right\" valign=\"top\"> <div style=\"height: 15px; line-height: 15px; font-size: 13px;\">&nbsp;</div> </td> </tr> </table> </div> <!--[if (gte mso 9)|(IE)]> </td></tr> </table><![endif]--> <div class=\"top_pad2\" style=\"height: 20px; line-height: 20px; font-size: 68px;\">&nbsp;</div> </td> </tr> </table> <hr size=\"2\" width=\"90%\" noshade=\"\" style=\"margin:0;margin:0!important;color:#06acc7;border-color:#06acc7;background-color:#06acc7;\"> <div class=\"top_pad2\" style=\"height: 20px; line-height: 20px; font-size: 68px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"left\" valign=\"top\"> <font class=\"mob_title1\" face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 35px; line-height: 55px; font-weight: 300; letter-spacing: -1.5px;\"> <span class=\"mob_title1\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 35px; line-height: 55px; font-weight: 300; letter-spacing: -1.5px;\">Hi Mohd Fadhil Fahmi</span> </font> <div style=\"height: 22px; line-height: 22px; font-size: 20px;\">&nbsp;</div> <font class=\"mob_title2\" face=\"'Source Sans Pro', sans-serif\" color=\"#5e5e5e\" style=\"font-size: 20px; line-height: 45px; font-weight: 300; letter-spacing: -1px;\"> <span class=\"mob_title2\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #5e5e5e; font-size: 20px; line-height: 45px; font-weight: 300; letter-spacing: -1px;\">Anda mempunyai beberapa perkara yang perlu diambil tindakan :</span> </font> </td> </tr> </table>";

        email.setBody(html);

        return email;
    }

    public static String emailTemplatePenyelia(LoginProfile log, String receiverName, String action, Leave ac, CoStaff toSt) throws Exception {

        CoStaff st = StaffDAO.getInfo(log, ac.getStaffID());

        String ayattindakan = "Anda mempunyai beberapa perkara yang perlu diambil tindakan :";

        String buttonTitle = "";
        String actiontext = "";

        if (action.equals("Approved")) {

            ayattindakan = "Cuti anda telah DILULUSKAN!";
            buttonTitle = "Lihat Cuti";

        } else if (action.equals("Preparing")) {

            buttonTitle = "Semak Cuti";
            actiontext = "DISEMAK";

        } else if (action.equals("Checked")) {

            buttonTitle = "Sokong Cuti";
            actiontext = "DISOKONG";

        } else if (action.equals("Supported")) {

            buttonTitle = "Lulus Cuti";
            actiontext = "DILULUSKAN";

        }/*else if(action.equals("Approved")){
            
            buttonTitle = "Sah Cuti";
            
        }*/ else if (action.equals("Rejected")) {

            ayattindakan = "Cuti anda TIDAK DILULUSKAN";
            buttonTitle = "Lihat Cuti";

        }

        //String receiverName = StaffDAO.getInfo(log, ac.getActionby()).getName();
        String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n"
                + "\n"
                + "<head>\n"
                + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n"
                + "    <!--[if !mso]--><!-- -->\n"
                + "    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n"
                + "    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n"
                + "    <!--<![endif]-->\n"
                + "\n"
                + "    <title>Material Design for Bootstrap</title>\n"
                + "\n"
                + "    <style type=\"text/css\">\n"
                + "        body {\n"
                + "            width: 100%;\n"
                + "            background-color: #ffffff;\n"
                + "            margin: 0;\n"
                + "            padding: 0;\n"
                + "            -webkit-font-smoothing: antialiased;\n"
                + "            mso-margin-top-alt: 0px;\n"
                + "            mso-margin-bottom-alt: 0px;\n"
                + "            mso-padding-alt: 0px 0px 0px 0px;\n"
                + "        }\n"
                + "\n"
                + "        p,\n"
                + "        h1,\n"
                + "        h2,\n"
                + "        h3,\n"
                + "        h4 {\n"
                + "            margin-top: 0;\n"
                + "            margin-bottom: 0;\n"
                + "            padding-top: 0;\n"
                + "            padding-bottom: 0;\n"
                + "            margin-left: 20px;\n"
                + "            margin-right: 0;\n"
                + "\n"
                + "        }\n"
                + "\n"
                + "        span.preheader {\n"
                + "            display: none;\n"
                + "            font-size: 1px;\n"
                + "        }\n"
                + "\n"
                + "        html {\n"
                + "            width: 100%;\n"
                + "        }\n"
                + "\n"
                + "        table {\n"
                + "            font-size: 14px;\n"
                + "            border: 0;\n"
                + "        }\n"
                + "\n"
                + ".img-circular{\n"
                + " width: 60px;\n"
                + " height: 60px;\n"
                + " background-image: url('https://lh3.googleusercontent.com/a-/AOh14GhhkFS82FtQpzPRV4_qRpHLn39S3ftJ8z62Ehn2Nw=s96-c');\n"
                + " background-size: cover;\n"
                + " display: block;\n"
                + " border-radius: 100px;\n"
                + " -webkit-border-radius: 100px;\n"
                + " -moz-border-radius: 100px;\n"
                + "}\n"
                + "\n"
                + ".img-circular2{\n"
                + " width: 60px;\n"
                + " height: 60px;\n"
                + " background-image: url('" + st.getImageURL() + "');\n"
                + " background-size: cover;\n"
                + " display: block;\n"
                + " border-radius: 100px;\n"
                + " -webkit-border-radius: 100px;\n"
                + " -moz-border-radius: 100px;\n"
                + "}\n"
                + "\n"
                + "        /* ----------- responsivity ----------- */\n"
                + "\n"
                + "        @media only screen and (max-width: 640px) {\n"
                + "            /*------ top header ------ */\n"
                + "            .main-header {\n"
                + "                font-size: 20px !important;\n"
                + "            }\n"
                + "            .main-section-header {\n"
                + "                font-size: 28px !important;\n"
                + "            }\n"
                + "            .show {\n"
                + "                display: block !important;\n"
                + "            }\n"
                + "            .hide {\n"
                + "                display: none !important;\n"
                + "            }\n"
                + "            .align-center {\n"
                + "                text-align: center !important;\n"
                + "            }\n"
                + "            .no-bg {\n"
                + "                background: none !important;\n"
                + "            }\n"
                + "            /*----- main image -------*/\n"
                + "            .main-image img {\n"
                + "                width: 440px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "            /* ====== divider ====== */\n"
                + "            .divider img {\n"
                + "                width: 440px !important;\n"
                + "            }\n"
                + "            /*-------- container --------*/\n"
                + "            .container590 {\n"
                + "                width: 440px !important;\n"
                + "            }\n"
                + "            .container580 {\n"
                + "                width: 400px !important;\n"
                + "            }\n"
                + "            .main-button {\n"
                + "                width: 220px !important;\n"
                + "            }\n"
                + "            /*-------- secions ----------*/\n"
                + "            .section-img img {\n"
                + "                width: 320px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "            .team-img img {\n"
                + "                width: 100% !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "        }\n"
                + "\n"
                + "        @media only screen and (max-width: 479px) {\n"
                + "            /*------ top header ------ */\n"
                + "            .main-header {\n"
                + "                font-size: 18px !important;\n"
                + "            }\n"
                + "            .main-section-header {\n"
                + "                font-size: 26px !important;\n"
                + "            }\n"
                + "            /* ====== divider ====== */\n"
                + "            .divider img {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            /*-------- container --------*/\n"
                + "            .container590 {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            .container590 {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            .container580 {\n"
                + "                width: 260px !important;\n"
                + "            }\n"
                + "            /*-------- secions ----------*/\n"
                + "            .section-img img {\n"
                + "                width: 280px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "        }\n"
                + "    </style>\n"
                + "    <!--[if gte mso 9]><style type=”text/css”>\n"
                + "        body {\n"
                + "        font-family: arial, sans-serif!important;\n"
                + "        }\n"
                + "        </style>\n"
                + "    <![endif]-->\n"
                + "</head>\n"
                + "\n"
                + "\n"
                + "<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n"
                + "    <!-- pre-header -->\n"
                + "    <!--<table style=\"display:none!important;\">\n"
                + "        <tr>\n"
                + "            <td>\n"
                + "                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n"
                + "                    Welcome to MDB!\n"
                + "                </div>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "    </table>-->\n"
                + "    <!-- pre-header end -->\n"
                + "    <!-- header -->\n"
                + "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n"
                + "\n"
                + "        <tr>\n"
                + "            <td align=\"center\">\n"
                + "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\">\n"
                + "\n"
                + "                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "\n"
                + "                                <tr>\n"
                + "                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
                + "                            class=\"main-header\">\n"
                + "                            <!-- section text ======-->\n"
                + "\n"
                + "                            <div style=\"line-height: 35px\">\n"
                + "\n"
                + "                                e<span style=\"color: #008000;\">Cuti</span>\n"
                + "\n"
                + "                            </div>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "\n"
                + "                                <tr>\n"
                + "                                    <td align=\"center\">\n"
                + "                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n"
                + "                                            class=\"container590 hide\">\n"
                + "                                            <tr>\n"
                + "                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                                    Sistem Permohonan Cuti Kakitangan LCSB\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                        </table>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "    </table>\n"
                + "    <!-- end header -->\n"
                + "\n"
                + "    <!-- big image section -->\n"
                + "\n"
                + "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\" >\n"
                + "\n"
                + "        <tr>\n"
                + "            <td align=\"center\">\n"
                + "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"background-color: #f0f5f5\">\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
                + "                            class=\"main-header\">\n"
                + "                            <!-- section text ======-->\n"
                + "                            \n"
                + "                            <!--<div class=\"img-circular\"></div>-->\n"
                + "                            <div style=\"line-height: 35px\"><h4>Assalamualaikum <span style=\"color: #008000;\"> " + receiverName + " </span></h4>\n"
                + "\n"
                + "                            </div>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\">\n"
                + "                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n"
                + "                                <tr>\n"
                + "                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"left\">\n"
                + "                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                        <!-- section text ======-->\n"
                + "\n"
                + "                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n"
                + "\n"
                + "                                            Tuan/Puan,\n"
                + "\n"
                + "                                        </p>\n"
                + "                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n"
                + "                                            Anda mempunyai satu permohonan cuti untuk " + actiontext + ".\n"
                + "                                        </p>\n"
                + "                                       \n"
                + "                                        \n"
                + "                                        \n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                        <!-- section text ======-->\n"
                + "\n"
                + "                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "                                        \n"
                + "                                        <tr>\n"
                + "                                                <td colspan=\"3\" align=\"center\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><div class=\"img-circular2\"></div></td>\n"
                + "                                                \n"
                + "                                            </tr>\n"
                + "                                        	<tr>\n"
                + "                                				<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Nama Pemohon</strong></p></td>\n"
                + "                                				<td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                				<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>" + st.getName() + "</strong></p></td>\n"
                + "                                			</tr>\n"
                + "                                			<tr>\n"
                + "                                				<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Jenis Cuti</strong></p></td>\n"
                + "                                				<td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                				<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>" + LeaveDAO.getLeaveType(log, ac.getType()).getName() + "</strong></p></td>\n"
                + "                                			</tr>\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Tarikh Bercuti</strong></p></td>\n"
                + "                                                <td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>" + AccountingPeriod.fullDateMonth(ac.getDatestart()) + " hingga " + AccountingPeriod.fullDateMonth(ac.getDateend()) + "</strong></p></td>\n"
                + "                                            </tr>\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Sebab Bercuti</strong></p></td>\n"
                + "                                                <td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>" + ac.getReason() + "</strong></p></td>\n"
                + "                                            </tr>\n"
                + "                                            \n"
                + "                                		</table>\n"
                + "                                		<p>&nbsp;</p>\n"
                + "                                		 <p style=\"line-height: 24px; margin-bottom:20px;\">\n"
                + "                                            Anda boleh mengakses cuti dan rekod dengan menekan butang seperti di bawah.\n"
                + "                                        </p>\n"
                + "                                        \n"
                + "                                        \n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                	<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                		<br>\n"
                + "                                		<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#008000\" style=\"margin-bottom:20px;\">\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n"
                + "                                                    <!-- main section button -->\n"
                + "\n"
                + "                                                    <div style=\"line-height: 22px;\">\n"
                + "                                                        <a href=\"https://ecuti.lcsb.com.my/Login?linkTo=viewleaveapproval&leaveID=" + ac.getLeaveID() + "\" style=\"color: #ffffff; text-decoration: none;\">" + buttonTitle + "</a>\n"
                + "                                                    </div>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                        </table>\n"
                + "                                		<p>&nbsp;</p>\n"
                + "                                		<p style=\"line-height: 24px\">\n"
                + "                                            Yang Benar,\n"
                + "                                        </p>\n"
                + "										<p style=\"line-height: 24px\">\n"
                + "                                            <strong>LCSB Bot</strong>\n"
                + "                                        </p>\n"
                + "                                        <p style=\"line-height: 24px\">\n"
                + "                                            Pentadbir Maya Sistem eCuti\n"
                + "                                        </p>\n"
                + "\n"
                + "                                	</td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "                </table>\n"
                + "\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "\n"
                + "        <tr>\n"
                + "            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n"
                + "        </tr>\n"
                + "\n"
                + "    </table>\n"
                + "\n"
                + "    <!-- end section -->\n"
                + "\n"
                + "\n"
                + "    <!-- main section -->\n"
                + "    \n"
                + "\n"
                + "    <!-- end section -->\n"
                + "\n"
                + "    \n"
                + "    <!-- end section -->\n"
                + "\n"
                + "    <!-- footer ====== -->\n"
                + "    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n"
                + "    <!-- end footer ====== -->\n"
                + "\n"
                + "</body>\n"
                + "\n"
                + "</html>";

        return html;
    }

    public static String emailTemplatePemohon(LoginProfile log, String receiverName, String action, Leave ac, CoStaff toSt) throws Exception {

        CoStaff st = StaffDAO.getInfo(log, ac.getStaffID());

        String ayattindakan = "Anda mempunyai beberapa perkara yang perlu diambil tindakan :";

        String buttonTitle = "";
        String actiontext = "";

        if (action.equals("Approved")) {
            ayattindakan = "Tahniah!, Cuti anda telah <span style=\"color: #008000;\"><b>DILULUSKAN!</b></span>";
            buttonTitle = "Lihat Cuti";

        } else if (action.equals("Preparing")) {

            buttonTitle = "Semak Cuti";
            actiontext = "DISEMAK";

        } else if (action.equals("Checked")) {

            buttonTitle = "Sokong Cuti";
            actiontext = "DISOKONG";

        } else if (action.equals("Supported")) {

            buttonTitle = "Lulus Cuti";
            actiontext = "DILULUSKAN";

        }/*else if(action.equals("Approved")){
            
            buttonTitle = "Sah Cuti";
            
        }*/ else if (action.equals("Rejected")) {
            ayattindakan = "Cuti anda <span style=\"color: red;\"><b>TIDAK DILULUSKAN!</b></span>";
            buttonTitle = "Lihat Cuti";

        }

        //String receiverName = StaffDAO.getInfo(log, ac.getActionby()).getName();
        String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n"
                + "\n"
                + "<head>\n"
                + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n"
                + "    <!--[if !mso]--><!-- -->\n"
                + "    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n"
                + "    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n"
                + "    <!--<![endif]-->\n"
                + "\n"
                + "    <title>Material Design for Bootstrap</title>\n"
                + "\n"
                + "    <style type=\"text/css\">\n"
                + "        body {\n"
                + "            width: 100%;\n"
                + "            background-color: #ffffff;\n"
                + "            margin: 0;\n"
                + "            padding: 0;\n"
                + "            -webkit-font-smoothing: antialiased;\n"
                + "            mso-margin-top-alt: 0px;\n"
                + "            mso-margin-bottom-alt: 0px;\n"
                + "            mso-padding-alt: 0px 0px 0px 0px;\n"
                + "        }\n"
                + "\n"
                + "        p,\n"
                + "        h1,\n"
                + "        h2,\n"
                + "        h3,\n"
                + "        h4 {\n"
                + "            margin-top: 0;\n"
                + "            margin-bottom: 0;\n"
                + "            padding-top: 0;\n"
                + "            padding-bottom: 0;\n"
                + "            margin-left: 20px;\n"
                + "            margin-right: 20px;\n"
                + "\n"
                + "        }\n"
                + "\n"
                + "        span.preheader {\n"
                + "            display: none;\n"
                + "            font-size: 1px;\n"
                + "        }\n"
                + "\n"
                + "        html {\n"
                + "            width: 100%;\n"
                + "        }\n"
                + "\n"
                + "        table {\n"
                + "            font-size: 14px;\n"
                + "            border: 0;\n"
                + "        }\n"
                + "\n"
                + ".img-circular{\n"
                + " width: 60px;\n"
                + " height: 60px;\n"
                + " background-image: url('" + st.getImageURL() + "');\n"
                + " background-size: cover;\n"
                + " display: block;\n"
                + " border-radius: 100px;\n"
                + " -webkit-border-radius: 100px;\n"
                + " -moz-border-radius: 100px;\n"
                + "}\n"
                + "\n"
                + "        /* ----------- responsivity ----------- */\n"
                + "\n"
                + "        @media only screen and (max-width: 640px) {\n"
                + "            /*------ top header ------ */\n"
                + "            .main-header {\n"
                + "                font-size: 20px !important;\n"
                + "            }\n"
                + "            .main-section-header {\n"
                + "                font-size: 28px !important;\n"
                + "            }\n"
                + "            .show {\n"
                + "                display: block !important;\n"
                + "            }\n"
                + "            .hide {\n"
                + "                display: none !important;\n"
                + "            }\n"
                + "            .align-center {\n"
                + "                text-align: center !important;\n"
                + "            }\n"
                + "            .no-bg {\n"
                + "                background: none !important;\n"
                + "            }\n"
                + "            /*----- main image -------*/\n"
                + "            .main-image img {\n"
                + "                width: 440px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "            /* ====== divider ====== */\n"
                + "            .divider img {\n"
                + "                width: 440px !important;\n"
                + "            }\n"
                + "            /*-------- container --------*/\n"
                + "            .container590 {\n"
                + "                width: 440px !important;\n"
                + "            }\n"
                + "            .container580 {\n"
                + "                width: 400px !important;\n"
                + "            }\n"
                + "            .main-button {\n"
                + "                width: 220px !important;\n"
                + "            }\n"
                + "            /*-------- secions ----------*/\n"
                + "            .section-img img {\n"
                + "                width: 320px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "            .team-img img {\n"
                + "                width: 100% !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "        }\n"
                + "\n"
                + "        @media only screen and (max-width: 479px) {\n"
                + "            /*------ top header ------ */\n"
                + "            .main-header {\n"
                + "                font-size: 18px !important;\n"
                + "            }\n"
                + "            .main-section-header {\n"
                + "                font-size: 26px !important;\n"
                + "            }\n"
                + "            /* ====== divider ====== */\n"
                + "            .divider img {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            /*-------- container --------*/\n"
                + "            .container590 {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            .container590 {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            .container580 {\n"
                + "                width: 260px !important;\n"
                + "            }\n"
                + "            /*-------- secions ----------*/\n"
                + "            .section-img img {\n"
                + "                width: 280px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "        }\n"
                + "    </style>\n"
                + "    <!--[if gte mso 9]><style type=”text/css”>\n"
                + "        body {\n"
                + "        font-family: arial, sans-serif!important;\n"
                + "        }\n"
                + "        </style>\n"
                + "    <![endif]-->\n"
                + "</head>\n"
                + "\n"
                + "\n"
                + "<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n"
                + "    <!-- pre-header -->\n"
                + "    <!--<table style=\"display:none!important;\">\n"
                + "        <tr>\n"
                + "            <td>\n"
                + "                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n"
                + "                    Welcome to MDB!\n"
                + "                </div>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "    </table>-->\n"
                + "    <!-- pre-header end -->\n"
                + "    <!-- header -->\n"
                + "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n"
                + "\n"
                + "        <tr>\n"
                + "            <td align=\"center\">\n"
                + "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\">\n"
                + "\n"
                + "                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "\n"
                + "                                <tr>\n"
                + "                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
                + "                            class=\"main-header\">\n"
                + "                            <!-- section text ======-->\n"
                + "\n"
                + "                            <div style=\"line-height: 35px\">\n"
                + "\n"
                + "                                e<span style=\"color: #008000;\">Cuti</span>\n"
                + "\n"
                + "                            </div>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "\n"
                + "                                <tr>\n"
                + "                                    <td align=\"center\">\n"
                + "                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n"
                + "                                            class=\"container590 hide\">\n"
                + "                                            <tr>\n"
                + "                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                                    Sistem Permohonan Cuti LCSB\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                        </table>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "    </table>\n"
                + "    <!-- end header -->\n"
                + "\n"
                + "    <!-- big image section -->\n"
                + "\n"
                + "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\" >\n"
                + "\n"
                + "        <tr>\n"
                + "            <td align=\"center\">\n"
                + "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"background-color: #f0f5f5\">\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
                + "                            class=\"main-header\">\n"
                + "                            <!-- section text ======-->\n"
                + "                            \n"
                + "                            <div class=\"img-circular\"></div>\n"
                + "                            <div style=\"line-height: 35px\">\n"
                + "\n"
                + "                                <h4>Assalamualaikum <span style=\"color: #008000;\">" + st.getName() + "</span></h4>\n"
                + "\n"
                + "                            </div>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\">\n"
                + "                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n"
                + "                                <tr>\n"
                + "                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"left\">\n"
                + "                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                        <!-- section text ======-->\n"
                + "\n"
                + "                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n"
                + "\n"
                + "                                            Tuan/Puan,\n"
                + "\n"
                + "                                        </p>\n"
                + "                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n"
                + "                                            " + ayattindakan + "\n"
                + "                                        </p>\n"
                + "                                       \n"
                + "                                        \n"
                + "                                        \n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                        <!-- section text ======-->\n"
                + "\n"
                + "                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "                                        \n"
                + "                                			<tr>\n"
                + "                                				<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Jenis Cuti</strong></p></td>\n"
                + "                                				<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                				<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>" + LeaveDAO.getLeaveType(log, ac.getType()).getName() + "</strong></p></td>\n"
                + "\n"
                + "\n"
                + "                                			</tr>\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Tarikh Bercuti</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>" + AccountingPeriod.fullDateMonth(ac.getDatestart()) + " hingga " + AccountingPeriod.fullDateMonth(ac.getDateend()) + "</strong></p></td>\n"
                + "\n"
                + "                                            </tr>\n"
                + "                                            \n"
                + "                                		</table>\n"
                + "                                		<p>&nbsp;</p>\n"
                + "                                		 <p style=\"line-height: 24px; margin-bottom:20px;\">\n"
                + "                                            Anda boleh mengakses cuti dan rekod dengan menekan butang seperti di bawah.\n"
                + "                                        </p>\n"
                + "                                        \n"
                + "                                        \n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                	<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                		<br>\n"
                + "                                		<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#008000\" style=\"margin-bottom:20px;\">\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n"
                + "                                                    <!-- main section button -->\n"
                + "\n"
                + "                                                    <div style=\"line-height: 22px;\">\n"
                + "                                                        <a href=\"https://ecuti.lcsb.com.my/Login?linkTo=viewleaveapproval&leaveID=" + ac.getLeaveID() + "\" style=\"color: #ffffff; text-decoration: none;\">" + buttonTitle + "</a>\n"
                + "                                                    </div>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                        </table>\n"
                + "                                		<p>&nbsp;</p>\n"
                + "                                		<p style=\"line-height: 24px\">\n"
                + "                                            Yang Benar,\n"
                + "                                        </p>\n"
                + "										<p style=\"line-height: 24px\">\n"
                + "                                            <strong>LCSB Bot</strong>\n"
                + "                                        </p>\n"
                + "                                        <p style=\"line-height: 24px\">\n"
                + "                                            Pentadbir Maya Sistem eCuti\n"
                + "                                        </p>\n"
                + "\n"
                + "                                	</td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "                </table>\n"
                + "\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "\n"
                + "        <tr>\n"
                + "            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n"
                + "        </tr>\n"
                + "\n"
                + "    </table>\n"
                + "\n"
                + "    <!-- end section -->\n"
                + "\n"
                + "\n"
                + "    <!-- main section -->\n"
                + "    \n"
                + "\n"
                + "    <!-- end section -->\n"
                + "\n"
                + "    \n"
                + "    <!-- end section -->\n"
                + "\n"
                + "    <!-- footer ====== -->\n"
                + "    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n"
                + "    <!-- end footer ====== -->\n"
                + "\n"
                + "</body>\n"
                + "\n"
                + "</html>";

        return html;
    }

    public static String emailTemplateKomen(LoginProfile log, Leave ac, CoStaff toSt) throws Exception {
        
        LeaveComment lc = LeaveDAO.getLeaveCommentLast(log, ac.getLeaveID());

        //CoStaff st = StaffDAO.getInfo(log, ac.getStaffID());

        //String receiverName = StaffDAO.getInfo(log, ac.getActionby()).getName();
        String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n"
                + "\n"
                + "<head>\n"
                + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n"
                + "    <!--[if !mso]--><!-- -->\n"
                + "    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n"
                + "    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n"
                + "    <!--<![endif]-->\n"
                + "\n"
                + "    <title>Material Design for Bootstrap</title>\n"
                + "\n"
                + "    <style type=\"text/css\">\n"
                + "        body {\n"
                + "            width: 100%;\n"
                + "            background-color: #ffffff;\n"
                + "            margin: 0;\n"
                + "            padding: 0;\n"
                + "            -webkit-font-smoothing: antialiased;\n"
                + "            mso-margin-top-alt: 0px;\n"
                + "            mso-margin-bottom-alt: 0px;\n"
                + "            mso-padding-alt: 0px 0px 0px 0px;\n"
                + "        }\n"
                + "\n"
                + "        p,\n"
                + "        h1,\n"
                + "        h2,\n"
                + "        h3,\n"
                + "        h4 {\n"
                + "            margin-top: 0;\n"
                + "            margin-bottom: 0;\n"
                + "            padding-top: 0;\n"
                + "            padding-bottom: 0;\n"
                + "            margin-left: 20px;\n"
                + "            margin-right: 20px;\n"
                + "\n"
                + "        }\n"
                + "\n"
                + "        span.preheader {\n"
                + "            display: none;\n"
                + "            font-size: 1px;\n"
                + "        }\n"
                + "\n"
                + "        html {\n"
                + "            width: 100%;\n"
                + "        }\n"
                + "\n"
                + "        table {\n"
                + "            font-size: 14px;\n"
                + "            border: 0;\n"
                + "        }\n"
                + "\n"
                + ".img-circular{\n"
                + " width: 60px;\n"
                + " height: 60px;\n"
                + " background-image: url('"+toSt.getImageURL()+"');\n"
                + " background-size: cover;\n"
                + " display: block;\n"
                + " border-radius: 100px;\n"
                + " -webkit-border-radius: 100px;\n"
                + " -moz-border-radius: 100px;\n"
                + "}\n"
                + "\n"
                + "        /* ----------- responsivity ----------- */\n"
                + "\n"
                + "        @media only screen and (max-width: 640px) {\n"
                + "            /*------ top header ------ */\n"
                + "            .main-header {\n"
                + "                font-size: 20px !important;\n"
                + "            }\n"
                + "            .main-section-header {\n"
                + "                font-size: 28px !important;\n"
                + "            }\n"
                + "            .show {\n"
                + "                display: block !important;\n"
                + "            }\n"
                + "            .hide {\n"
                + "                display: none !important;\n"
                + "            }\n"
                + "            .align-center {\n"
                + "                text-align: center !important;\n"
                + "            }\n"
                + "            .no-bg {\n"
                + "                background: none !important;\n"
                + "            }\n"
                + "            /*----- main image -------*/\n"
                + "            .main-image img {\n"
                + "                width: 440px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "            /* ====== divider ====== */\n"
                + "            .divider img {\n"
                + "                width: 440px !important;\n"
                + "            }\n"
                + "            /*-------- container --------*/\n"
                + "            .container590 {\n"
                + "                width: 440px !important;\n"
                + "            }\n"
                + "            .container580 {\n"
                + "                width: 400px !important;\n"
                + "            }\n"
                + "            .main-button {\n"
                + "                width: 220px !important;\n"
                + "            }\n"
                + "            /*-------- secions ----------*/\n"
                + "            .section-img img {\n"
                + "                width: 320px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "            .team-img img {\n"
                + "                width: 100% !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "        }\n"
                + "\n"
                + "        @media only screen and (max-width: 479px) {\n"
                + "            /*------ top header ------ */\n"
                + "            .main-header {\n"
                + "                font-size: 18px !important;\n"
                + "            }\n"
                + "            .main-section-header {\n"
                + "                font-size: 26px !important;\n"
                + "            }\n"
                + "            /* ====== divider ====== */\n"
                + "            .divider img {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            /*-------- container --------*/\n"
                + "            .container590 {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            .container590 {\n"
                + "                width: 280px !important;\n"
                + "            }\n"
                + "            .container580 {\n"
                + "                width: 260px !important;\n"
                + "            }\n"
                + "            /*-------- secions ----------*/\n"
                + "            .section-img img {\n"
                + "                width: 280px !important;\n"
                + "                height: auto !important;\n"
                + "            }\n"
                + "        }\n"
                + "    </style>\n"
                + "    <!--[if gte mso 9]><style type=”text/css”>\n"
                + "        body {\n"
                + "        font-family: arial, sans-serif!important;\n"
                + "        }\n"
                + "        </style>\n"
                + "    <![endif]-->\n"
                + "</head>\n"
                + "\n"
                + "\n"
                + "<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n"
                + "    <!-- pre-header -->\n"
                + "    <!--<table style=\"display:none!important;\">\n"
                + "        <tr>\n"
                + "            <td>\n"
                + "                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n"
                + "                    Welcome to MDB!\n"
                + "                </div>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "    </table>-->\n"
                + "    <!-- pre-header end -->\n"
                + "    <!-- header -->\n"
                + "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n"
                + "\n"
                + "        <tr>\n"
                + "            <td align=\"center\">\n"
                + "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\">\n"
                + "\n"
                + "                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "\n"
                + "                                <tr>\n"
                + "                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
                + "                            class=\"main-header\">\n"
                + "                            <!-- section text ======-->\n"
                + "\n"
                + "                            <div style=\"line-height: 35px\">\n"
                + "\n"
                + "                                e<span style=\"color: #008000;\">Cuti</span>\n"
                + "\n"
                + "                            </div>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "\n"
                + "                                <tr>\n"
                + "                                    <td align=\"center\">\n"
                + "                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n"
                + "                                            class=\"container590 hide\">\n"
                + "                                            <tr>\n"
                + "                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                                    Sistem Permohonan Cuti LCSB\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                        </table>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "    </table>\n"
                + "    <!-- end header -->\n"
                + "\n"
                + "    <!-- big image section -->\n"
                + "\n"
                + "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\" >\n"
                + "\n"
                + "        <tr>\n"
                + "            <td align=\"center\">\n"
                + "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"background-color: #f0f5f5\">\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
                + "                            class=\"main-header\">\n"
                + "                            <!-- section text ======-->\n"
                + "                            \n"
                + "                            <div class=\"img-circular\"></div>\n"
                + "                            <div style=\"line-height: 35px\">\n"
                + "\n"
                + "                                <h4>Assalamualaikum <span style=\"color: #008000;\">"+ GeneralTerm.capitalizeFirstLetter(toSt.getName()) +"</span></h4>\n"
                + "\n"
                + "                            </div>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\">\n"
                + "                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n"
                + "                                <tr>\n"
                + "                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n"
                + "                    </tr>\n"
                + "\n"
                + "                    <tr>\n"
                + "                        <td align=\"left\">\n"
                + "                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                        <!-- section text ======-->\n"
                + "\n"
                + "                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n"
                + "\n"
                + "                                            Tuan/Puan,\n"
                + "\n"
                + "                                        </p>\n"
                + "                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n"
                + "                                            Anda mempunyai satu komen di permohonan cuti.\n"
                + "                                        </p>\n"
                + "                                       \n"
                + "                                        \n"
                + "                                        \n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                        <!-- section text ======-->\n"
                + "\n"
                + "                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
                + "                                        \n"
                + "                                			<tr>\n"
                + "                                				<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Dikomen oleh</strong></p></td>\n"
                + "                                				<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                				<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"+GeneralTerm.capitalizeFirstLetter(StaffDAO.getInfo(log, lc.getStaffID()).getName())+"</strong></p></td>\n"
                + "\n"
                + "\n"
                + "                                			</tr>\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Pada</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"+AccountingPeriod.fullDateMonth(lc.getDate())+", "+lc.getTime()+"</strong></p></td>\n"
                + "\n"
                + "                                            </tr>\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Komentar</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n"
                + "                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>\""+lc.getComment()+"\"</strong></p></td>\n"
                + "\n"
                + "                                            </tr>\n"
                + "                                            \n"
                + "                                		</table>\n"
                + "                                        \n"
                + "                                        \n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                	<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
                + "                                		<br>\n"
                + "                                		<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#008000\" style=\"margin-bottom:20px;\">\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n"
                + "                                                    <!-- main section button -->\n"
                + "\n"
                + "                                                    <div style=\"line-height: 22px;\">\n"
                + "                                                        <a href=\"https://ecuti.lcsb.com.my/Login?linkTo=viewleaveapproval&leaveID="+lc.getLeaveID()+"\" style=\"color: #ffffff; text-decoration: none;\">Lihat Komen</a>\n"
                + "                                                    </div>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                            <tr>\n"
                + "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
                + "                                            </tr>\n"
                + "\n"
                + "                                        </table>\n"
                + "                                		<p>&nbsp;</p>\n"
               // + "                                		<p style=\"line-height: 24px\">\n"
                //+ "                                            Yang Benar,\n"
               // + "                                        </p>\n"
                //+ "										<p style=\"line-height: 24px\">\n"
                //+ "                                            <strong>Khairul Annuar bn Samad</strong>\n"
                //+ "                                        </p>\n"
                //+ "                                        <p style=\"line-height: 24px\">\n"
                //+ "                                            Pengurus Bahagian Pentadbiran & Sumber Manusia\n"
                //+ "                                        </p>\n"
                + "\n"
                + "                                	</td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "                </table>\n"
                + "\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "\n"
                + "        <tr>\n"
                + "            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n"
                + "        </tr>\n"
                + "\n"
                + "    </table>\n"
                + "\n"
                + "    <!-- end section -->\n"
                + "\n"
                + "\n"
                + "    <!-- main section -->\n"
                + "    \n"
                + "\n"
                + "    <!-- end section -->\n"
                + "\n"
                + "    \n"
                + "    <!-- end section -->\n"
                + "\n"
                + "    <!-- footer ====== -->\n"
                + "    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n"
                + "    <!-- end footer ====== -->\n"
                + "\n"
                + "</body>\n"
                + "\n"
                + "</html>";

        return html;
    }
    
    public static void sendCommentEmail(LoginProfile log, String leaveID, String replyCommentID) throws IOException, Exception {

        
        //for (AuthActionTable j : ac) {
        Leave l =  LeaveDAO.getLeaveInfoDetail(log, leaveID);
        CoStaff co =  null;
        
        if(replyCommentID.equals("None")){
            co = StaffDAO.getInfo(log, l.getStaffID());
        }else{
            co = StaffDAO.getInfo(log, LeaveDAO.getLeaveCommentByCommentID(log, replyCommentID).getStaffID());
        }
        
        Email email = (Email) EmailDAO.emailTemplate(log, l, co, "comment");
        
        String emailadd = co.getEmail();
        //String emailadd = "fadhilfahmi@lcsb.com.my";

        String escaped = StringEscapeUtils.escapeJson(email.getBody());

        //JSON String need to be constructed for the specific resource. 
        //We may construct complex JSON using any third-party JSON libraries such as jackson or org.json
        String jsonInputString = "{\"to_address\": \"" + emailadd + "\", \"subject\": \"[Notifikasi] Komentar Cuti \", \"senderEmail\": \"ecuti@lcsb.com.my\", \"senderPassword\": \"@dminEcuti\", \"body\": \"" + escaped + "\"}";

        //Logger.getLogger(EmailDAO.class.getName()).log(Level.INFO, jsonInputString);
        //Change the URL with any other publicly accessible POST resource, which accepts JSON request body
        URL url = new URL("http://192.168.254.200:8087/send");

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");

        con.setDoOutput(true);

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int code = con.getResponseCode();
        System.out.println(code);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }

        // }
    }

}
