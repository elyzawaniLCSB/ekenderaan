/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author fadhilfahmi
 */
public class DateAndTime {

    public static String addTime(String time) throws ParseException {//add 8 hours to palarel with server time

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date d = df.parse(time);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        //cal.add(Calendar.MINUTE, 475);
        return df.format(cal.getTime());

    }

    public static String dateIETFFormat() {
    Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            return dateFormat.format(calendar.getTime());
        
    }
    
    public static String convertDateToIETFFormat(String oridate) {
    Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            return dateFormat.format(oridate);
        
    }

    public static long getDiffBetweenTwoDates(String date1, String date2) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(date1);
        Date secondDate = sdf.parse(date2);

        long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        if (diff == 0) {
            diff = 1;
        }

        return diff;
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    public static String timeFormatHourMinute(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(time);
    }

    public static String fullDateMonth(String strDate) throws ParseException {
        if (strDate.equals("") || strDate == null || strDate == "null") {
            strDate = "0000-00-00";
        }

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = format1.parse(strDate);
        return format2.format(date);

    }

    public static Date convertStringtoDate(String strDate) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        if (strDate.equals("") || strDate.equals("0000-00-00") || strDate == null || strDate == "null" || strDate.equals("null") || strDate.equals(null)) {
            Date date = formatter.parse("0000-00-00");
            return date;
        } else {
            Date date = formatter.parse(strDate);
            return date;
        }

    }

}
