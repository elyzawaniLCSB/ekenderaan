/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.model.CoStaff;
import com.lcsb.ekenderaan.model.CoStaffDepartment;
import com.lcsb.ekenderaan.model.CoStaffLocation;
import com.lcsb.ekenderaan.model.CoStaffPosition;
import com.lcsb.ekenderaan.model.Executive;
import com.lcsb.ekenderaan.model.ExecutiveChild;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.SuperviseInfo;
import com.lcsb.ekenderaan.model.SysViewAs;
import com.lcsb.ekenderaan.model.UserAccess;
import static java.lang.Math.log;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class StaffDAO {

    public static CoStaff getInfo(LoginProfile log, String no) throws SQLException, Exception {
        CoStaff c = new CoStaff();

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE staffid=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getInfo(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static CoStaff getInfoByEmail(LoginProfile log, String no) throws SQLException, Exception {
        CoStaff c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE email=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }

    public static CoStaff getInfo(ResultSet rs) throws SQLException {

        CoStaff g = new CoStaff();

        g.setAddress(rs.getString("address"));
        g.setBirth(rs.getString("birth"));
        g.setCitizen(rs.getString("citizen"));
        g.setCity(rs.getString("city"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setHp(rs.getString("hp"));
        g.setMarital(rs.getString("marital"));
        g.setName(rs.getString("name"));
        g.setIc(rs.getString("ic"));
        g.setPhone(rs.getString("phone"));
        g.setPostcode(rs.getString("postcode"));
        g.setRace(rs.getString("race"));
        g.setReligion(rs.getString("religion"));
        g.setSex(rs.getString("sex"));
        g.setState(rs.getString("state"));
        g.setStatus(rs.getString("status"));
        g.setStaffid(rs.getString("staffid"));
        g.setPobirth(rs.getString("pobirth"));
        g.setRemarks(rs.getString("remarks"));
        g.setOldic(rs.getString("oldic"));
        g.setTitle(rs.getString("title"));
        g.setStaffid(rs.getString("staffid"));
        g.setDepartment(rs.getString("department"));
        g.setPosition(rs.getString("position"));
        g.setImageURL(rs.getString("imageURL"));
        g.setLocation(rs.getString("location"));
        g.setDepartmentID(rs.getString("departmentID"));
        g.setLocID(rs.getString("locID"));

        return g;
    }

    public static Executive getInfoExec(ResultSet rm) throws SQLException {

        Executive e = new Executive();

        e.setStaffid(rm.getString("staffid"));
        e.setNokp(rm.getString("nokp"));
        e.setDob(rm.getString("dob"));
        e.setMarital(rm.getString("marital"));
        e.setBirthplace(rm.getString("birthplace"));
        e.setBlood(rm.getString("blood"));
        e.setDepartment(rm.getString("department"));
        e.setNoepf(rm.getString("noepf"));
        e.setNosocso(rm.getString("nosocso"));
        e.setTaxno(rm.getString("taxno"));
        e.setReligion(rm.getString("religion"));
        e.setWorkdate(rm.getString("workdate"));
        e.setSpouse(rm.getString("spouse"));
        e.setSpousework(rm.getString("spousework"));
        e.setRace(rm.getString("race"));
        e.setNokpspouse(rm.getString("nokpspouse"));
        e.setFixaddress(rm.getString("fixaddress"));
        e.setAddress(rm.getString("address"));
        e.setNamawaris1(rm.getString("namawaris1"));
        e.setAlamatwaris1(rm.getString("alamatwaris1"));
        e.setNotelwaris1(rm.getString("notelwaris1"));
        e.setHubunganwaris1(rm.getString("hubunganwaris1"));
        e.setNamawaris2(rm.getString("namawaris2"));
        e.setAlamatwaris2(rm.getString("alamatwaris2"));
        e.setNotelwaris2(rm.getString("notelwaris2"));
        e.setHubunganwaris2(rm.getString("hubunganwaris2"));
        e.setBilanak(Integer.parseInt("bilanak"));
        e.setPosition(rm.getString("position"));

        return e;
    }

    public static void saveInfoExec(LoginProfile log, Executive e) throws Exception {

        String q = ("INSERT INTO executive(staffid,nokp,dob,marital,birthplace,blood,department,noepf,nosocso,taxno,"
                + "religion,race,workdate,spouse,spousework,nokpspouse,fixaddress,address,namawaris1,alamatwaris1,notelwaris1,hubunganwaris1,namawaris2,alamatwaris2,notelwaris2,hubunganwaris2,bilanak,position) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, e.getStaffid());
        ps.setString(2, e.getNokp());
        ps.setString(3, e.getDob());
        ps.setString(4, e.getMarital());
        ps.setString(5, e.getBirthplace());
        ps.setString(6, e.getBlood());
        ps.setString(7, e.getDepartment());
        ps.setString(8, e.getNoepf());
        ps.setString(9, e.getNosocso());
        ps.setString(10, e.getTaxno());
        ps.setString(11, e.getReligion());
        ps.setString(12, e.getRace());
        ps.setString(13, e.getWorkdate());
        ps.setString(14, e.getSpouse());
        ps.setString(15, e.getSpousework());
        ps.setString(16, e.getNokpspouse());
        ps.setString(17, e.getFixaddress());
        ps.setString(18, e.getAddress());
        ps.setString(19, e.getNamawaris1());
        ps.setString(20, e.getAlamatwaris1());
        ps.setString(21, e.getHubunganwaris1());
        ps.setString(22, e.getNotelwaris1());
        ps.setString(23, e.getNamawaris2());
        ps.setString(24, e.getAlamatwaris2());
        ps.setString(25, e.getHubunganwaris2());
        ps.setString(26, e.getNotelwaris2());
        ps.setInt(27, e.getBilanak());
        ps.setString(28, e.getPosition());

        ps.executeUpdate();
        ps.close();

    }

    public static ExecutiveChild getInfoExecChild(ResultSet rs) throws SQLException {

        ExecutiveChild ec = new ExecutiveChild();

        ec.setStaffid(rs.getString("staffid"));
        ec.setEducationlevel(rs.getString("educationlevel"));
        ec.setNamaanak(rs.getString("namaanak"));
        ec.setNokpanak(rs.getString("nokpanak"));
        ec.setOku(rs.getString("oku"));
        ec.setId(Integer.parseInt("id"));
        ec.setUmur(Integer.parseInt("umur"));

        return ec;

    }

    public static void saveInfoExecChild(LoginProfile log, ExecutiveChild ec) throws Exception {

        String q1 = ("INSERT into executive_child(staffID,educationlevel,namaanak,nokpanak,oku,id,umur) values (?,?,?,?,?,?,?)");
        PreparedStatement ps1 = log.getCon().prepareStatement(q1);
        ps1.setString(1, ec.getEducationlevel());
        ps1.setString(2, ec.getNamaanak());
        ps1.setString(3, ec.getNokpanak());
        ps1.setString(4, ec.getOku());
        ps1.setString(5, ec.getStaffid());
        ps1.setInt(6, ec.getId());
        ps1.setInt(7, ec.getUmur());

        ps1.executeUpdate();
        ps1.close();

    }

    public static void changePerson(LoginProfile log, String staffID) throws Exception {

        CoStaff co = (CoStaff) StaffDAO.getInfo(log, staffID);

        String q = ("UPDATE co_staff SET staffid = ?, locID = ?, departmentID = ? WHERE email = ?");
        PreparedStatement ps = log.getCon().prepareStatement(q);

        ps.setString(1, co.getStaffid());
        ps.setString(2, co.getLocID());
        ps.setString(3, co.getDepartmentID());
        ps.setString(4, log.getEmail());

        ps.executeUpdate();
        ps.close();

        String q1 = ("INSERT into sys_view_as(staffID,locID,deptID,email) values (?,?,?,?)");
        PreparedStatement ps1 = log.getCon().prepareStatement(q1);
        ps1.setString(1, log.getUserID());
        ps1.setString(2, log.getLocID());
        ps1.setString(3, log.getDeptID());
        ps1.setString(4, log.getEmail());

        ps1.executeUpdate();
        ps1.close();

    }

    public static void resetViewAs(LoginProfile log) throws Exception {

        SysViewAs co = (SysViewAs) StaffDAO.getInfoSysViewAs(log, log.getEmail());

        String q = ("UPDATE co_staff SET staffid = ?, locID = ?, departmentID = ? WHERE email = ?");
        PreparedStatement ps = log.getCon().prepareStatement(q);

        ps.setString(1, co.getStaffID());
        ps.setString(2, co.getLocID());
        ps.setString(3, co.getDeptID());
        ps.setString(4, log.getEmail());

        ps.executeUpdate();
        ps.close();

        String q1 = ("DELETE FROM sys_view_as WHERE email = ?");
        PreparedStatement ps1 = log.getCon().prepareStatement(q1);
        ps1.setString(1, log.getEmail());

        ps1.executeUpdate();
        ps1.close();

    }

    public static List<CoStaff> getAllStaff(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff where locID = '" + log.getLocID() + "'");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffList(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        String q = "";

        if (log.getLocID().equals("9911")) {
            q = "where (locID = '9911' OR (locID <> '9911' AND departmentID IN ('0003','0004'))) ";
        } else {
            q = "where locID = '" + log.getLocID() + "'";
        }

        if (log.getUserID().equals("P0703") || log.getUserID().equals("P0702")) {
            q = "";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff  " + q + "");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffListOrderByPosition(LoginProfile log) {


        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        String q = "";

        if (log.getLocID().equals("9911")) {
            q = "AND departmentID = '" + log.getDeptID()+ "' ";
        } else {
            q = "AND locID = '" + log.getLocID() + "'";
        }

        if (log.getUserID().equals("P0703") || log.getUserID().equals("P0702")) {
            q = "";
        }

//        if (log.getUserID().equals("P0867") || log.getUserID().equals("P0812") ){
//            q = "AND departmentID = '" + log.getDeptID()+ "' ";
//        }
        
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_position a, co_staff b where a.code = b.postid " + q + " order by a.level");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---newquery-- " + stmt);
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffIntro(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffWithEmail(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff where email <> ''");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffWithByDeptWithLeave(LoginProfile log, String dept) {

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        String q = "";
        if (log.getAccessLevel() == 1) {
            q = "  AND (locID = '" + log.getLocID() + "' OR (departmentID IN ('0003','0004') AND locID <> '9911'))";
        }

        if (log.getAccessLevel() == 1 && (log.getUserID().equals("P0702") || log.getUserID().equals("P0703"))) {
            q = "";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, leave_master b where a.staffID = b.staffID AND (datestart >= CURDATE() OR dateend >= CURDATE()) and a.departmentID = ? " + q + " and b.leaveID is not null group by a.staffID");
            stmt.setString(1, dept);
            rs = stmt.executeQuery();
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffFilteredByForReportList(LoginProfile log, String filter, String param_filter) throws Exception {

        String q = "";

        if (filter.equals("location")) {

            if (param_filter.equals("IBU PEJABAT")) {

                q = " WHERE " + filter + " = '" + param_filter + "'";

            }

        }

        if (filter.equals("departmentID")) {

            if (!param_filter.equals("none")) {

                q = " AND " + filter + " = '" + param_filter + "'";

            } else {
                q = " AND email <> ''";
            }

        }

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, co_staff_position b WHERE a.position = b.descp  AND  a.status = 'Active' " + q + " ORDER BY b.level ASC  ");
            //stmt.setString(1, staffid);
            //Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffFilteredBy(LoginProfile log, String filter, String param_filter) throws Exception {

        String q = "";

//        if(filter.equals("location")){
//            
//            if(param_filter.equals("IBU PEJABAT")){
//                
//                q = filter+" = '"+ param_filter + "'";
//                
//            }
//            
//        }
//        
//        if(filter.equals("departmentID")){
//            
//            if(!param_filter.equals("none")){
//                
//                q = filter+" = '"+ param_filter + "'";
//                
//            }else{
//                q = " AND email <> ''";
//            }
//            
//        }
        if (log.getAccessLevel() == 0) {
            q += "  staffID = '" + log.getUserID() + "'";
        } else if (log.getAccessLevel() == 2 || log.getAccessLevel() == 3) {
            q += "  staffID = '" + log.getUserID() + "' OR staffID IN " + LeaveDAO.getSuperviseeID(log);
        } else if (log.getAccessLevel() == 1) {
            q += "  locID = '" + log.getLocID() + "'";
        }

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            //PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, co_staff_position b WHERE a.position = b.descp " + q +" ORDER BY b.level ASC  ");
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a WHERE " + q);
            //stmt.setString(1, staffid);
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static boolean isStaffInfoExisted(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE staffID=?");
        stmt.setString(1, id);
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);

        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static void saveStaff(LoginProfile log, CoStaff c, String supervisorID) throws Exception {

        try {

            if (isStaffInfoExisted(log, c.getStaffid())) {

                String q = ("UPDATE co_staff SET name = ?, imageURL = ?, email = ?, postID = ?, department = ?,departmentID = ?, location = ?, locID = ?, position = ? WHERE staffID = ?");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, c.getName());
                ps.setString(2, log.getImageURL());
                ps.setString(3, log.getEmail());
                ps.setString(4, c.getPosition());
                ps.setString(5, StaffDAO.getInfoDepartment(log, c.getDepartment()).getDescp());
                ps.setString(6, c.getDepartment());
                ps.setString(7, StaffDAO.getInfoLocation(log, c.getLocation()).getDescp());
                ps.setString(8, c.getLocation());
                ps.setString(9, StaffDAO.getInfoPosition(log, c.getPosition()).getDescp());
                ps.setString(10, c.getStaffid());

                ps.executeUpdate();
                ps.close();
            } else {

                String q = ("INSERT INTO co_staff(name, imageURL, email, position, department, location, staffID, departmentID, locID) VALUES (?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, c.getName());
                ps.setString(2, log.getImageURL());
                ps.setString(3, log.getEmail());
                ps.setString(4, c.getPosition());
                ps.setString(5, StaffDAO.getInfoDepartment(log, c.getDepartment()).getDescp());
                ps.setString(6, StaffDAO.getInfoLocation(log, c.getLocation()).getDescp());
                ps.setString(7, c.getStaffid());
                ps.setString(8, c.getDepartment());
                ps.setString(9, c.getLocation());

                Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---query-- " + ps);

                ps.executeUpdate();
                ps.close();

            }

            String q1 = ("INSERT into supervise_info(staffID, supervisorID, headID) values (?,?,?)");
            PreparedStatement ps1 = log.getCon().prepareStatement(q1);
            ps1.setString(1, c.getStaffid());
            ps1.setString(2, supervisorID);
            ps1.setString(3, StaffDAO.getHeadID(log, c));

            ps1.executeUpdate();
            ps1.close();

            int is = 0;
            if (isStaffSupervisorFirstLogin(log, c.getStaffid())) {
                is = 2;
            }
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---supervisor-- " + is);

            if (isStaffHeadIDFirstLogin(log, c.getStaffid())) {
                is = 3;
            }

            String q2 = ("INSERT into user_access(staffID, level) values (?,?)");
            PreparedStatement ps2 = log.getCon().prepareStatement(q2);
            ps2.setString(1, c.getStaffid());
            ps2.setInt(2, is);

            ps2.executeUpdate();
            ps2.close();

            if (isSupervisorRegistered(log, supervisorID)) {
                StaffDAO.updateToSupervisor(log, supervisorID);
            }

            //dummy leave info - remove once released
            LeaveDAO.saveDummyLeave(log, c.getStaffid());

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getHeadID(LoginProfile log, CoStaff st) throws SQLException, Exception {
        String c = "";

        c = StaffDAO.getInfoDepartment(log, st.getDepartment()).getHeadID();

        if (c.equals(st.getStaffid())) {
            c = StaffDAO.getInfoDepartment(log, "0010").getHeadID();
        }

//        ResultSet rs = null;
//        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE supervisorID=?");
//        stmt.setString(1, id);
//        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---supervisor-- " + stmt);
//
//        rs = stmt.executeQuery();
//        if (rs.next()) {
//            c = true;
//        }
        return c;
    }

    public static boolean isStaffSupervisorFirstLogin(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE supervisorID=?");
        stmt.setString(1, id);
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---supervisor-- " + stmt);

        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static boolean isStaffHeadIDFirstLogin(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_department WHERE headID=?");
        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static boolean isSupervisorRegistered(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access WHERE staffID=?");
        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static void updateToSupervisor(LoginProfile log, String supervisorID) throws Exception {//kalau user bawah login update dia jadi penyelia

        try {

            if (StaffDAO.getUserAccess(log, supervisorID).getLevel() == 0 && !supervisorID.equals("Tiada")) {

                String q = ("UPDATE user_access SET level = ? WHERE staffID = ?");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setInt(1, 2);
                ps.setString(2, supervisorID);

                ps.executeUpdate();
                ps.close();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<CoStaff> getMatchNamefromRecord(LoginProfile log, String name) throws Exception {

        Statement stmt = null;
        List<CoStaff> Com;
        Com = new ArrayList();

        String q = "";
        ArrayList<String> totalCheckCode = new ArrayList<String>();
        StringTokenizer t = new StringTokenizer(name);
        String word = "";
        while (t.hasMoreTokens()) {
            word = t.nextToken();

        }

        String[] n = name.split("\\s+");

        word = n[0] + " " + n[1];

        //if (!word.contains("MOHD") || !word.contains("B.") || !word.contains("Mohd")) {
        //if(!(word.equalsIgnoreCase("mohd")) && !(word.equalsIgnoreCase("b.")) && !(word.equalsIgnoreCase("bt.")) && !(word.equalsIgnoreCase("mat"))){
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "--- " + word);
        if (name != null) {
            q = "where name like '%" + word + "%'";
            //Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, ">>>>>>> " + q);
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select staffID,name from co_staff " + q + " order by name");
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, String.valueOf("select staffID,name from co_staff " + q + " order by name"));
            while (rs.next()) {

                if (totalCheckCode.size() == 0) {
                    //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "firstinsert=={0}", a.getCoacode());
                    totalCheckCode.add(rs.getString("staffID"));
                    CoStaff by = new CoStaff();
                    by.setStaffid(rs.getString("staffID"));
                    by.setName(rs.getString("name"));

                    Com.add(by);
                }

                int x = 0;
                for (int i = 0; i < totalCheckCode.size(); i++) {
                    if (totalCheckCode.get(i).equals(rs.getString("staffID"))) {
                        x++;

                    }
                }

                //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "2ndrow=={0}", x);
                if (x == 0) {
                    totalCheckCode.add(rs.getString("staffID"));
                    CoStaff by = new CoStaff();
                    by.setStaffid(rs.getString("staffID"));
                    by.setName(rs.getString("name"));

                    Com.add(by);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //}

        for (int i = 0; i < totalCheckCode.size(); i++) {
            //Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "********* " + totalCheckCode.get(i));
        }

        return Com;
    }

    public static CoStaffPosition getInfoStaffPositionRS(ResultSet rs) throws SQLException {

        CoStaffPosition g = new CoStaffPosition();

        g.setDescp(rs.getString("descp"));
        g.setDorder(rs.getInt("dorder"));
        g.setId(rs.getInt("id"));
        g.setLevel(rs.getInt("level"));
        g.setCode(rs.getString("code"));

        return g;
    }
    
    public static CoStaffPosition getInfoPosition(LoginProfile log, String no) throws SQLException, Exception {
        CoStaffPosition c = new CoStaffPosition();
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_position WHERE postID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoStaffPositionRS(rs);
        }

        return c;
    }

    public static List<CoStaffPosition> getAllPosition(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffPosition> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_position order by dorder asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoStaffPositionRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static CoStaffDepartment getInfoDepartmentRS(ResultSet rs) throws SQLException {

        CoStaffDepartment g = new CoStaffDepartment();

        g.setDescp(rs.getString("descp"));
        g.setId(rs.getString("id"));
        g.setHeadID(rs.getString("headID"));
        g.setLocID(rs.getString("locID"));

        return g;
    }

    public static List<CoStaffDepartment> getAllDepartment(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffDepartment> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_department order by id asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoDepartmentRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaffDepartment> getAllDepartmentWithoutGMMD(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffDepartment> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_department WHERE id NOT IN ('0010','0011') order by id asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoDepartmentRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static CoStaffLocation getInfoLocationRS(ResultSet rs) throws SQLException {

        CoStaffLocation g = new CoStaffLocation();

        g.setDescp(rs.getString("descp"));
        g.setId(rs.getInt("id"));
        g.setLocID(rs.getString("locID"));

        return g;
    }

    public static List<CoStaffLocation> getAllLocation(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffLocation> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_location order by id asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoLocationRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static CoStaffDepartment getInfoDepartment(LoginProfile log, String no) throws SQLException, Exception {
        CoStaffDepartment c = new CoStaffDepartment();
        ResultSet rs = null;
        
        if(no.equals("none")){
            no = log.getDeptID();
        }
        
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_department WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoDepartmentRS(rs);
        }

        return c;
    }

    public static CoStaffLocation getInfoLocation(LoginProfile log, String no) throws SQLException, Exception {
        CoStaffLocation c = new CoStaffLocation();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_location WHERE locID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoLocationRS(rs);
        }

        return c;
    }

    public static UserAccess getUserAccessRS(ResultSet rs) throws SQLException {

        UserAccess g = new UserAccess();

        g.setId(rs.getInt("id"));
        g.setLevel(rs.getInt("level"));
        g.setStaffID(rs.getString("staffID"));

        return g;
    }

    public static UserAccess getUserAccess(LoginProfile log, String no) throws SQLException, Exception {
        UserAccess c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access WHERE staffID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getUserAccessRS(rs);
        }

        return c;
    }

    public static UserAccess getUserAccessByLevel(LoginProfile log, int no) throws SQLException, Exception {
        UserAccess c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access a, co_staff b WHERE a.staffID = b.staffID AND a.level=? AND b.locID = ? ");
        stmt.setInt(1, no);
        stmt.setString(2, log.getLocID());
        rs = stmt.executeQuery();
        if (rs.next()) {

            c = getUserAccessRS(rs);
        }

        return c;
    }

    public static boolean isAdminAvailable(LoginProfile log, String no) throws SQLException, Exception {
        boolean c = true;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_master WHERE staffID=? and ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN datestart AND dateend)");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = false;
        }

        return c;
    }

    public static UserAccess getUserAccessByLevelForEmail(LoginProfile log, int no, boolean email) throws SQLException, Exception {

        UserAccess c = getUserAccessByLevelForEmailx(log, no, true);
        if (StaffDAO.getInfo(log, c.getStaffID()).getLocID().equals("9911")) {
            if (!isAdminAvailable(log, c.getStaffID())) {
                c = getUserAccessByLevelForEmailx(log, no, false);
            }
        }

        return c;
    }

    public static UserAccess getUserAccessByLevelForEmailx(LoginProfile log, int no, boolean email) throws SQLException, Exception {
        UserAccess c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access a, co_staff b WHERE a.staffID = b.staffID AND a.level=? AND b.locID = ? AND a.email = ? order by a.id ASC");
        stmt.setInt(1, no);
        stmt.setString(2, log.getLocID());
        stmt.setBoolean(3, email);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getUserAccessRS(rs);

        }

        return c;
    }

    public static UserAccess getUserAccessByLevelAndEmailNotification(LoginProfile log, int no) throws SQLException, Exception {
        UserAccess c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access a, co_staff b WHERE a.staffID = b.staffID AND a.level=? AND b.locID = ? ");
        stmt.setInt(1, no);
        stmt.setString(2, log.getLocID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getUserAccessRS(rs);
        }

        return c;
    }

    public static CoStaff getInfoSupervisor(LoginProfile log, String no) throws SQLException, Exception {

        CoStaff c = null;
        String supervisorID = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE staffid=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            supervisorID = rs.getString("supervisorID");
        }

        c = (CoStaff) StaffDAO.getInfo(log, supervisorID);

        return c;
    }

    public static CoStaff getInfoHead(LoginProfile log, String no) throws SQLException, Exception {

        CoStaff c = null;
        String headID = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE staffid=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            headID = rs.getString("headID");
        }

        c = (CoStaff) StaffDAO.getInfo(log, headID);

        return c;
    }

    public static CoStaff getInfoHRHead(LoginProfile log) throws SQLException, Exception {

        CoStaff c = (CoStaff) StaffDAO.getInfo(log, getUserAccessByLevel(log, 4).getStaffID());

        return c;
    }

    public static List<CoStaff> getAllStaffSearch(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<CoStaff> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = " WHERE name like '%" + keyword + "%' or staffID like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM co_staff " + q);
            while (rs.next()) {
                Com.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static String getBadgeLevelColor(int lvl) {

        String color = "";

        if (lvl == 0) {
            color = "warning";
        } else if (lvl == 1) {
            color = "success";
        } else if (lvl == 2) {
            color = "blue";
        } else if (lvl == 3) {
            color = "info";
        } else if (lvl == 4) {
            color = "default";
        }

        return color;
    }

    public static String getDepartmentMemberID(LoginProfile log, String departmentID) throws SQLException, Exception {

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE departmentID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, departmentID);
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("staffID") + "'";
            } else {
                s += "'" + rs.getString("staffID") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }

    public static boolean isSupervisorAsHeadDept(LoginProfile log, String staffID) throws SQLException, Exception {
        boolean c = false;

        if (StaffDAO.getInfoSupervisor(log, staffID).getStaffid().equals(StaffDAO.getInfoHead(log, staffID).getStaffid())) {
            c = true;
        }

        return c;
    }

    public static boolean isChangePersonExisted(LoginProfile log) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM sys_view_as WHERE email=?");
        stmt.setString(1, log.getEmail());
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);

        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static SysViewAs getSysViewAs(ResultSet rs) throws SQLException {

        SysViewAs ec = new SysViewAs();

        ec.setDeptID(rs.getString("deptID"));
        ec.setEmail(rs.getString("email"));
        ec.setId(rs.getInt("id"));
        ec.setLocID(rs.getString("locID"));
        ec.setStaffID(rs.getString("staffID"));

        return ec;

    }

    public static SysViewAs getInfoSysViewAs(LoginProfile log, String no) throws SQLException, Exception {
        SysViewAs c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM sys_view_as WHERE email=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getSysViewAs(rs);
        }

        return c;
    }

    public static void transferLocation(LoginProfile log, String staffID, String deptID) throws Exception {

        CoStaff co = (CoStaff) StaffDAO.getInfo(log, staffID);
        CoStaffDepartment cod = (CoStaffDepartment) StaffDAO.getInfoDepartment(log, deptID);
        CoStaffLocation col = (CoStaffLocation) StaffDAO.getInfoLocation(log, cod.getLocID());

        String q = ("UPDATE co_staff SET locID = ?,location = ?, departmentID = ?, department = ? WHERE staffID = ?");
        PreparedStatement ps = log.getCon().prepareStatement(q);

        ps.setString(1, col.getLocID());
        ps.setString(2, col.getDescp());
        ps.setString(3, cod.getId());
        ps.setString(4, cod.getDescp());
        ps.setString(5, staffID);

        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "query=={0}", ps);

        ps.executeUpdate();
        ps.close();

        String q1 = ("UPDATE supervise_info SET headID = ?, supervisorID = ? WHERE staffID = ?");
        PreparedStatement ps1 = log.getCon().prepareStatement(q1);
        ps1.setString(1, cod.getHeadID());
        ps1.setString(2, null);
        ps1.setString(3, staffID);

        ps1.executeUpdate();
        ps1.close();
        
        String q2 = ("UPDATE supervise_info SET supervisorID = ? WHERE supervisorID = ?");
        PreparedStatement ps2 = log.getCon().prepareStatement(q2);
        ps2.setString(1, null);
        ps2.setString(2, staffID);

        ps2.executeUpdate();
        ps2.close();

//        String q2 = ("INSERT INTO staff_location_history (staffID, locID, deptID,supervisorID, datechanged) VALUES (?,?,?,?,?)");
//        PreparedStatement ps2 = log.getCon().prepareStatement(q2);
//        ps2.setString(1, staffID);
//        ps2.setString(2, co.getLocID());
//        ps2.setString(3, co.getDepartmentID());
//        ps2.setString(4, AccountingPeriod.getCurrentTimeStamp());
//
//        ps2.executeUpdate();
//        ps2.close();
    }

    public static int isSupervisorExist(LoginProfile log) throws SQLException, Exception {
        int c = 0;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE staffID=?");
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            if (rs.getString("supervisorID") == null) {
                c = 0;
            } else {
                c = 1;
            }

        }

        return c;
    }

    public static int isSupervisorExistUsingStaffID(LoginProfile log, String StaffID) throws SQLException, Exception {
        int c = 0;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE staffID=?");
        stmt.setString(1, StaffID);
        rs = stmt.executeQuery();
        if (rs.next()) {
            if (rs.getString("supervisorID") == null) {
                c = 0;
            } else {
                c = 1;
            }

        }

        return c;
    }

    public static void updateSupervisor(LoginProfile log, String staffID, String supervisorID) throws Exception {

        //CoStaff co = (CoStaff) StaffDAO.getInfo(log, staffID);
        String q = ("UPDATE supervise_info SET supervisorID = ?  WHERE staffid = ?");
        PreparedStatement ps = log.getCon().prepareStatement(q);

        ps.setString(1, supervisorID);
        ps.setString(2, staffID);

        ps.executeUpdate();
        ps.close();

    }

    public static boolean isSupervisorAsApprover(LoginProfile log, String staffID) throws SQLException, Exception {
        boolean c = false;

        SuperviseInfo si = (SuperviseInfo) getSuperviseInfo(log, staffID);
        if (si.getSupervisorID().equals(si.getHeadID())) {
            c = true;
        }

        return c;
    }

    public static SuperviseInfo getSuperviseInfoRS(ResultSet rs) throws SQLException {

        SuperviseInfo ec = new SuperviseInfo();

        ec.setHeadID(rs.getString("headID"));
        ec.setId(rs.getInt("id"));
        ec.setStaffID(rs.getString("staffID"));
        ec.setSupervisorID(rs.getString("supervisorID"));

        return ec;

    }

    public static SuperviseInfo getSuperviseInfo(LoginProfile log, String no) throws SQLException, Exception {
        SuperviseInfo c = new SuperviseInfo();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE staffID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getSuperviseInfoRS(rs);
        }

        return c;
    }

    public static List<CoStaff> getStaffWithLevel(LoginProfile log, int level, String deptID) throws Exception {

        List<CoStaff> Com;
        Com = new ArrayList();
        
        if(deptID.equals("none")){
            deptID = log.getDeptID();
        }

        try {
            ResultSet rs = null;
         
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, user_access b, supervise_info c where a.staffID = b.staffID and a.staffID = c.staffID and b.level = ? and a.departmentID = ?  and c.supervisorID is not null");
            stmt.setInt(1, level);
            stmt.setString(2, deptID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Com.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static List<CoStaff> getStaffWithUnderSupervise(LoginProfile log, String superviseID, String deptID) throws Exception {

        List<CoStaff> Com;
        Com = new ArrayList();
        
        if(deptID.equals("none")){
            deptID = log.getDeptID();
        }

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from supervise_info a, user_access b, co_staff c, co_staff_position d where a.staffID = b.staffID and a.staffID = c.staffID and c.postID = d.code  and a.supervisorID = ? and c.departmentID = ? order by d.level,c.staffID asc");
            //PreparedStatement stmt = log.getCon().prepareStatement("select * from supervise_info a, user_access b, co_staff c, co_staff_position d where a.staffID = b.staffID and a.staffID = c.staffID and c.postID = d.code and b.level in (0,1) and a.supervisorID = ? and c.departmentID = ? order by d.level,c.staffID asc");
            stmt.setString(1, superviseID);
            stmt.setString(2, deptID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Com.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static int getMax(int[] inputArray) {
        int maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }
    
    public static List<CoStaff> getStaffWithoutSupervise(LoginProfile log, String deptID) throws Exception {

        List<CoStaff> Com;
        Com = new ArrayList();
        
        
        
        if(deptID.equals("none")){
            deptID = log.getDeptID();
        }

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from supervise_info a, user_access b, co_staff c where a.staffID = b.staffID and a.staffID = c.staffID  and a.supervisorID is null and c.departmentID = ?");
            stmt.setString(1, deptID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Com.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    public static List<CoStaff> getAllStaffListWithHigherLevelThanzero(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        String q = "";

        if (log.getLocID().equals("9911")) {
            q = "and (locID = '9911' OR (locID <> '9911' AND departmentID IN ('0003','0004'))) ";
        } else {
            q = "and locID = '" + log.getLocID() + "'";
        }

        if (log.getUserID().equals("P0703") || log.getUserID().equals("P0702")) {
            q = "";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, user_access b, supervise_info c where a.staffID = b.staffID and a.staffID = c.staffID and b.level in (2,3) and c.supervisorID is not null " + q + "");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

}
