/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.model.EstateInfo;
import com.lcsb.ekenderaan.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class EstateDAO {

    public static List<EstateInfo> getAllEstate(LoginProfile log, String keyword, String type) throws Exception {

        Statement stmt = null;
        List<EstateInfo> Com;
        Com = new ArrayList();

        String q = "";
        String exQ = "";

        if (keyword != null) {
            q = "where estatecode like '%" + keyword + "%' or estatedescp like '%" + keyword + "%'";
            
            if (type != null) {
                exQ = "and type = '"+ type +"'";
            
            }
        }else{
            
            exQ = "where type = '"+ type +"'";
        }

       

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from estateinfo " + q + " "+ exQ +" order by estatecode");
            while (rs.next()) {

                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static EstateInfo getResult(ResultSet rs) throws SQLException {
        EstateInfo comp = new EstateInfo();
        comp.setAddress(rs.getString("address"));
        comp.setCity(rs.getString("city"));
        comp.setDistrict(rs.getString("district"));
        comp.setEstatecode(rs.getString("estatecode"));
        comp.setEstatedescp(rs.getString("estatedescp"));
        comp.setFax(rs.getString("fax"));
        comp.setPhone(rs.getString("phone"));
        comp.setPostcode(rs.getString("postcode"));
        comp.setState(rs.getString("state"));
        return comp;
    }

    public static EstateInfo getEstateInfo(LoginProfile log, String code, String columnRefer) throws Exception, SQLException {

        EstateInfo es = new EstateInfo();

        Logger.getLogger(EstateDAO.class.getName()).log(Level.INFO, "select * from estateinfo where " + columnRefer + " = '" + code + "' ");

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from estateinfo where " + columnRefer + " = ? ");
            stmt.setString(1, code);
            rs = stmt.executeQuery();
            if (rs.next()) {
                es = getResult(rs);
            }

            stmt.close();
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return es;

    }

}
