/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.model.BookingCar;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.Members;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class MemberDAO {
    
    public static List<Members> getMember(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<Members> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = " WHERE memberName like '%" + keyword + "%' or email like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM member "+q);
            while (rs.next()) {
                Com.add(getMemberRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
     public static Members getMemberInfo(LoginProfile log, String id) throws Exception {

        Members c = new Members();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM member WHERE compID = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getMemberRS(rs);
        }

        return c;
    }

    
    private static Members getMemberRS(ResultSet rs) throws SQLException, Exception {

        Members b = new Members();

        b.setAbb(rs.getString("abb"));
        b.setCompID(rs.getString("compID"));
        b.setEmail(rs.getString("email"));
        b.setId(rs.getInt("id"));
        b.setImageURL(rs.getString("imageURL"));
        b.setLevel(rs.getInt("level"));
        b.setMemberID(rs.getString("memberID"));
        b.setMemberName(rs.getString("memberName"));
        b.setPosition(rs.getString("position"));

        return b;
    }
    
}
