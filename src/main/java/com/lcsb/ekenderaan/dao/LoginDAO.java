/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.model.CoStaff;
import com.lcsb.ekenderaan.model.Login;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.Members;
import com.lcsb.ekenderaan.model.SysLogLogin;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class LoginDAO {

    public static Boolean checkUser(String username, String password, Connection con) throws Exception {

        ResultSet rs = null;
        String passdb = "";
        String passlogin = "";

        Boolean check = false;

        PreparedStatement stmt = con.prepareStatement("SELECT *,password('" + password + "') as pass FROM login WHERE user = ?");
        stmt.setString(1, username);

        rs = stmt.executeQuery();
        if (rs.next()) {

            passlogin = rs.getString("pass");
            passdb = rs.getString("password");
            Logger.getLogger(LoginDAO.class.getName()).log(Level.INFO, password + "-------password----" + passdb);

            if (passdb.equals(passlogin)) {
                check = true;
                //setGeneralUsage(username,estatecode,rs.getString("NoPekerja"));

            }
        }

        return check;

    }

    public static Boolean checkUserUsingEmail(String email, Connection con) throws Exception {

        ResultSet rs = null;

        Boolean check = false;

        PreparedStatement stmt = con.prepareStatement("SELECT * FROM co_staff WHERE email = ?");
        stmt.setString(1, email);

        rs = stmt.executeQuery();
        if (rs.next()) {

            check = true;

        }

        return check;

    }

    public static String userID(String username, Connection con) throws Exception {

        ResultSet rs = null;

        String userID = "";
        PreparedStatement stmt = con.prepareStatement("SELECT * FROM login WHERE user = ?");
        stmt.setString(1, username);

        rs = stmt.executeQuery();
        if (rs.next()) {
            userID = rs.getString("staff_id");

        }

        return userID;

    }
    
    public static int getAccessLevel(Connection con, String staffID) throws Exception {

       int l = 0;
        ResultSet rs = null;

        PreparedStatement stmt = con.prepareStatement("SELECT * FROM user_access WHERE staffID = ?");
        stmt.setString(1, staffID);
        Logger.getLogger(LoginDAO.class.getName()).log(Level.INFO, "-----" + String.valueOf(stmt));
        rs = stmt.executeQuery();
        if (rs.next()) {
            l = rs.getInt("level");

        }

        return l;

    }

    public static Members userInfo(Connection con, String email) throws Exception {

        Members ua = new Members();
        ResultSet rs = null;

        PreparedStatement stmt = con.prepareStatement("SELECT * FROM member WHERE email = ?");
        stmt.setString(1, email);

        rs = stmt.executeQuery();
        if (rs.next()) {
            ua.setLevel(rs.getInt("level"));
            ua.setMemberID(rs.getString("memberID"));
            ua.setMemberName(rs.getString("memberName"));

        }

        return ua;

    }

    public static LoginProfile setLogin(Connection con) throws Exception {

        ResultSet rs = null;


        LoginProfile login = new LoginProfile();
        login.setCon(con);

        return login;
    }

    public static void updateMemberWithGoogleCredential(Connection con, String email, String userID, String name, String imageURL) throws Exception {

        try {
            String q = ("UPDATE member SET memberName = ?, compID = ?, imageURL = ? WHERE email = '" + email + "'");
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, name);
            ps.setString(2, userID);
            ps.setString(3, imageURL);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setUserLog(LoginProfile log, String sess) throws Exception {

        Connection con = log.getCon();
        String q = ("insert into sec_usrlog(userid,workerid,workername,estatecode,estatename,logintime,loginstatus,sessionid) values (?,?,?,?,?,?,?,?)");
        PreparedStatement ps = con.prepareStatement(q);
        //ps.setString(1, LoginProfile.getUserName());
        //ps.setString(2, LoginProfile.getUserID());
        //ps.setString(3, LoginDAO.getStaffName());
        // ps.setString(4, LoginProfile.getEstateCode());
        // ps.setString(5, LoginProfile.getEstateDescp());
        //ps.setString(6, String.valueOf(GeneralTerm.getCurrentDateTime()));
        //ps.setString(7, "Success");
        // ps.setString(8, sess);
        // ps.executeUpdate();
        // ps.close();
    }
    
    public static void setLoginLog(LoginProfile log, SysLogLogin sys, CoStaff co,String type) throws Exception {

        Connection con = log.getCon();
        String q = ("insert into sys_log_login(connection,date,deptID,deptName,locID,locName,name,sessionid,time,userid,lastAccessTime,typeoflogin) values (?,?,?,?,?,?,?,?,?,?,?,?)");
        PreparedStatement ps = con.prepareStatement(q);
        ps.setString(1, String.valueOf(log.getCon()));
        ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
        ps.setString(3, co.getDepartmentID());
        ps.setString(4, co.getDepartment());
        ps.setString(5, co.getLocID());
        ps.setString(6, co.getLocation());
        ps.setString(7, co.getName());
        ps.setString(8, sys.getSessionid());
        ps.setString(9, AccountingPeriod.getCurrentTime());
        ps.setString(10, co.getStaffid());
        ps.setString(11, sys.getLastAccessTime());
        ps.setString(12, type);
        ps.executeUpdate();
        ps.close();
    }

    public static LoginProfile getStaffDetail(LoginProfile log, String id) throws Exception {
        ResultSet rs = null;
        Connection con = log.getCon();

        LoginProfile logx = new LoginProfile();

        String name = "";
        String staff_id = "";
        PreparedStatement stmt = con.prepareStatement("select * from executive where id=?");
        stmt.setString(1, id);

        rs = stmt.executeQuery();
        if (rs.next()) {
            name = rs.getString("name");
            staff_id = rs.getString("id");

            logx.setFullname(name);
            logx.setUserID(staff_id);

        }
        return log;
    }

//    private static void setLoginActivity(Connection con, String userID, String userName, String sessionID) throws Exception {
//
//        boolean a = false;
//        ResultSet rs = null;
//        PreparedStatement stmt = con.prepareStatement("select * from sys_log_login where sessionid=?");
//        stmt.setString(1, sessionID);
//
//        rs = stmt.executeQuery();
//        if (rs.next()) {
//            a = true;
//        }
//
//        if (!a) {
//            String query = "INSERT INTO sys_log_login(userid,username,sessionid,date,time) values (?,?,?,?,?)";
//
//            try (PreparedStatement ps = con.prepareStatement(query)) {
//                ps.setString(1, userID);
//                ps.setString(2, userName);
//                ps.setString(3, sessionID);
//                ps.setString(4, AccountingPeriod.getCurrentTimeStamp());
//                ps.setString(5, DateAndTime.addTime(AccountingPeriod.getCurrentTime()));
//                ps.executeUpdate();
//            }
//        }
//
//    }
//    public static void updateLogoutTime(LoginProfile log, String sessionID) throws Exception {
//
//        String query = "update sys_log_login set dateout = ?, timeout = ? WHERE sessionid = '" + sessionID + "'";
//
//        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
//            ps.setString(1, AccountingPeriod.getCurrentTimeStamp());
//            ps.setString(2, DateAndTime.addTime(AccountingPeriod.getCurrentTime()));
//            ps.executeUpdate();
//        }
//
//        SysLogDAO.logoutUpdate(log);
//
//    }
    
    public static void saveMemberInfo(LoginProfile log, Members item) throws Exception {

        try {
            String q = ("insert into member(abb,compID,email,imageURL,memberID,memberName,position,level) values (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getAbb());
            ps.setString(2, item.getCompID());
            ps.setString(3, item.getEmail());
            ps.setString(4, item.getImageURL());
            ps.setString(5, item.getMemberID());
            ps.setString(6, item.getMemberName());
            ps.setString(7, item.getPosition());
            ps.setInt(8, item.getLevel());

            ps.executeUpdate();
            ps.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static void updateImageGoogle(Connection con, String email, String imageURL) throws Exception {

        try {
            String q = ("UPDATE co_staff SET imageURL = ? WHERE email = ?");
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, imageURL);
            ps.setString(2, email);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
}
