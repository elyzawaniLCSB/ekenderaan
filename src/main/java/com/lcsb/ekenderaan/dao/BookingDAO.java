/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.model.Book;
import com.lcsb.ekenderaan.model.BookingCar;
import com.lcsb.ekenderaan.model.BookingDestination;
import com.lcsb.ekenderaan.model.BookingDestinationTemp;
import com.lcsb.ekenderaan.model.BookingMaster;
import com.lcsb.ekenderaan.model.BookingMasterTemp;
import com.lcsb.ekenderaan.model.BookingPassenger;
import com.lcsb.ekenderaan.model.BookingPassengerTemp;
import com.lcsb.ekenderaan.model.Car;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.Members;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author fadhilfahmi
 */
public class BookingDAO {

//    public static void saveNewBookold(LoginProfile log, Book item) throws Exception {
//
//        try {
//            String q = ("insert into booking(bookID, dateapply,destination,email,enddate,endtime,reason,staffID,staffname,startdate,starttime,status,daycount) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
//            PreparedStatement ps = log.getCon().prepareStatement(q);
//            ps.setString(1, AutoGenerate.get4digitNo(log, "booking", "bookID"));
//            ps.setString(2, DateAndTime.getCurrentTimeStamp());
//            ps.setString(3, item.getDestination());
//            ps.setString(4, log.getEmail());
//            ps.setString(5, item.getEnddate());
//            ps.setString(6, item.getEndtime());
//            ps.setString(7, item.getReason());
//            ps.setString(8, item.getStaffID());
//            ps.setString(9, log.getFullname());
//            ps.setString(10, item.getStartdate());
//            ps.setString(11, item.getStarttime());
//            ps.setString(12, "Preparing");
//            ps.setLong(13, DateAndTime.getDiffBetweenTwoDates(item.getStartdate(), item.getEnddate()));
//
//            ps.executeUpdate();
//            ps.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }
    public static String saveNewBook(LoginProfile log) throws Exception {

        String bookid = AutoGenerate.get4digitNo(log, "booking", "bookID");
        try {
            String q = ("insert into booking(bookID, dateapply,email,staffID,staffname,status) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, bookid);
            ps.setString(2, DateAndTime.getCurrentTimeStamp());
            ps.setString(3, log.getEmail());
            ps.setString(4, log.getUserID());
            ps.setString(5, log.getFullname());
            ps.setString(6, "Preparing");

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bookid;

    }

    public static void saveNewBookDestination(LoginProfile log, BookingDestination i) throws Exception {

        try {
            String q = ("insert into booking_destination(bookID, destcode,destdescp,desttype,enddate,endtime,startdate,starttime,userID,daycount) values (?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, i.getBookID());
            ps.setString(2, i.getDestcode());
            ps.setString(3, i.getDestdescp());
            ps.setString(4, i.getDesttype());
            ps.setString(5, i.getEnddate());
            ps.setString(6, i.getEndtime());
            ps.setString(7, i.getStartdate());
            ps.setString(8, i.getStarttime());
            ps.setString(9, i.getUserID());
            ps.setLong(10, DateAndTime.getDiffBetweenTwoDates(i.getStartdate(), i.getEnddate()));

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveNewBookPassenger(LoginProfile log, BookingPassenger i) throws Exception {

        try {
            String q = ("insert into booking_passenger(bookID, compID) values (?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, i.getBookID());
            ps.setString(2, i.getCompID());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static Book getBookRS(ResultSet rs) throws SQLException, Exception {

        Book b = new Book();

        b.setAppID(rs.getString("appID"));
        b.setAppdate(rs.getString("appdate"));
        b.setAppemail(rs.getString("appemail"));
        b.setAppname(rs.getString("appname"));
        b.setDateapply(rs.getString("dateapply"));
        b.setDrivetype(rs.getString("drivetype"));
        b.setEmail(rs.getString("email"));
        b.setBookID(rs.getString("bookID"));
        b.setStaffID(rs.getString("staffID"));
        b.setStaffname(rs.getString("staffname"));
        b.setStatus(rs.getString("status"));

        return b;
    }

    public static List<Book> getAllBook(LoginProfile log) throws Exception, SQLException {

        Statement stmt = null;
        List<Book> CV;
        CV = new ArrayList();

        String restrictByEmail = "";

        if (log.getAccessLevel() == 0) {
            restrictByEmail = "WHERE email = '" + log.getEmail() + "'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM booking " + restrictByEmail + " order by bookID asc ");

            while (rs.next()) {
                CV.add(getBookRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<Book> getAllConfirmedBook(LoginProfile log) throws Exception, SQLException {

        Statement stmt = null;
        List<Book> CV;
        CV = new ArrayList();

        String restrictByEmail = "";

        if (log.getAccessLevel() == 0) {
            restrictByEmail = " email = '" + log.getEmail() + " AND'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM booking WHERE " + restrictByEmail + "  status = 'Confirmed' order by bookID asc ");

            while (rs.next()) {
                CV.add(getBookRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static String getBadgeColor(String status) {

        String color = "dark";

        if (status.equals("Approved")) {
            color = "success";
        } else if (status.equals("Preparing")) {
            color = "blue";
        } else if (status.equals("Pending")) {
            color = "warning";
        }

        return color;
    }

    public static void deleteBook(LoginProfile log, String id) throws SQLException, Exception {

        String deleteQuery_2 = "DELETE FROM booking where bookID = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, id);
        ps_2.executeUpdate();
        ps_2.close();

    }

    public static Book getBookingInfo(LoginProfile log, String id) throws Exception {

        Book c = new Book();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM booking WHERE bookID = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBookRS(rs);
        }

        return c;
    }

    public static String getDayWord(int day) {

        String word = "";

        if (day == 1) {
            word = "day only";
        } else if (day > 1) {
            word = "days";
        }

        return word;
    }

    public static BookingCar getBookedCar(LoginProfile log, String id) throws Exception {

        BookingCar c = new BookingCar();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM booking_car WHERE bookID = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBookedCarRS(rs);
        }

        return c;
    }

    public static BookingCar getBookedCarByDestination(LoginProfile log, int id) throws Exception {

        BookingCar c = new BookingCar();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM booking_car WHERE destID = ?");

        stmt.setInt(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBookedCarRS(rs);
        }

        return c;
    }

    public static boolean isCarBooked(LoginProfile log, String id) throws Exception {

        boolean b = false;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM booking_car WHERE bookID = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            b = true;
        }

        return b;
    }

    public static String getWordBookedCar(boolean isB) {

        String word = "";

        if (isB) {
            word = "Assigned";
        } else {
            word = "Not Yet Assigned";
        }

        return word;
    }

    private static BookingCar getBookedCarRS(ResultSet rs) throws SQLException, Exception {

        BookingCar b = new BookingCar();

        b.setAppdate(rs.getString("appDate"));
        b.setBookID(rs.getString("bookID"));
        b.setCarID(rs.getString("carID"));
        b.setDriverID(rs.getString("driverID"));
        b.setId(rs.getInt("id"));
        b.setReason(rs.getString("reason"));
        b.setStatus(rs.getString("status"));
        b.setDrivetype(rs.getString("drivetype"));
        b.setDestID(rs.getInt("destID"));

        return b;
    }

    public static ArrayList<String> getBookIDinRangePeriod(LoginProfile log, String bookID, String id) throws Exception {

        ArrayList<String> r = new ArrayList<String>();

        BookingDestination b = (BookingDestination) BookingDAO.getDestination(log, id);//get date for this book

        Date current = DateAndTime.convertStringtoDate(b.getStartdate());//get start date for loop
        int hasSameDay = 0;
        while (current.before(DateAndTime.convertStringtoDate(b.getEnddate()))) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(current);
            calendar.add(Calendar.DATE, 1);
            current = calendar.getTime();

            List<BookingDestination> listAll = (List<BookingDestination>) BookingDAO.getListRangeDate(log, bookID);
            int i = 0;
            // Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(current));

            for (BookingDestination j : listAll) {

                //List<BookingDestination> listDest = (List<BookingDestination>) BookingDAO.getListDestinationByBookID(log, bookID);
                //for (BookingDestination m : listDest) {
                boolean a = BookingDAO.checkBetween2(current, j.getStartdate(), j.getEnddate());

                if (a) {
                    hasSameDay++;

                    if (!r.contains(j.getBookID())) {
                        r.add(j.getBookID());
                    }

                }
                //}

                //Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, j.getBookID() + "-" + current + "--" + String.valueOf(a));
            }

//            processDate(current);
//
        }

        Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf("Same Day : " + r));

        return r;
    }

    public static ArrayList<String> getAvailableCarIDinRangePeriod(LoginProfile log, String bookid, String id) throws Exception {

        ArrayList<String> bookIDList = BookingDAO.getBookIDinRangePeriod(log, bookid, id);

        ArrayList<String> r = new ArrayList<String>();

        for (String i : bookIDList) {

            r.add(getBookedCar(log, i).getCarID());

        }

        ArrayList<String> av = new ArrayList<String>();
        List<Car> listAll = (List<Car>) CarDAO.getAllCar(log);
        int i = 0;

        for (Car j : listAll) {

            //for (String k : r) {
            if (!r.contains(j.getCarID())) {//dalam array R takde carID
                //if (!k.contains(j.getCarID())) {
                av.add(j.getCarID());
                //}
            }
            //}

        }

        return av;
    }

    public static List<BookingDestination> getListRangeDate(LoginProfile log, String bookID) throws Exception, SQLException {

        Statement stmt = null;
        List<BookingDestination> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from booking_car a, booking b where a.bookID = b.bookID and b.status = 'Confirmed' AND b.bookID <> '" + bookID + "'");

            while (rs.next()) {
                //CV.add(BookingDAO.getBookRS(rs));

                Statement stmt1 = null;
                stmt1 = log.getCon().createStatement();
                ResultSet rs1 = stmt1.executeQuery("select * from booking_destination where bookID = '" + rs.getString("bookID") + "' enddate > '" + DateAndTime.getCurrentTimeStamp() + "'");

                while (rs1.next()) {
                    CV.add(BookingDAO.getBookingDestinationRS(rs));
                }
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static boolean checkBetween2(Date dateToCheck, String startDate, String endDate) throws ParseException {

        Date sd = DateAndTime.convertStringtoDate(startDate);
        Date ed = DateAndTime.convertStringtoDate(endDate);

        return dateToCheck.compareTo(sd) >= 0 && dateToCheck.compareTo(ed) <= 0;
    }

    public static boolean checkBetween(String dateToCheck, String startDate, String endDate) throws ParseException {
        Date dtc = DateAndTime.convertStringtoDate(dateToCheck);
        Date sd = DateAndTime.convertStringtoDate(startDate);
        Date ed = DateAndTime.convertStringtoDate(endDate);

        return dtc.compareTo(sd) >= 0 && dtc.compareTo(ed) <= 0;
    }

    public static boolean checkDestinationExisted(LoginProfile log, BookingDestinationTemp b) throws ParseException, SQLException {

        boolean c = false;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from booking_destination_temp where sessionid = ? and desttype = ? and destcode = ? and destdescp = ?");

        stmt.setString(1, b.getSessionid());
        stmt.setString(2, b.getDesttype());
        stmt.setString(3, b.getDestcode());
        stmt.setString(4, b.getDestdescp());
        rs = stmt.executeQuery();
        Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        if (rs.next()) {
            c = true;
        }

        return c;

    }

    public static int insertSelectedDestination(LoginProfile log, BookingDestinationTemp b) throws Exception {

        int i = 0;
        if (!checkDestinationExisted(log, b)) {
            String q = ("INSERT INTO booking_destination_temp(destcode,destdescp,desttype,sessionid,userID) VALUES (?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q, Statement.RETURN_GENERATED_KEYS)) {
                ps.setString(1, b.getDestcode());
                ps.setString(2, b.getDestdescp());
                ps.setString(3, b.getDesttype());
                ps.setString(4, b.getSessionid());
                ps.setString(5, b.getUserID());
                ps.executeUpdate();

                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    i = rs.getInt(1);
                }
                Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.close();

                // i = 1;
            }
        }

        return i;

    }

    public static int deleteSelectedDestination(LoginProfile log, String id) throws Exception {

        int i = 0;
        String q = ("DELETE FROM booking_destination_temp WHERE id = ?");
        try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
            ps.setString(1, id);
            ps.executeUpdate();
            Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            i = 1;
        }

        return i;

    }

    public static int deleteSelectedPassenger(LoginProfile log, String id) throws Exception {

        int i = 0;
        String q = ("DELETE FROM booking_passenger_temp WHERE id = ?");
        try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
            ps.setString(1, id);
            ps.executeUpdate();
            Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            i = 1;
        }

        return i;

    }

    public static List<BookingDestinationTemp> getListTempDestination(LoginProfile log, String sessionid) throws Exception, SQLException {

        List<BookingDestinationTemp> CV;
        CV = new ArrayList();

        try {
            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * from booking_destination_temp WHERE sessionid = ?");

            stmt.setString(1, sessionid);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CV.add(getBookingDestinationTempRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<BookingPassengerTemp> getListTempPassenger(LoginProfile log, String sessionid) throws Exception, SQLException {

        List<BookingPassengerTemp> CV;
        CV = new ArrayList();

        try {
            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * from booking_passenger_temp WHERE sessionid = ?");

            stmt.setString(1, sessionid);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CV.add(getBookingPassengerTempRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static BookingDestinationTemp getBookingDestinationTempByID(LoginProfile log, String id) throws Exception {

        BookingDestinationTemp c = new BookingDestinationTemp();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM booking_destination_temp WHERE id = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBookingDestinationTempRS(rs);
        }

        return c;
    }

    private static BookingDestinationTemp getBookingDestinationTempRS(ResultSet rs) throws SQLException, Exception {

        BookingDestinationTemp b = new BookingDestinationTemp();

        b.setDestcode(rs.getString("destcode"));
        b.setDestdescp(rs.getString("destdescp"));
        b.setDesttype(rs.getString("desttype"));
        b.setId(rs.getInt("id"));
        b.setSessionid(rs.getString("sessionid"));
        b.setUserID(rs.getString("userID"));
        b.setEnddate(rs.getString("enddate"));
        b.setEndtime(rs.getString("endtime"));
        b.setStartdate(rs.getString("startdate"));
        b.setStarttime(rs.getString("starttime"));

        return b;
    }

    private static BookingPassengerTemp getBookingPassengerTempRS(ResultSet rs) throws SQLException, Exception {

        BookingPassengerTemp b = new BookingPassengerTemp();

        b.setCompID(rs.getString("compID"));
        b.setId(rs.getInt("id"));
        b.setSessionid(rs.getString("sessionid"));

        return b;
    }

    private static BookingPassenger getBookingPassengerRS(ResultSet rs) throws SQLException, Exception {

        BookingPassenger b = new BookingPassenger();

        b.setCompID(rs.getString("compID"));
        b.setId(rs.getInt("id"));
        b.setBookID(rs.getString("bookID"));

        return b;
    }

    public static int updateDateTimeforSelectedDestination(LoginProfile log, BookingDestinationTemp b) throws Exception {

        int i = 0;
        if (!checkDestinationExisted(log, b)) {
            String q = ("UPDATE booking_destination_temp SET startdate = ?, enddate = ?, starttime = ?, endtime = ? WHERE id = ?");
            try (PreparedStatement ps = log.getCon().prepareStatement(q, Statement.RETURN_GENERATED_KEYS)) {
                ps.setString(1, b.getStartdate());
                ps.setString(2, b.getEnddate());
                ps.setString(3, b.getStarttime());
                ps.setString(4, b.getEndtime());
                ps.setInt(5, b.getId());
                ps.executeUpdate();
                Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.close();

                i = 1;
            }
        }

        return i;

    }

    public static int insertSelectedPassenger(LoginProfile log, Members b, String sessionid) throws Exception {

        int i = 0;
        if (!checkPassengerExisted(log, b, sessionid)) {
            String q = ("INSERT INTO booking_passenger_temp(sessionid,compID) VALUES (?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q, Statement.RETURN_GENERATED_KEYS)) {
                ps.setString(1, sessionid);
                ps.setString(2, b.getCompID());
                ps.executeUpdate();

                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    i = rs.getInt(1);
                }
                Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.close();

                // i = 1;
            }
        }

        return i;

    }

    public static boolean checkPassengerExisted(LoginProfile log, Members b, String sessionid) throws ParseException, SQLException {

        boolean c = false;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from booking_passenger_temp where sessionid = ? and compID = ?");

        stmt.setString(1, sessionid);
        stmt.setString(2, b.getCompID());
        rs = stmt.executeQuery();
        Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        if (rs.next()) {
            c = true;
        }

        return c;

    }

    public static BookingMasterTemp getBookingMasterTemp(LoginProfile log, String sessionid) throws ParseException, SQLException, Exception {

        BookingMasterTemp c = new BookingMasterTemp();

        c.setListDestination(BookingDAO.getListTempDestination(log, sessionid));
        c.setListPassenger(BookingDAO.getListTempPassenger(log, sessionid));
        c.setSessionid(sessionid);
        c.setCompID(log.getUserID());

        return c;

    }

    public static int saveBookingMaster(LoginProfile log, String sessionid) throws SQLException, Exception {

        int i = 0;
        try {
            BookingMasterTemp bm = (BookingMasterTemp) BookingDAO.getBookingMasterTemp(log, sessionid);
            List<BookingDestinationTemp> bdt = bm.getListDestination();
            List<BookingPassengerTemp> bpt = bm.getListPassenger();

            String bookID = BookingDAO.saveNewBook(log);

            for (BookingDestinationTemp j : bdt) {
                BookingDestination b = new BookingDestination();
                b.setBookID(bookID);
                b.setDestcode(j.getDestcode());
                b.setDestdescp(j.getDestdescp());
                b.setDesttype(j.getDesttype());
                b.setEnddate(j.getEnddate());
                b.setEndtime(j.getEndtime());
                b.setStartdate(j.getStartdate());
                b.setStarttime(j.getStarttime());
                b.setUserID(j.getUserID());
                BookingDAO.saveNewBookDestination(log, b);
            }

            for (BookingPassengerTemp j : bpt) {
                BookingPassenger p = new BookingPassenger();
                p.setBookID(bookID);
                p.setCompID(j.getCompID());
                BookingDAO.saveNewBookPassenger(log, p);
            }

            i = 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;

    }

    public static List<BookingDestination> getListDestinationByBookID(LoginProfile log, String bookID) throws Exception, SQLException {

        List<BookingDestination> CV;
        CV = new ArrayList();

        try {
            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * from booking_destination WHERE bookID = ?");

            stmt.setString(1, bookID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CV.add(getBookingDestinationRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    private static BookingDestination getBookingDestinationRS(ResultSet rs) throws SQLException, Exception {

        BookingDestination b = new BookingDestination();

        b.setDestcode(rs.getString("destcode"));
        b.setDestdescp(rs.getString("destdescp"));
        b.setDesttype(rs.getString("desttype"));
        b.setId(rs.getInt("id"));
        b.setBookID(rs.getString("bookID"));
        b.setUserID(rs.getString("userID"));
        b.setEnddate(rs.getString("enddate"));
        b.setEndtime(rs.getString("endtime"));
        b.setStartdate(rs.getString("startdate"));
        b.setStarttime(rs.getString("starttime"));
        b.setDaycount(rs.getInt("daycount"));

        return b;
    }

    public static List<BookingPassenger> getListPassengerByID(LoginProfile log, String bookID) throws Exception, SQLException {

        List<BookingPassenger> CV;
        CV = new ArrayList();

        try {
            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * from booking_passenger WHERE bookID = ?");

            stmt.setString(1, bookID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CV.add(getBookingPassengerRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static BookingMaster getBookingMaster(LoginProfile log, String bookID) throws ParseException, SQLException, Exception {

        BookingMaster c = new BookingMaster();

        c.setListDestination(BookingDAO.getListDestinationByBookID(log, bookID));
        c.setListPassenger(BookingDAO.getListPassengerByID(log, bookID));
        c.setBooking(BookingDAO.getBookingInfo(log, bookID));

        return c;

    }

    public static BookingDestination getDestination(LoginProfile log, String id) throws Exception {

        BookingDestination c = new BookingDestination();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM booking_destination WHERE id = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBookingDestinationRS(rs);
        }

        return c;
    }

    public static int assignCarForDestination(LoginProfile log, BookingCar b) throws Exception {

        int i = 0;
        //if (!checkDestinationExisted(log, b)) {
        String q = ("INSERT INTO booking_car(bookID,carID,driverID,drivetype,destID) VALUES (?,?,?,?,?)");
        try (PreparedStatement ps = log.getCon().prepareStatement(q, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, b.getBookID());
            ps.setString(2, b.getCarID());
            ps.setString(3, CarDAO.getCarInfo(log, b.getCarID()).getDriverID());
            ps.setString(4, b.getDrivetype());
            ps.setInt(5, b.getDestID());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                i = rs.getInt(1);
            }
            Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            // i = 1;
        }
        // }
        // }
        // }
        // }

        return i;

    }

    public static int isBookingReadyToConfirm(LoginProfile log, String bookID) throws Exception {

        int b = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM booking_destination WHERE bookID = ?");

        stmt.setString(1, bookID);
        rs = stmt.executeQuery();
        while (rs.next()) {

            ResultSet rs1 = null;
            PreparedStatement stmt1 = log.getCon().prepareStatement("SELECT * FROM booking_car WHERE destID = ?");

            stmt1.setString(1, rs.getString("id"));
            rs1 = stmt1.executeQuery();
            if (rs1.next()) {
                b += 0;
            } else {
                b += 1;
            }
        }

        return b;
    }

    public static int confirmBooking(LoginProfile log, String bookID) throws Exception {

        int i = 0;
        String q = ("UPDATE booking SET status = ?  WHERE bookID = ?");
        try (PreparedStatement ps = log.getCon().prepareStatement(q, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, "Confirmed");
            ps.setString(2, bookID);
            ps.executeUpdate();
            Logger.getLogger(BookingDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            i = 1;
        }

        return i;

    }

    public static JSONObject writeJsonSimpleDemo() throws Exception {
        JSONObject person = new JSONObject();
        person.put("firstName", "fadhil");
        person.put("lastName", "fahmi");
        return person;

    }

    public static JSONArray getAllBookingForCalendar(LoginProfile log) throws Exception {

        JSONArray array = new JSONArray();

        List<Book> bdt = BookingDAO.getAllConfirmedBook(log);

        for (Book j : bdt) {

            List<BookingDestination> ls = BookingDAO.getListDestinationByBookID(log, j.getBookID());

            for (BookingDestination i : ls) {

                JSONObject jo = new JSONObject();
                jo.put("id", i.getId());
                //jo.put("title", "<img src = \""+MemberDAO.getMemberInfo(log, j.getStaffID()).getImageURL()+"\">-"+ i.getDestdescp());
                jo.put("title", i.getDestdescp());
                jo.put("start", i.getStartdate());
                jo.put("end", i.getEnddate());
                jo.put("className", "fc-event-danger");
                jo.put("imageurl", MemberDAO.getMemberInfo(log, j.getStaffID()).getImageURL());
                array.add(jo);
                jo = null;

            }

        }


        return array;

    }
    
    public static int getNewBooking(LoginProfile log) throws Exception {

        int x = 0;
        
        String restrictByEmail = "";

        if (log.getAccessLevel() == 0) {
            restrictByEmail = "AND  email = '" + log.getEmail() + "'";
        }
        
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select count(*) as cnt from booking where status = 'Preparing' "+restrictByEmail+"");

        if (setent.next()) {
            x += setent.getInt("cnt");
        }

        stment.close();
        setent.close();
        return x;
    }

}
