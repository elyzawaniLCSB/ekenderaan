/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.model.Car;
import com.lcsb.ekenderaan.model.Driver;
import com.lcsb.ekenderaan.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class CarDAO {

    public static List<Car> getAllCar(LoginProfile log) throws Exception, SQLException {

        Statement stmt = null;
        List<Car> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM car order by carID asc ");

            while (rs.next()) {
                CV.add(getCarRS(rs));
            }
//
            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    private static Car getCarRS(ResultSet rs) throws SQLException, Exception {
        Car cv = new Car();

        cv.setCarID(rs.getString("carID"));
        cv.setDescp(rs.getString("descp"));
        cv.setPlateNo(rs.getString("plateNo"));
        cv.setStatus(rs.getString("status"));
        cv.setRemark(rs.getString("remark"));
        cv.setDriverID(rs.getString("driverID"));
//asadaaaaaaaaaa
        return cv;
    }

    public static void saveNewCar(LoginProfile log, Car item) throws Exception {

        try {
            String q = ("insert into car(carID,descp,plateNo,status,remark,driverID) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get4digitNo(log, "car", "carID"));
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getPlateNo());
            ps.setString(4, item.getStatus());
            ps.setString(5, item.getRemark());
            ps.setString(6, item.getDriverID());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateCar(LoginProfile log, Car item) throws Exception {

        try {
            String q = ("UPDATE car SET descp = ?, plateNo = ?, status = ?, remark = ?, driverID = ? WHERE carID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDescp());
            ps.setString(2, item.getPlateNo());
            ps.setString(3, item.getStatus());
            ps.setString(4, item.getRemark());
            ps.setString(5, item.getDriverID());
            ps.setString(6, item.getCarID());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Car getCarInfo(LoginProfile log, String id) throws Exception {

        Car c = new Car();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM car WHERE carID = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getCarRS(rs);
        }

        return c;
    }

    public static String getBadgeColor(String status) {

        String color = "dark";

        if (status.equals("Active")) {
            color = "success";
        } else if (status.equals("On Maintenance")) {
            color = "blue";
        } else if (status.equals("Not Active")) {
            color = "warning";
        }

        return color;
    }
    
    public static void deleteCar(LoginProfile log, String id) throws SQLException, Exception {


        String deleteQuery_2 = "DELETE FROM car where carID = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, id);
        ps_2.executeUpdate();
        ps_2.close();

    }
    
    
    
   

}
