/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import com.lcsb.ekenderaan.model.Car;
import com.lcsb.ekenderaan.model.Driver;
import com.lcsb.ekenderaan.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class DriverDAO {
    
    public static List<Driver> getAllDriver(LoginProfile log) throws Exception, SQLException {

        Statement stmt = null;
        List<Driver> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM driver order by driverID asc ");

            while (rs.next()) {
                CV.add(getDriverRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    private static Driver getDriverRS(ResultSet rs) throws SQLException, Exception {
        Driver cv = new Driver();

        cv.setDriverID(rs.getString("driverID"));
        cv.setDriverName(rs.getString("driverName"));
        cv.setEmail(rs.getString("email"));
        cv.setStaffID(rs.getString("staffID"));
        cv.setStatus(rs.getString("status"));
        



        return cv;
    }
    
    public static Driver getDriverInfo(LoginProfile log, String id) throws Exception {

        Driver c = new Driver();
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM driver WHERE driverID = ?");

        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getDriverRS(rs);
        }

        return c;
    }
    

    public static void saveNewDriver(LoginProfile log, Driver item) throws Exception {

        try {
            String q = ("insert into driver(driverID,driverName,staffID,email,status) values (?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get4digitNo(log, "driver", "driverID"));
            ps.setString(2, item.getDriverName());
            ps.setString(3, item.getStaffID());
            ps.setString(4, item.getEmail());
            ps.setString(5, item.getStatus());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateDriver(LoginProfile log, Driver item) throws Exception {

        try {
            String q = ("UPDATE driver SET driverName = ?, staffID = ?, email = ?, status = ? WHERE driverID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDriverName());
            ps.setString(2, item.getStaffID());
            ps.setString(3, item.getEmail());
            ps.setString(4, item.getStatus());
            ps.setString(5, item.getDriverID());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    

    public static String getBadgeColor(String status) {

        String color = "dark";

        if (status.equals("Active")) {
            color = "success";
        } else if (status.equals("On Maintenance")) {
            color = "blue";
        } else if (status.equals("Not Active")) {
            color = "warning";
        }

        return color;
    }
    
    
    public static void deleteDriver(LoginProfile log, String id) throws SQLException, Exception {


        String deleteQuery_2 = "DELETE FROM driver where driverID = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, id);
        ps_2.executeUpdate();
        ps_2.close();

    }
    
}
