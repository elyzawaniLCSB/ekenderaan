/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import static com.lcsb.ekenderaan.dao.StaffDAO.getInfoLocationRS;
import com.lcsb.ekenderaan.model.CoStaffLocation;
import com.lcsb.ekenderaan.model.Leave;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.Notification;
import com.lcsb.ekenderaan.model.Notify;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class NotifyDAO {

    public static void insertNoti(LoginProfile log, Notification n) throws Exception {

        try {

            String q = ("INSERT INTO notification(date,staffID,time,type,seen,leaveID) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(2, n.getStaffID());
            ps.setString(3, AccountingPeriod.getCurrentTime());
            ps.setString(4, n.getType());
            ps.setBoolean(5, false);
            ps.setString(6, n.getLeaveID());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<Notification> getAllNotificationByStaffID(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<Notification> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM notification WHERE staffID = ? AND seen = ? order by id desc limit 10");
            stmt.setString(1, log.getUserID());
            stmt.setBoolean(2, false);
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--" + String.valueOf(stmt));
            while (rs.next()) {

                boolean b = NotifyDAO.isTakeAction(log, rs.getString("leaveID"), rs.getString("type"));

                if (b) {
                    NotifyDAO.updateNotification(log, rs.getString("id"));
                }

                CVi.add(getInfoRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static Notification getInfoRS(ResultSet rs) throws SQLException {

        Notification n = new Notification();

        n.setDate(rs.getString("date"));
        n.setId(rs.getInt("id"));
        n.setSeen(rs.getBoolean("seen"));
        n.setStaffID(rs.getString("staffID"));
        n.setTime(rs.getString("time"));
        n.setType(rs.getString("type"));
        n.setLeaveID(rs.getString("leaveID"));

        return n;
    }

    public static Notify getNotify(Notification n) {

        Notify nt = new Notify();

        String word = "";
        String icon = "";

        if (n.getType().equals("prepareleave")) {
            word = "Semak Permohonan";
            icon = "ti-announcement";
        } else if (n.getType().equals("checkedleave")) {
            word = "Sokong Permohonan";
            icon = "ti-announcement";
        } else if (n.getType().equals("supportedleave")) {
            word = "Lulus Permohonan";
            icon = "ti-announcement";
        } else if (n.getType().equals("rejectedleave")) {
            word = "Cuti tidak lulus!";
            icon = "ti-face-sad text-danger";
        } else if (n.getType().equals("approvedleave")) {
            word = "Cuti anda lulus!";
            icon = "ti-check text-success";
        } else if (n.getType().equals("comment")) {
            word = "Cuti anda dikomen";
            icon = "ti-comment-alt";
        }

        nt.setIcon(icon);
        nt.setWord(word);

        return nt;

    }

    public static int updateNotify(LoginProfile log, String id) throws Exception {

        int i = 0;
        try {
            String q = ("UPDATE notification SET seen = ?  WHERE id = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setBoolean(1, true);
            ps.setString(2, id);

            ps.executeUpdate();
            ps.close();

            i = 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;

    }

    public static void updateNotifyFromPage(LoginProfile log, String leaveID) throws Exception {

        try {
            String q = ("UPDATE notification SET seen = ?  WHERE leaveID = ? AND staffID = ? AND seen = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setBoolean(1, true);
            ps.setString(2, leaveID);
            ps.setString(3, log.getUserID());
            ps.setBoolean(4, false);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Notification getNotificationByLeaveID(LoginProfile log, String leaveID) {

        ResultSet rs = null;
        Notification CVi = new Notification();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM notification WHERE staffID = ? AND leaveID = ? AND seen = ? order by id desc");
            stmt.setString(1, log.getUserID());
            stmt.setString(2, leaveID);
            stmt.setBoolean(3, false);
            rs = stmt.executeQuery();
            while (rs.next()) {
                //CVi.add(getInfoRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static boolean isTakeAction(LoginProfile log, String leaveID, String todo) throws Exception {

        boolean t = false;
        Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, leaveID);

        if (todo.equals("prepareleave")) {
            if (!l.getCheckID().equals("")) {
                t = true;
            }
        } else if (todo.equals("checkedleave")) {

            if (StaffDAO.isSupervisorAsApprover(log, l.getStaffID()) || l.getType().equals("C02")) {//if true = 2 level
                //2 level @ mc leave
                if (!l.getHeadID().equals("")) {
                    t = true;
                }
            } else {
                //3 level
                if (!l.getSupervisorID().equals("")) {
                    t = true;
                }
            }

        } else if (todo.equals("supportedleave")) {
            if (!l.getHeadID().equals("")) {
                t = true;
            }
        }else if (todo.equals("approvedleave")) {
            Date today = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dateStart = formatter.parse(l.getDatestart());
            if (dateStart.compareTo(today) < 0 && !formatter.format(dateStart).equals(formatter.format(today))) {
                t = true;
            }
           
        }

        return t;
    }

    public static void updateNotification(LoginProfile log, String id) {

        try {

            String q = ("UPDATE notification SET seen = ? WHERE id = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setBoolean(1, true);
            ps.setString(2, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
