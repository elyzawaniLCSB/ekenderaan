/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.ekenderaan.dao;

import static com.lcsb.ekenderaan.dao.StaffDAO.getInfo;
import com.lcsb.ekenderaan.model.Book;
import com.lcsb.ekenderaan.model.BookingDestination;
import com.lcsb.ekenderaan.model.Car;
import com.lcsb.ekenderaan.model.CoStaff;
import com.lcsb.ekenderaan.model.CoStaffDepartment;
import com.lcsb.ekenderaan.model.CompassionateLeave;
import com.lcsb.ekenderaan.model.ExecutiveChild;
import com.lcsb.ekenderaan.model.Leave;
import com.lcsb.ekenderaan.model.LeaveAttachment;
import com.lcsb.ekenderaan.model.LeaveComment;
import com.lcsb.ekenderaan.model.LeaveInfo;
import com.lcsb.ekenderaan.model.LeaveMultiTemp;
import com.lcsb.ekenderaan.model.LeaveReasonTemplate;
import com.lcsb.ekenderaan.model.LeaveRequest;
import com.lcsb.ekenderaan.model.LeaveType;
import com.lcsb.ekenderaan.model.LoginProfile;
import com.lcsb.ekenderaan.model.SysLogActivity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author fadhilfahmi
 */
public class LeaveDAO {

    public static double PrecisionDouble(Double value) {
        DecimalFormat df = new DecimalFormat("###########0.00");

        double number = Math.ceil(value * 100);

        double output = Double.parseDouble(df.format(number / 100));

        return output;

    }

    public static double twoDecimalDouble(Double value) {
        DecimalFormat df = new DecimalFormat("###########0.00");

        double output = Double.parseDouble(df.format(value));

        return output;

    }

    private static Leave getLeaveRS(ResultSet rs) throws SQLException, Exception {
        Leave cv = new Leave();

        cv.setCheckDate(rs.getString("check_date"));
        cv.setCheckID(rs.getString("checkID"));
        cv.setDateapply(rs.getString("dateapply"));
        cv.setDateend(rs.getString("dateend"));
        cv.setDatestart(rs.getString("datestart"));
        cv.setDays(rs.getInt("days"));
        cv.setHeadDate(rs.getString("head_date"));
        cv.setHeadID(rs.getString("headID"));
        cv.setHrDate(rs.getString("hr_date"));
        cv.setHrID(rs.getString("hrID"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setReason(rs.getString("reason"));
        cv.setStaffID(rs.getString("staffID"));
        cv.setSupervisorDate(rs.getString("supervisor_date"));
        cv.setSupervisorID(rs.getString("supervisorID"));
        cv.setType(rs.getString("type"));
        cv.setStatus(rs.getString("status"));
        cv.setYear(rs.getString("year"));
        cv.setPeriod(rs.getString("period"));
        cv.setStafflevel(rs.getInt("stafflevel"));
        cv.setChecktime(rs.getString("checktime"));
        cv.setHeadtime(rs.getString("headtime"));
        cv.setTimeapply(rs.getString("timeapply"));
        cv.setSpvtime(rs.getString("spvtime"));

        return cv;
    }

    public static String saveLeave(LoginProfile log, Leave item, String sessionid) throws Exception {

        //String leaveID = AutoGenerate.get4digitNo(log, "leave_master", "leaveID");
        String year = AccountingPeriod.getCurYearByDate(item.getDatestart());
        String period = AccountingPeriod.getCurPeriodByDate(item.getDatestart());

        String p = period;

        if (p.length() == 1) {
            p = "0" + p;
        }

        String leaveID = AutoGenerate.getLeaveID(log, item.getType(), year, p, period);
        String reason = item.getReason();
        if (item.getType().equals("C02")) {
            reason = "Rujuk Lampiran";
        } else if (item.getType().equals("C04")) {
            reason = "Spt Lampiran";
        } else if (item.getType().equals("C03")) {
            reason = "Spt Lampiran";
        }
        try {//sdsd
            String q = ("insert into leave_master(leaveID,dateapply,dateend,datestart,reason,type, staffID, days, year, period, status, stafflevel,timeapply) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, leaveID);
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, item.getDateend());
            ps.setString(4, item.getDatestart());
            ps.setString(5, reason);
            ps.setString(6, item.getType());
            ps.setString(7, log.getUserID());
            ps.setInt(8, item.getDays());
            ps.setString(9, year);
            ps.setString(10, period);
            ps.setString(11, "Preparing");
            ps.setInt(12, getUserLevel(log));
            ps.setString(13, AccountingPeriod.getCurrentTime());

            ps.executeUpdate();
            ps.close();

            if (item.getType().equals("C02")) {
                String q_attach = ("UPDATE leave_attachment SET leaveID = ? WHERE sessionid = ?");
                PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
                ps_attach.setString(1, leaveID);
                ps_attach.setString(2, sessionid);

                ps_attach.executeUpdate();
                ps_attach.close();
            } else if (item.getType().equals("C04")) {
                String q_attach = ("UPDATE leave_attachment SET leaveID = ? WHERE sessionid = ?");
                PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
                ps_attach.setString(1, leaveID);
                ps_attach.setString(2, sessionid);

                ps_attach.executeUpdate();
                ps_attach.close();
            } else if (item.getType().equals("C03")) {
                String q_attach = ("UPDATE leave_attachment SET leaveID = ? WHERE sessionid = ?");
                PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
                ps_attach.setString(1, leaveID);
                ps_attach.setString(2, sessionid);

                ps_attach.executeUpdate();
                ps_attach.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return leaveID;

    }

    public static String saveLeaveOldRecord(LoginProfile log, Leave item, String sessionid) throws Exception {

        String year = AccountingPeriod.getCurYearByDate(item.getDatestart());
        String period = AccountingPeriod.getCurPeriodByDate(item.getDatestart());

        String p = period;

        if (p.length() == 1) {
            p = "0" + p;
        }
        String leaveID = AutoGenerate.getLeaveID(log, item.getType(), year, p, period);
        String reason = item.getReason();
        if (item.getType().equals("C02")) {
            reason = "Rujuk Lampiran";
        }
        try {
            String q = ("insert into leave_master(leaveID,dateapply,dateend,datestart,reason,type, staffID, days, year, period, status, stafflevel) values (?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, leaveID);
            ps.setString(2, item.getDateapply());
            ps.setString(3, item.getDateend());
            ps.setString(4, item.getDatestart());
            ps.setString(5, reason);
            ps.setString(6, item.getType());
            ps.setString(7, item.getStaffID());
            ps.setInt(8, item.getDays());
            ps.setString(9, AccountingPeriod.getCurYearByDate(item.getDatestart()));
            ps.setString(10, AccountingPeriod.getCurPeriodByDate(item.getDatestart()));
            ps.setString(11, "Approved");
            ps.setInt(12, 0);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return leaveID;

    }

    public static void saveLeaveAttachment(LoginProfile log, String name, String sessionid) throws Exception {

        try {

            String q_attach = ("insert into leave_attachment(filename,sessionID, staffID) values (?,?,?)");
            PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
            ps_attach.setString(1, name);
            ps_attach.setString(2, sessionid);
            ps_attach.setString(3, log.getUserID());

            ps_attach.executeUpdate();
            ps_attach.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static int getUserLevel(LoginProfile log) throws SQLException {

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_level WHERE staffID=?");
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt("level");
        }

        return i;

    }

    public static List<Leave> getAllLeaveExcludeYou(LoginProfile log, String year, String period, String deptID) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";
        String q1 = "";

        if (!deptID.equals("None")) {
            q1 = "AND b.departmentID = '" + deptID + "'";
        }

        if (log.getAccessLevel() == 1) {

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'C02' AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'C02' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'C02')";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'C02')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            String ql1 = "";
            String ql2 = "";

            if (LeaveDAO.isSuperViseeExistForLevel3(log, "support")) {
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            } else if (LeaveDAO.isSuperViseeExistForLevel3(log, "approve")) {
                ql2 = "";
            }

            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) " + ql1 + ")";

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR (a.status = 'Checked' AND stafflevel = 2)  OR (a.status = 'Checked' AND stafflevel = 0) OR  (a.status = 'Checked' AND type = 'C02')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) OR (a.status = 'Checked' AND stafflevel = 3)";
            CVi.addAll(LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1));

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        }

        return CVi;
    }

    public static List<CoStaff> getAllLeaveExcludeYouGroupByStaff(LoginProfile log, String year, String period) throws Exception {

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        String q = "";
        String q1 = "";

        if (log.getAccessLevel() == 1) {

            if (!log.getLocID().equals("9911")) {
                q = "AND ((b.locID = '" + log.getLocID() + "' AND b.departmentID = '" + log.getDeptID() + "') OR (b.locID = '" + log.getLocID() + "' AND b.departmentID  NOT IN ('0003','0004')))";
            }

        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'C02' AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'C02' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'C02')";

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'C02')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            String ql1 = "";
            String ql2 = "";

            if (LeaveDAO.isSuperViseeExistForLevel3(log, "support")) {
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            } else if (LeaveDAO.isSuperViseeExistForLevel3(log, "approve")) {
                ql2 = "";
            }

            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) " + ql1 + ")";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

        }

        String k = "select * from leave_master a, co_staff b WHERE  a.staffID = b.staffID  AND a.staffID <> 'P0157' AND (datestart >= CURDATE() OR dateend >= CURDATE())  AND a.staffID IN ('P0826','P0098','P0426','P0269','P0350','P0403','P0490')  AND  ( ( type <> 'C02'  AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Rejected') ) OR ( type = 'C02'  AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Rejected')  ) ) group by a.staffID ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected'), dateapply desc";

        String j = "select * from leave_master a, co_staff b WHERE  a.staffID = b.staffID " + q1 + " AND a.staffID <> '" + log.getUserID() + "'  AND (datestart >= CURDATE() OR dateend >= CURDATE()) " + q + " group by a.staffID ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected'), dateapply desc";
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--thequery--" + String.valueOf(j));

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "querygroupbystaff : " + j);

        try {
            PreparedStatement stmt = log.getCon().prepareStatement(j);
            //stmt.setString(1, dept);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Leave> getAllLeaveExcludeYouGridByStaff(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";
        String q1 = "";

        if (log.getAccessLevel() == 1) {

            if (log.getLocID().equals("9911")) {
                q = "AND (a.status = 'Preparing') and (b.locID = '" + log.getLocID() + "' OR (b.locID <> '" + log.getLocID() + "' AND b.departmentID IN ('0003','0004')))";
            } else {
                q = "AND (a.status = 'Preparing') and ((b.locID = '" + log.getLocID() + "' AND b.departmentID = '" + log.getDeptID() + "') OR (b.locID = '" + log.getLocID() + "' AND b.departmentID  NOT IN ('0003','0004')))";
            }

        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'C02' AND (a.status = 'Checked' OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'C02' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'C02')";

        } else if (log.getAccessLevel() == 3) {//level head dept

            String ql1 = "";

            ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked')";

            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR (a.status = 'Checked' AND type = 'C02')) " + ql1 + ") ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

        } else if (log.getAccessLevel() == 0) {//level HR dept

            q = "AND a.staffID = '" + log.getUserID() + "' ";

        }

        String j = "select * from leave_master a, co_staff b WHERE  a.staffID = b.staffID " + q + "  ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected'), dateapply desc";
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--thequery--" + String.valueOf(j));

        //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "querygroupbystaff : "+ j);
        try {
            PreparedStatement stmt = log.getCon().prepareStatement(j);
            //stmt.setString(1, dept);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Leave> getAllLeaveExcludeYouByDept(LoginProfile log, String year, String period, String staffID) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";
        String q1 = "";

        if (!staffID.equals("None")) {
            q1 = "AND b.staffID = '" + staffID + "'";
        }

        if (log.getAccessLevel() == 1) {

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'C02' AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID = '" + staffID + "'  AND type <> 'C02' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'C02')";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'C02')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            String ql1 = "";
            String ql2 = "";

            if (LeaveDAO.isSuperViseeExistForLevel3(log, "support")) {
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            } else if (LeaveDAO.isSuperViseeExistForLevel3(log, "approve")) {
                ql2 = "";
            }

            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) " + ql1 + ")";

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR (a.status = 'Checked' AND stafflevel = 2)  OR (a.status = 'Checked' AND stafflevel = 0) OR  (a.status = 'Checked' AND type = 'C02')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) OR (a.status = 'Checked' AND stafflevel = 3)";
            CVi.addAll(LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1));

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        }

        return CVi;
    }

    public static List<Leave> getAllLeaveExcludeYouExecuteQuery(LoginProfile log, String query, String query1) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master a, co_staff b WHERE  a.staffID = b.staffID " + query1 + " AND a.staffID <> '" + log.getUserID() + "' AND (datestart >= CURDATE() OR dateend >= CURDATE()) " + query + "   ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected'), dateapply desc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--" + String.valueOf(stmt));
            while (rs.next()) {

                Leave cv = new Leave();

                cv.setCheckDate(rs.getString("check_date"));
                cv.setCheckID(rs.getString("checkID"));
                cv.setDateapply(rs.getString("dateapply"));
                cv.setDateend(rs.getString("dateend"));
                cv.setDatestart(rs.getString("datestart"));
                cv.setDays(rs.getInt("days"));
                cv.setHeadDate(rs.getString("head_date"));
                cv.setHeadID(rs.getString("headID"));
                cv.setHrDate(rs.getString("hr_date"));
                cv.setHrID(rs.getString("hrID"));
                cv.setLeaveID(rs.getString("leaveID"));
                cv.setReason(rs.getString("reason"));
                cv.setStaffID(rs.getString("a.staffID"));
                cv.setSupervisorDate(rs.getString("supervisor_date"));
                cv.setSupervisorID(rs.getString("supervisorID"));
                cv.setType(rs.getString("type"));
                cv.setStatus(rs.getString("status"));
                cv.setYear(rs.getString("year"));
                cv.setPeriod(rs.getString("period"));
                cv.setStafflevel(rs.getInt("stafflevel"));

                CVi.add(cv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Leave> getAllLeave(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";

        if (log.getAccessLevel() == 0) {
            q += "AND a.staffID = '" + log.getUserID() + "'";
        } else if (log.getAccessLevel() == 2 || log.getAccessLevel() == 3) {
            q += "AND (a.staffID IN " + LeaveDAO.getSuperviseeID(log) + " OR a.staffID = '" + log.getUserID() + "')";
        } else if (log.getAccessLevel() == 1) {
            q += "AND b.locID = '" + log.getLocID() + "'";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master a, co_staff b WHERE a.staffID = b.staffID and year = '" + AccountingPeriod.getCurYearByCurrentDate() + "' " + q + " ORDER BY a.leaveID DESC");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "-----reportall-" + stmt);
            while (rs.next()) {
                CVi.add(getLeaveRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();//3
        }

        return CVi;
    }

    public static List<Leave> getAllLeaveOfYou(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master WHERE staffID = '" + log.getUserID() + "' AND year = '" + AccountingPeriod.getCurYearByCurrentDate() + "' ORDER BY datestart DESC");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static Leave getLeaveInfoDetail(LoginProfile log, String no) throws SQLException, Exception {
        Leave c = new Leave();
        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_master WHERE leaveID=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getLeaveRS(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static LeaveInfo getLeaveInfo(LoginProfile log, String no, String year) throws SQLException, Exception {
        LeaveInfo c = new LeaveInfo();
        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
            stmt.setString(1, no);
            stmt.setString(2, year);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getLeaveInfoRS(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static LeaveInfo getLeaveInfoRS(ResultSet rs) throws SQLException, Exception {
        LeaveInfo cv = new LeaveInfo();

        cv.setBf(rs.getInt("bf"));
        cv.setEligibleleave(rs.getInt("eligibleleave"));
        cv.setId(rs.getInt("id"));
        cv.setStaffID(rs.getString("staffID"));
        cv.setYear(rs.getString("year"));
        cv.setMc(rs.getInt("mc"));
        cv.setHosp(rs.getInt("hosp"));
        cv.setMaternity(rs.getInt("maternity"));

        return cv;
    }

    public static LeaveInfo getEligibleLeaveDetail(LoginProfile log, String no) throws SQLException, Exception {
        LeaveInfo c = new LeaveInfo();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? and year = '" + AccountingPeriod.getCurYearByCurrentDate() + "'");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveInfoRS(rs);
        }

        return c;
    }

    public static int getTotalLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and status = ? and type not in ('C02','C03','C04')");
        stmt.setString(1, no);
        stmt.setString(2, year);
        stmt.setString(3, "Approved");
        //stmt.setString(4, "C02");

        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getTotalSickLeaveUseOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and status = ? and type = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        stmt.setString(3, "Approved");
        stmt.setString(4, "C02");
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");

        }

        return bal;
    }

    public static int getTotalHospitalLeaveUseOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and status = ? and type = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        stmt.setString(3, "Approved");
        stmt.setString(4, "C03");
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getTotalSickLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT mc as cnt FROM leave_info WHERE staffID=? and year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getTotalHospitalLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT hosp as cnt FROM leave_info WHERE staffID=? and year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getBalanceTotalSickLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = getTotalSickLeaveOfTheYear(log, no, year) - getTotalSickLeaveUseOfTheYear(log, no, year);

        return bal;
    }

    public static int getBalanceTotalHospitalLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = getTotalHospitalLeaveOfTheYear(log, no, year) - getTotalHospitalLeaveUseOfTheYear(log, no, year);

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "---bal---" + no + getTotalHospitalLeaveOfTheYear(log, no, year));

        return bal;
    }

    public static int getTotalLeaveOfTheMonth(LoginProfile log, String no, String year, String period) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and period = ? and status = ? and type not in ('C02','C04','C03')");
        stmt.setString(1, no);
        stmt.setString(2, year);
        stmt.setString(3, period);
        stmt.setString(4, "Approved");
        //stmt.setString(5, "C02");
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------" + stmt);
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "***" + rs.getInt("cnt"));
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "***" + bal);
        }

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "***" + bal);

        return bal;
    }

    public static double getEligibleLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;

        bal = LeaveDAO.getEligibleLeaveWithoutCF(log, no, year) + LeaveDAO.getPreviousLeaveCF(log, no, year);

        return bal;
    }

    public static double getEligibleLeaveWithoutCF(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("eligibleleave");
        }

        return bal;
    }

    public static double getMedicalLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("mc");
        }

        return bal;
    }

    public static double getHospitalLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("hosp");
        }

        return bal;
    }

    public static double getPreviousLeaveCF(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("bf");
        }

        return bal;
    }

    public static double getBalanceEligibleLeave(LoginProfile log, String staffID, String year) throws Exception {

        double bal = getEligibleLeaveOfTheYear(log, staffID, year) - getTotalLeaveOfTheYear(log, staffID, year);

        return bal;

    }

    public static void updateLeave(LoginProfile log, Leave item) throws Exception {

        try {
            String q = ("UPDATE leave_master SET dateend = ?,datestart = ?,reason = ?,type = ?, days = ?, year = ?, period = ?  WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, item.getDateend());
            ps.setString(2, item.getDatestart());
            ps.setString(3, item.getReason());
            ps.setString(4, item.getType());
            ps.setInt(5, item.getDays());
            ps.setString(6, AccountingPeriod.getCurYearByDate(item.getDatestart()));
            ps.setString(7, AccountingPeriod.getCurPeriodByDate(item.getDatestart()));
            ps.setString(8, item.getLeaveID());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteLeave(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM leave_master WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

            LeaveDAO.deleteLeaveAttachment(log, id);
            LeaveDAO.deleteLeaveComment(log, id);
            LeaveDAO.deleteLeaveRequest(log, id);
            LeaveDAO.deleteNoti(log, id);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteLeaveAttachment(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM leave_attachment WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteLeaveComment(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM leave_comment WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteLeaveRequest(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM leave_request WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteNoti(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM notification WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteSysLog(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM sys_log_activity WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getBadgeColor(String status) {

        String color = "dark";

        if (status.equals("Approved")) {
            color = "success";
        } else if (status.equals("Preparing")) {
            color = "blue";
        } else if (status.equals("Checked")) {
            color = "warning";
        } else if (status.equals("Supported")) {
            color = "pink";
        } else if (status.equals("Verified")) {
            color = "success";
        } else {
            color = "danger";
        }

        return color;
    }

    public static String getBadgeColorTypeLeave(String status) {

        String color = "dark";

        if (status.equals("C02")) {
            color = "danger";
        } else if (status.equals("Cuti Kecemasan")) {
            color = "danger";
        } else if (status.equals("C01")) {
            color = "success";
        } else if (status.equals("C04")) {
            color = "pink";
        } else if (status.equals("C03")) {
            color = "warning";
        } else if (status.equals("C05")) {
            color = "blue";
        } else if (status.equals("Cuti Ganti")) {
            color = "grey";
        }

        return color;
    }

    public static double getEligibleLeaveForTheMonth(LoginProfile log, String staffID, String year, String date) throws Exception {

        double i = 0;

        double CL = LeaveDAO.getEligibleLeaveWithoutCF(log, staffID, year);
        double curMonth = Double.parseDouble(AccountingPeriod.getCurPeriodByDate(date));

        double beforeAddCF = CL / 12 * curMonth;
        double afterAddCF = beforeAddCF + LeaveDAO.getPreviousLeaveCF(log, staffID, year);
        double plusLeaveUse = afterAddCF - LeaveDAO.getTotalLeaveOfTheYear(log, staffID, year);
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "date : " + date + "------leave : " + beforeAddCF + " , CL : " + CL + " , addCF  : " + plusLeaveUse);

//        double balLeave = LeaveDAO.getBalanceEligibleLeave(log, staffID, year);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balleave" + String.valueOf(balLeave));
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balmonth" + String.valueOf(getBalanceMonthofYear(date)));
//
//        double e = balLeave / getBalanceMonthofYear(date);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------leave--" + String.valueOf(e));
        //String number = String.valueOf(d);
        //number = number.substring(number.indexOf(".")).substring(1);
        return plusLeaveUse;

    }

    public static double getEligibleLeaveForTheMonthExcludeApprovedThisMonth(LoginProfile log, String staffID, String year, String date) throws Exception {

        double i = 0;

        double CL = LeaveDAO.getEligibleLeaveWithoutCF(log, staffID, year);
        double curMonth = Double.parseDouble(AccountingPeriod.getCurPeriodByDate(date));

        double beforeAddCF = CL / 12 * curMonth;
        double afterAddCF = beforeAddCF + LeaveDAO.getPreviousLeaveCF(log, staffID, year);
        double plusLeaveUse = afterAddCF - LeaveDAO.getTotalLeaveOfTheYear(log, staffID, year) + LeaveDAO.getTotalLeaveOfTheMonth(log, staffID, year, AccountingPeriod.getCurPeriodByDate(date));
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------leave : " + beforeAddCF + " , CL : " + CL + " , addCF  : " + afterAddCF);

//        double balLeave = LeaveDAO.getBalanceEligibleLeave(log, staffID, year);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balleave" + String.valueOf(balLeave));
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balmonth" + String.valueOf(getBalanceMonthofYear(date)));
//
//        double e = balLeave / getBalanceMonthofYear(date);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------leave--" + String.valueOf(e));
        //String number = String.valueOf(d);
        //number = number.substring(number.indexOf(".")).substring(1);
        return LeaveDAO.PrecisionDouble(plusLeaveUse);

    }

    public static int getPreparedLeave(LoginProfile log) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  status = ?");
        //stmt.setString(1, no);
        stmt.setString(1, "Preparing");
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getLeaveInProcess(LoginProfile log) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  status <> ? AND status <> ? and staffID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, "Approved");
        stmt.setString(2, "Rejected");
        stmt.setString(3, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getBalanceMonthofYear(String date) throws Exception {

        int b = 0;

        String currentMonth = AccountingPeriod.getCurPeriodByDate(date);

        int balMonth = 12 - Integer.parseInt(currentMonth) + 1;
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balMonth" + String.valueOf(balMonth));
        if (balMonth == 0) {
            balMonth = 1;
        }

        return balMonth;

    }

    public static double getPercentLeaveUseYear(LoginProfile log, String staffID, String year) throws Exception {

        double l = 0;

        double totLeaveUse = LeaveDAO.getTotalLeaveOfTheYear(log, staffID, year);
        double totLeaveYear = LeaveDAO.getEligibleLeaveOfTheYear(log, staffID, year);

        l = totLeaveUse * 100 / totLeaveYear;

        return l;
    }

    public static double getPercentMedicalLeaveUseYear(LoginProfile log, String staffID, String year) throws Exception {

        double l = 0;

        double totLeaveUse = LeaveDAO.getTotalSickLeaveUseOfTheYear(log, staffID, year);
        double totLeaveYear = LeaveDAO.getMedicalLeaveOfTheYear(log, staffID, year);

        l = totLeaveUse * 100 / totLeaveYear;

        return l;
    }

    public static double getPercentHospitalLeaveUseYear(LoginProfile log, String staffID, String year) throws Exception {

        double l = 0;

        double totLeaveUse = LeaveDAO.getTotalHospitalLeaveUseOfTheYear(log, staffID, year);
        double totLeaveYear = LeaveDAO.getHospitalLeaveOfTheYear(log, staffID, year);

        l = totLeaveUse * 100 / totLeaveYear;

        return l;
    }

    public static double getPercentLeaveUseMonth(LoginProfile log, String staffID, String year, String period, String date) throws Exception {

        double l = 0;

        double totLeaveUse = LeaveDAO.getTotalLeaveOfTheMonth(log, staffID, year, period);
        double totLeaveYear = LeaveDAO.getEligibleLeaveForTheMonthExcludeApprovedThisMonth(log, staffID, year, date);

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------getPercentLeaveUseMonth " + totLeaveYear);

        l = totLeaveUse * 100 / totLeaveYear;

        return l;
    }

    public static String getProgressBarColor(LoginProfile log, double percent) throws Exception {

        String c = "";

        if (percent < 31) {
            c = "success";
        } else if (percent > 30 && percent < 61) {
            c = "warning";
        } else if (percent > 60) {
            c = "danger";
        }

        return c;
    }

    public static void validateLeave(LoginProfile log, String leaveID, String action) throws Exception {

        String colID = "";
        String colDate = "";
        String colTime = "";

        Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, leaveID);

        if (log.getAccessLevel() == 1) {

            colID = "checkID";
            colDate = "check_date";
            colTime = "checktime";

        } else if (log.getAccessLevel() == 2) {

            colID = "supervisorID";
            colDate = "supervisor_date";
            colTime = "spvtime";

        } else if (log.getAccessLevel() == 3) {

            if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("support")) {
                colID = "supervisorID";
                colDate = "supervisor_date";
                colTime = "spvtime";
            } else if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("approve")) {
                colID = "headID";
                colDate = "head_date";
                colTime = "headtime";
            }

        } else if (log.getAccessLevel() == 4) {

            colID = "hrID";
            colDate = "hr_date";

        }

        try {
            String q = ("UPDATE leave_master SET status = ?, " + colID + " = ?, " + colDate + " = ? , " + colTime + " = ?  WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, action);
            ps.setString(2, log.getUserID());
            ps.setString(3, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(4, AccountingPeriod.getCurrentTime());
            ps.setString(5, leaveID);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static int getLeaveCountForSupervisee(LoginProfile log) throws SQLException, Exception {

        String q = "";
        String status = "";

        if (log.getAccessLevel() == 1) {

            if (!log.getLocID().equals("9911")) {
                q = "AND a.status = 'Preparing' AND ((b.locID = '" + log.getLocID() + "' AND b.departmentID = '" + log.getDeptID() + "') OR (b.locID = '" + log.getLocID() + "' AND b.departmentID  NOT IN ('0003','0004')))";
            } else {
                q = "AND (a.status = 'Preparing') and (b.locID = '" + log.getLocID() + "' OR (b.locID <> '" + log.getLocID() + "' AND b.departmentID IN ('0003','0004')))";
            }

            // q = "AND a.status = 'Preparing' AND (b.locID = '" + log.getLocID() + "' OR (b.locID <> '9911' AND b.departmentID IN ('0003','0004')))";
        } else if (log.getAccessLevel() == 2) {
            q = "AND a.staffiD IN " + getSuperviseeID(log) + " AND a.status = 'Checked'";
            //status = "status = 'Checked'";
        } else if (log.getAccessLevel() == 3) {
            q = "AND ((a.staffiD IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND a.status = 'Checked') OR (a.staffiD IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND a.status = 'Supported'))";
            //status = "(status = 'Checked' OR status = 'Supported')";
        }

        int i = 0;

        //if (isSuperViseeExist(log)) {
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master a, co_staff b WHERE a.staffID = b.staffID AND a.staffID <> ?  " + q);
        //stmt.setString(1, status);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------qs " + stmt);

        while (rs.next()) {
            i = rs.getInt("cnt");
        }
        //}

        return i;
    }

    public static boolean isSuperViseeExistForLevel3(LoginProfile log, String type) throws SQLException, Exception {

        boolean b = false;

        String colID = "";

        if (type.equals("support")) {
            colID = "supervisorID";
        } else if (type.equals("approve")) {
            colID = "headID";
        }

        if (log.getAccessLevel() != 1) {

            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
            //stmt.setString(1, no);
            stmt.setString(1, log.getUserID());
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------q " + stmt);
            if (rs.next()) {

                b = true;
            }

        }

        return b;

    }

    public static boolean isSuperViseeExist(LoginProfile log) throws SQLException, Exception {

        boolean b = false;

        String colID = "";

        if (log.getAccessLevel() == 2) {
            colID = "supervisorID";
        } else if (log.getAccessLevel() == 3) {
            colID = "headID";
        }

        if (log.getAccessLevel() != 1) {

            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
            //stmt.setString(1, no);
            stmt.setString(1, log.getUserID());
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------q " + stmt);
            if (rs.next()) {

                b = true;
            }

        }

        return b;

    }

    public static String getSuperviseeID(LoginProfile log) throws SQLException, Exception {

        String colID = "";

        if (log.getAccessLevel() == 2) {
            colID = "supervisorID";
        } else if (log.getAccessLevel() == 3) {
            colID = "headID";
        }

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("staffID") + "'";
            } else {
                s += "'" + rs.getString("staffID") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }

    public static String getSuperviseeIDByLevel(LoginProfile log, String col) throws SQLException, Exception {

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + col + " = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("staffID") + "'";
            } else {
                s += "'" + rs.getString("staffID") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }

    public static boolean isButtonDisable(LoginProfile log, String leaveID) throws Exception {

        boolean b = false;

        Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, leaveID);

        if (log.getAccessLevel() == 2 && (l.getStatus().equals("Supported") || l.getStatus().equals("Approved") || l.getStatus().equals("Rejected")) || (l.getStaffID().equals(log.getUserID()))) {
            b = true;
        } else if (log.getAccessLevel() == 1 && (l.getStatus().equals("Checked") || l.getStatus().equals("Supported") || l.getStatus().equals("Approved") || l.getStatus().equals("Rejected"))) {
            b = true;
        } else if (log.getAccessLevel() == 3 && (l.getStatus().equals("Supported") || l.getStatus().equals("Approved") || l.getStatus().equals("Rejected")) && !(l.getStaffID().equals(log.getUserID()))) {

            if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("support") && l.getStatus().equals("Supported")) {
                b = true;
            } else if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("approve") && l.getStatus().equals("Approved")) {
                b = true;
            }

        } else if (log.getAccessLevel() == 0) {
            b = true;
        }//else if(log.getAccessLevel() == 4 && (l.getStatus().equals("Verified"))){
        //    b = true;
        // }

        if (log.getAccessLevel() == 1 && l.getStatus().equals("Preparing")) {
            b = false;
        }

        return b;

    }

    public static boolean isLeaveInfoExist(LoginProfile log, String staffID, String date) throws SQLException, Exception {

        boolean b = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE  staffID = ? AND year = ?");
        //stmt.setString(1, no);
        stmt.setString(1, staffID);
        stmt.setString(2, AccountingPeriod.getCurYearByDate(date));
        rs = stmt.executeQuery();
        if (rs.next()) {

            b = true;
        }

        return b;

    }

    public static String getLevelThreeSupportOrApprove(LoginProfile log, String staffID) throws SQLException, Exception {

        String isa = "";

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE  staffID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, staffID);
        rs = stmt.executeQuery();
        if (rs.next()) {

            String i = rs.getString("supervisorID");
            String j = rs.getString("headID");

            if (i.equals(log.getUserID())) {
                isa = "support";
            }

            if (j.equals(log.getUserID())) {
                isa = "approve";
            }
        }

        return isa;

    }

    public static void insertLeave(LoginProfile log, LeaveInfo item) throws Exception {

        try {
            String q = ("INSERT INTO leave_info(staffID, year, bf, eligibleleave, mc, hosp, maternity) values (?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, item.getStaffID());
            ps.setString(2, item.getYear());
            ps.setInt(3, item.getBf());
            ps.setInt(4, item.getEligibleleave());
            ps.setInt(5, item.getMc());
            ps.setInt(6, 60);
            ps.setInt(7, 60);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateLeaveInfo(LoginProfile log, LeaveInfo item) throws Exception {

        try {
//            String q = ("UPDATE leave_info SET bf = ?, eligibleleave = ?, mc = ? where staffID = ? and year = ?");

            LeaveInfo li = (LeaveInfo) LeaveDAO.getLeaveInfo(log, item.getStaffID(), item.getYear());

            if (li.getEligibleleave() == null) {
                LeaveDAO.insertLeave(log, item);
            } else {

                String q = ("UPDATE leave_info SET bf = ?, eligibleleave = ?, mc = ? where staffID = ? and year = ?");
                //String q = ("UPDATE leave_master SET status = ?, " + colID + " = ?, " + colDate + " = ? , " + colTime + " = ?  WHERE leaveID = ?");

                PreparedStatement ps = log.getCon().prepareStatement(q);

                ps.setInt(1, item.getBf());
                ps.setInt(2, item.getEligibleleave());
                ps.setInt(3, item.getMc());
                ps.setString(4, item.getStaffID());
                ps.setString(5, item.getYear());
                //ps.setString(5, item.getYear());

                ps.executeUpdate();
                ps.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static JSONArray getAllLeaveForCalendar(LoginProfile log, String filter) throws Exception {

        JSONArray array = new JSONArray();

        List<Leave> bdt = LeaveDAO.getAllConfirmedLeave(log, filter);

        for (Leave j : bdt) {

            if (j.getDays() > 1) {
                j.setDateend(AccountingPeriod.addOneDay(j.getDateend()));
            }

            //List<BookingDestination> ls = BookingDAO.getListDestinationByBookID(log, j.getBookID());
            //for (BookingDestination i : ls) {
            String typeColor = "";

            if (j.getType().equals("C01")) {
                typeColor = "success";
            } else {
                typeColor = "danger";
            }

            JSONObject jo = new JSONObject();
            jo.put("id", j.getLeaveID());
            //jo.put("title", "<img src = \""+MemberDAO.getMemberInfo(log, j.getStaffID()).getImageURL()+"\">-"+ i.getDestdescp());
            jo.put("title", StaffDAO.getInfo(log, j.getStaffID()).getName());
            jo.put("start", j.getDatestart());
            jo.put("end", j.getDateend());
            jo.put("className", "fc-event-" + typeColor);
            jo.put("imageurl", StaffDAO.getInfo(log, j.getStaffID()).getImageURL());
            array.add(jo);
            jo = null;

            //}
        }

        return array;

    }

    public static List<Leave> getAllConfirmedLeave(LoginProfile log, String filter) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = "";

        if (!filter.equals("none")) {

            if (log.getAccessLevel() == 0) {//normal user level : department view
                restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";
            }

            if (log.getAccessLevel() == 1) {//level leave admin : all staff view
                restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";
            }

            if (log.getAccessLevel() == 2) {//level supervisor :  : department view

                restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";

            } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

                restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";

            } else if (log.getAccessLevel() == 4) {//level HR dept

                //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
            }

        } else if (filter.equals("none")) {

        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID AND " + restrictByLevel + "  a.status = 'Approved'  order by a.leaveID asc ");
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID AND " + restrictByLevel + "  a.status = 'Approved'  order by a.leaveID asc");

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static int getCountConfirmedLeave(LoginProfile log) throws Exception, SQLException {

        int cnt = 0;

        Statement stmt = null;

        String restrictByLevel = "";

        if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view

        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved'");
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "-SELECT count(*) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved'");

            if (rs.next()) {
                cnt = rs.getInt("cnt");
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cnt;
    }

    public static int getAllLeaveForStaff(LoginProfile log) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  staffID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getCountSuperviseeLeave(LoginProfile log) throws SQLException, Exception {
        int bal = 0;

        if (LeaveDAO.isSuperViseeExist(log)) {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  staffID IN " + LeaveDAO.getSuperviseeID(log));
            //stmt.setString(1, no);
            //stmt.setString(1, log.getUserID());
            rs = stmt.executeQuery();
            if (rs.next()) {

                bal = rs.getInt("cnt");
            }
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));
        }

        return bal;
    }

    private static LeaveAttachment getLeaveAttachmentRS(ResultSet rs) throws SQLException, Exception {
        LeaveAttachment cv = new LeaveAttachment();

        cv.setFilename(rs.getString("filename"));
        cv.setId(rs.getInt("id"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setSessionID(rs.getString("sessionID"));
        cv.setStaffID(rs.getString("staffID"));

        return cv;
    }

    public static LeaveAttachment getLeaveAttachment(LoginProfile log, String no) throws SQLException, Exception {
        LeaveAttachment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_attachment WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveAttachmentRS(rs);
        }

        return c;
    }

    public static String getMalayWord(String word) {

        String newword = "";

        if (word.equals("Approved")) {
            newword = "Lulus";
        } else if (word.equals("Preparing")) {
            newword = "Baru";
        } else if (word.equals("Checked")) {
            newword = "Semak";
        } else if (word.equals("Supported")) {
            newword = "Sokong";
        } else if (word.equals("Verified")) {
            newword = "Sah";
        } else if (word.equals("Rejected")) {
            newword = "Batal";
        }

        return newword;
    }

    public static String getMalayWord2(String word) {

        String newword = "";

        if (word.equals("Approved")) {
            newword = "Lulus";
        } else if (word.equals("Preparing")) {
            newword = "Baru";
        } else if (word.equals("Checked")) {
            newword = "Disemak";
        } else if (word.equals("Supported")) {
            newword = "Disokong";
        } else if (word.equals("Verified")) {
            newword = "Sah";
        } else if (word.equals("Rejected")) {
            newword = "Batal";
        } else if (word.equals("cancel")) {
            newword = "Batal?";
        }

        return newword;
    }

    public static List<Leave> getAllConfirmedLeaveToday(LoginProfile log, String filter) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = " b.locID = '" + log.getLocID() + "' and ";

        /*if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view

        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }*/
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "  a.status = 'Approved' order by a.leaveID asc ");

            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "  a.status = 'Approved' order by a.leaveID asc ");

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<Leave> getAllLeaveTodayByDept(LoginProfile log, String date, String deptID) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        date = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN datestart AND dateend)  and b.departmentID = '" + deptID + "' order by a.leaveID asc ");

            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN datestart AND dateend)  and b.departmentID = '" + deptID + "' order by a.leaveID asc "));

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static JSONArray getAnalysisData(LoginProfile log, String type) throws Exception {

        JSONArray array = new JSONArray();

        for (int i = 1; i <= 12; i++) {

            array.add(LeaveDAO.getCountConfirmedLeaveForChart(log, type, AccountingPeriod.getCurYearByCurrentDate(), String.valueOf(i)));
        }

        return array;

    }

    public static int getCountConfirmedLeaveForChart(LoginProfile log, String type, String year, String period) throws Exception, SQLException {

        int cnt = 0;

        Statement stmt = null;

        String restrictByLevel = "";

        //if (log.getAccessLevel() == 0) {//normal user level : department view//aaaaa
        restrictByLevel = "  staffID = '" + log.getUserID() + "' AND ";
        //}

//        if (log.getAccessLevel() == 1) {//level leave admin : all staff view
//
//           
//
//        }
//        
//        if (log.getAccessLevel() == 2) {//level supervisor :  : department view
//
//           restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
//
//        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 
//
//           restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
//
//        } else if (log.getAccessLevel() == 4) {//level HR dept
//
//            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
//
//        }
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT sum(days) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved' AND year = '" + year + "' AND period = '" + period + "' AND type = '" + type + "'");
            //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT sum(days) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved' AND year = '" + year + "' AND period = '" + period + "' AND type = '" + type + "'");

            if (rs.next()) {
                cnt = rs.getInt("cnt");
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cnt;
    }

    public static boolean isNewLeaveExceedEligibleMonth(LoginProfile log, String staffID, String year, String date, int newLeave, String leaveType) throws SQLException, Exception {

        boolean b = false;

        double elM = 0.0;

        if (leaveType.equals("C01")) {
            elM = LeaveDAO.getEligibleLeaveForTheMonth(log, staffID, year, date);
        } else if (leaveType.equals("C02")) {
            elM = LeaveDAO.getMedicalLeaveOfTheYear(log, staffID, year) - LeaveDAO.getTotalSickLeaveUseOfTheYear(log, staffID, year);
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "allmc : " + LeaveDAO.getMedicalLeaveOfTheYear(log, staffID, year) + " usemc :'" + LeaveDAO.getTotalSickLeaveUseOfTheYear(log, staffID, year));
        } else if (leaveType.equals("C03")) {
            elM = LeaveDAO.getHospitalLeaveOfTheYear(log, staffID, year) - LeaveDAO.getTotalHospitalLeaveUseOfTheYear(log, staffID, year);
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "allmc : " + LeaveDAO.getHospitalLeaveOfTheYear(log, staffID, year) + " usemc :'" + LeaveDAO.getTotalHospitalLeaveUseOfTheYear(log, staffID, year));
        }
        /*else if (leaveType.equals("C05")) {
            elM = LeaveDAO.getIhsanLeaveOfTheYear(log, staffID, year) - LeaveDAO.getTotalIhsanLeaveUseOfTheYear(log, staffID, year);
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "allmc : " + LeaveDAO.getHospitalLeaveOfTheYear(log, staffID, year) + " usemc :'" + LeaveDAO.getTotalHospitalLeaveUseOfTheYear(log, staffID, year));
        }*/

        if (newLeave > elM) {
            b = true;
        }

        return b;

    }

    public static void saveComment(LoginProfile log, LeaveComment lv) throws Exception {

        try {

            String q_attach = ("insert into leave_comment(comment, date, leaveID, staffID, time) values (?,?,?,?,?)");
            PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
            ps_attach.setString(1, lv.getComment());
            ps_attach.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps_attach.setString(3, lv.getLeaveID());
            ps_attach.setString(4, log.getUserID());
            ps_attach.setString(5, AccountingPeriod.getCurrentTime());

            ps_attach.executeUpdate();
            ps_attach.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static LeaveComment getLeaveCommentRS(ResultSet rs) throws SQLException, Exception {
        LeaveComment lv = new LeaveComment();

        lv.setComment(rs.getString("comment"));
        lv.setDate(rs.getString("date"));
        lv.setId(rs.getInt("id"));
        lv.setLeaveID(rs.getString("leaveID"));
        lv.setStaffID(rs.getString("staffID"));
        lv.setTime(rs.getString("time"));

        return lv;
    }

    public static LeaveComment getLeaveComment(LoginProfile log, String no) throws SQLException, Exception {
        LeaveComment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_comment WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveCommentRS(rs);
        }

        return c;
    }

    public static LeaveComment getLeaveCommentLast(LoginProfile log, String no) throws SQLException, Exception {
        LeaveComment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM `leave_comment` WHERE id=(SELECT MAX(id) FROM `leave_comment` where leaveID = ?);");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveCommentRS(rs);
        }

        return c;
    }

    public static LeaveComment getLeaveCommentByCommentID(LoginProfile log, String no) throws SQLException, Exception {
        LeaveComment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_comment WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveCommentRS(rs);
        }

        return c;
    }

    public static List<LeaveComment> getAllLeaveComment(LoginProfile log, String leaveID) throws Exception {

        ResultSet rs = null;
        List<LeaveComment> CVi;
        CVi = new ArrayList();
        LeaveComment c = new LeaveComment();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_comment WHERE leaveID = ? ORDER BY id DESC");
            stmt.setString(1, leaveID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveCommentRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();//3
        }

        return CVi;
    }

    public static boolean hasComment(LoginProfile log, String no) throws SQLException, Exception {
        boolean c = false;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_comment WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static List<CoStaff> getSuperviseeListInfo(LoginProfile log) throws SQLException, Exception {

        String colID = "";

        if (log.getAccessLevel() == 2) {
            colID = "supervisorID";
        } else if (log.getAccessLevel() == 3) {
            colID = "headID";
        }

        String s = "(";
        ResultSet rs = null;

        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();

        while (rs.next()) {
            CVi.add(StaffDAO.getInfo(log, rs.getString("staffID")));
        }

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return CVi;

    }

    public static List<Leave> getAllLeaveByStaffID(LoginProfile log, String staffID, String type) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master WHERE staffID = '" + staffID + "' AND type = '" + type + "' ORDER BY leaveID DESC");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static void saveDummyLeave(LoginProfile log, String staffID) throws Exception {

        try {
            String q = ("INSERT INTO leave_info(staffID, year, bf, eligibleleave,mc) values (?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, staffID);
            ps.setString(2, AccountingPeriod.getCurYearByCurrentDate());
            ps.setInt(3, 10);
            ps.setInt(4, 30);
            ps.setInt(5, 20);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<CoStaffDepartment> getAllLeaveGroupByDepartment(LoginProfile log, String year, String period) throws Exception {

        ResultSet rs = null;
        List<CoStaffDepartment> CVi;
        CVi = new ArrayList();

        String q = "";
        if (log.getAccessLevel() == 1) {
            q = "  AND b.locID = '" + log.getLocID() + "'";
        }

        if (log.getAccessLevel() == 1 && (log.getUserID().equals("P0702") || log.getUserID().equals("P0703"))) {
            q = "";
        }

        try {
            //PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master a, co_staff b where a.staffID = b.staffID and a.year = ? AND a.period = ?  group by departmentID");
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master a, co_staff b where a.staffID = b.staffID " + q + "  group by departmentID");
            //stmt.setString(1, year);
            //stmt.setString(2, period);

            rs = stmt.executeQuery();
            while (rs.next()) {

                CVi.add(StaffDAO.getInfoDepartment(log, rs.getString("b.departmentID")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<LeaveType> getAllLeaveType(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<LeaveType> CVi;
        CVi = new ArrayList();
        LeaveType lt = new LeaveType();

        String q = "";

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_type");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveTypeList(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CompassionateLeave> getAllIhsanLeaveType(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<CompassionateLeave> CVi;
        CVi = new ArrayList();
        CompassionateLeave lt = new CompassionateLeave();

        String q = "";

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from compassionate_leave");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getIhsanLeaveTypeList(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static LeaveType getLeaveType(LoginProfile log, String code) throws Exception {

        ResultSet rs = null;
        LeaveType lt = new LeaveType();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_type where code = ?");
            stmt.setString(1, code);
            rs = stmt.executeQuery();
            while (rs.next()) {
                lt = getLeaveTypeList(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lt;
    }

    public static LeaveType getLeaveTypeList(ResultSet rs) throws SQLException {

        LeaveType lt = new LeaveType();

        lt.setCode(rs.getString("code"));
        lt.setName(rs.getString("name"));
        lt.setDescp(rs.getString("descp"));
        lt.setSymbol(rs.getString("symbol"));
        lt.setId(rs.getInt("id"));
        lt.setActive(rs.getBoolean("active"));

        return lt;

    }

    public static CompassionateLeave getIhsanLeaveTypeList(ResultSet rs) throws SQLException {

        CompassionateLeave lt = new CompassionateLeave();

        lt.setActive(rs.getInt("active"));
        lt.setBil(rs.getString("bil"));
        lt.setId(rs.getInt("id"));
        lt.setLeaveID(rs.getString("leaveID"));
        lt.setRemark(rs.getString("remark"));
        lt.setType(rs.getString("type"));

        return lt;

    }

    public static int getCountLeaveByDepartment(LoginProfile log) throws SQLException {

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_master WHERE staffID=?");
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt("level");
        }

        return i;

    }

    public static int getLeaveCountForDepartmentByStatus(LoginProfile log, String deptID, String status) throws SQLException, Exception {

        int i = 0;
        ResultSet rs = null;

        String q = "";
        if (log.getAccessLevel() == 1) {
            q = "  AND (locID = '" + log.getLocID() + "' OR (departmentID IN ('0003','0004') AND locID <> '9911'))";
        }

        if (log.getAccessLevel() == 1 && (log.getUserID().equals("P0702") || log.getUserID().equals("P0703"))) {
            q = "";
        }

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND b.departmentID = '" + deptID + "' AND a.status = '" + status + "' AND a.staffID <> '" + log.getUserID() + "' AND (datestart >= CURDATE() OR dateend >= CURDATE()) " + q + " ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));
        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }

    public static int getLeaveCountByStatusAndDate(LoginProfile log, String status, String date) throws SQLException, Exception {

        date = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);
        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND a.status = '" + status + "' AND ('" + date + "' BETWEEN datestart AND dateend) ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }

    public static int getLeaveCountForDepartmentByStatusAndDate(LoginProfile log, String deptID, String status, String date) throws SQLException, Exception {

        date = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);
        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND b.departmentID = '" + deptID + "' AND a.status = '" + status + "' AND ('" + date + "' BETWEEN datestart AND dateend) ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));
        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }

    public static int getLeaveCountForStaffByStatus(LoginProfile log, String staffID, String status) throws SQLException, Exception {

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND b.staffID = '" + staffID + "' AND a.status = '" + status + "' AND a.staffID <> '" + log.getUserID() + "' AND (datestart >= CURDATE() OR dateend >= CURDATE()) ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));
        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }

    public static int getLeaveCountForStaffByStatusByLevel(LoginProfile log, String staffID, String status) throws SQLException, Exception {

        int i = 0;
        ResultSet rs = null;

        String q = "";

        if (log.getAccessLevel() == 1) {

        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = " AND type <> 'C02'";

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'C02')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            String ql1 = "";
            String ql2 = "";

            if (LeaveDAO.isSuperViseeExistForLevel3(log, "support")) {
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            } else if (LeaveDAO.isSuperViseeExistForLevel3(log, "approve")) {
                ql2 = "";
            }

            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'C02') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) " + ql1 + ")";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

        }
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID  AND b.staffID = '" + staffID + "' AND a.status = '" + status + "'  AND (datestart >= CURDATE() OR dateend >= CURDATE()) AND  a.staffID <> '" + log.getUserID() + "' " + q);
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));
        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }

    public static void saveRequest(LoginProfile log, LeaveRequest lr) throws Exception {

        try {
            String q = ("INSERT INTO leave_request(datetochange, leaveID, requestto, status, daterequest, timerequest) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, lr.getDatetochange());
            ps.setString(2, lr.getLeaveID());
            ps.setString(3, lr.getRequestto());
            ps.setBoolean(4, lr.getStatus());
            ps.setString(5, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(6, AccountingPeriod.getCurrentTime());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static LeaveRequest getLeaveRequestRS(ResultSet rs) throws SQLException, Exception {
        LeaveRequest cv = new LeaveRequest();

        cv.setDaterequest(rs.getString("daterequest"));
        cv.setDatetochange(rs.getString("datetochange"));
        cv.setId(rs.getInt("id"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setRequestto(rs.getString("requestto"));
        cv.setStatus(rs.getBoolean("status"));
        cv.setTimerequest(rs.getString("timerequest"));

        return cv;
    }

    public static LeaveRequest getLeaveRequest(LoginProfile log, String no) throws SQLException, Exception {
        LeaveRequest c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_request WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveRequestRS(rs);
        }

        return c;
    }

    public static boolean isLeaveRequestExist(LoginProfile log, String leaveID, String request) throws SQLException, Exception {

        boolean b = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_request WHERE  leaveID = ? AND requestto = ?");
        //stmt.setString(1, no);
        stmt.setString(1, leaveID);
        stmt.setString(2, request);
        rs = stmt.executeQuery();
        if (rs.next()) {

            b = true;
        }

        return b;

    }

    private static LeaveReasonTemplate getLeaveReasonTemplateRS(ResultSet rs) throws SQLException, Exception {
        LeaveReasonTemplate cv = new LeaveReasonTemplate();

        cv.setId(rs.getInt("id"));
        cv.setDorder(rs.getInt("dorder"));
        cv.setActive(rs.getBoolean("active"));
        cv.setReason(rs.getString("reason"));

        return cv;
    }

    public static LeaveReasonTemplate getLeaveReasonTemplate(LoginProfile log, String no) throws SQLException, Exception {
        LeaveReasonTemplate c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_reason_template WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveReasonTemplateRS(rs);
        }

        return c;
    }

    public static List<LeaveReasonTemplate> getAllLeaveReasonTemplate(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<LeaveReasonTemplate> CVi;
        CVi = new ArrayList();
        LeaveReasonTemplate c = new LeaveReasonTemplate();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_reason_template WHERE active = ? ORDER BY dorder");
            stmt.setBoolean(1, true);
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--" + String.valueOf(stmt));
            while (rs.next()) {
                CVi.add(getLeaveReasonTemplateRS(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();//3
        }

        return CVi;
    }

    public static List<Leave> getAllLeaveTodaySummary(LoginProfile log, String date) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = " b.locID = '" + log.getLocID() + "' and ";

        if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view
            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "  a.status = 'Approved' order by a.leaveID asc ");

            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "  a.status = 'Approved' order by a.leaveID asc ");

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static String getAllLeaveTodaySummaryString(LoginProfile log, String date) throws Exception, SQLException {

        String st = "";
        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = " b.locID = '" + log.getLocID() + "' ";

        if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view
            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";
        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "   and a.staffID <> '" + log.getUserID() + "'  ORDER BY FIELD(a.status, 'Approved', 'Supported', 'Checked' 'Preparing')");

            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + " and a.staffID <> '" + log.getUserID() + "'    ORDER BY FIELD(a.status, 'Approved', 'Supported', 'Checked' 'Preparing')");

            int i = 0;
            int rowcount = 0;
            boolean b = false;

            while (rs.next()) {
                i++;
                CV.add(LeaveDAO.getLeaveRS(rs));

                //if (i > 2 && !b) {
                //    st += " & " + (rowcount - 2) + " orang memohon cuti hari ini.";
                //    b = true;
                //} else if (!b) {
                //st += GeneralTerm.capitalizeFirstLetter(StaffDAO.getInfo(log, rs.getString("staffID")).getName()) + ", ";
                String color = "";

                if (rs.getString("a.status").equals("Preparing")) {
                    color = "#3599da";
                } else if (rs.getString("a.status").equals("Checked")) {
                    color = "#f39c12";
                } else if (rs.getString("a.status").equals("Supported")) {
                    color = "#ff4080";
                } else if (rs.getString("a.status").equals("Approved")) {
                    color = "#19c5a9";
                }
                st += "<img style=\"border: 3px solid " + color + ";\" class=\"img-circle ml-0 mr-2\" src=\"" + StaffDAO.getInfo(log, rs.getString("a.staffID")).getImageURL() + "\" alt=\"image\" width=\"35\"/>";
                //}

            }

            if (rs.last()) {

                //st += " memohon cuti hari ini. ";
                // rowcount = rs.getRow();
                //rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return st;
    }

    public static int getCountLeaveTodaySummaryByDept(LoginProfile log, String date) throws Exception, SQLException {

        int s = 0;
        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = " b.locID = '" + log.getLocID() + "' ";

        if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view
            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";
        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  a.staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as cnt FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "   and a.staffID <> '" + log.getUserID() + "'  ORDER BY FIELD(a.status, 'Approved', 'Supported', 'Checked' 'Preparing')");

            if (rs.next()) {
                s = rs.getInt("cnt");

            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return s;
    }

    private static SysLogActivity geSysLogActivityRS(ResultSet rs) throws SQLException, Exception {
        SysLogActivity cv = new SysLogActivity();

        cv.setActivity(rs.getString("activity"));
        cv.setDate(rs.getString("date"));
        cv.setDeptID(rs.getString("deptID"));
        cv.setDeptName(rs.getString("deptName"));
        cv.setId(rs.getInt("id"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setLocID(rs.getString("locID"));
        cv.setLocName(rs.getString("locName"));
        cv.setName(rs.getString("name"));
        cv.setSessionid(rs.getString("sessionid"));
        cv.setTime(rs.getString("time"));
        cv.setUserid(rs.getString("userid"));

        return cv;
    }

    public static void saveActivity(LoginProfile log, SysLogActivity ac) throws Exception {

        try {

            String q_attach = ("insert into sys_log_activity(activity, date, deptID,deptName, leaveID, locID, locName, name, sessionid, time, userid) values (?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
            ps_attach.setString(1, ac.getActivity());
            ps_attach.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps_attach.setString(3, ac.getDeptID());
            ps_attach.setString(4, ac.getDeptName());
            ps_attach.setString(5, ac.getLeaveID());
            ps_attach.setString(6, ac.getLocID());
            ps_attach.setString(7, ac.getLocName());
            ps_attach.setString(8, ac.getName());
            ps_attach.setString(9, ac.getSessionid());
            ps_attach.setString(10, AccountingPeriod.getCurrentTime());
            ps_attach.setString(11, ac.getUserid());

            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, String.valueOf(ps_attach));

            ps_attach.executeUpdate();
            ps_attach.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<Leave> getAllPendingLeaveToday(LoginProfile log, String filter) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = " b.locID = '" + log.getLocID() + "' and ";

        /*if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view

        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }*/
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "  a.status <> 'Approved' and a.status <> 'Rejected' order by a.leaveID asc ");

            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN a.datestart AND a.dateend) AND " + restrictByLevel + "  a.status <> 'Approved' and a.status <> 'Rejected' order by a.leaveID asc ");

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static String saveMultiLeaveTemp(LoginProfile log, Leave item, String sessionid) throws Exception {

        String leaveID = LeaveDAO.getLeaveNo(log, "leave_multi_temp", "leaveID");
        String year = AccountingPeriod.getCurYearByDate(item.getDatestart());
        String period = AccountingPeriod.getCurPeriodByDate(item.getDatestart());

        String p = period;

        if (p.length() == 1) {
            p = "0" + p;
        }

        //String leaveID = AutoGenerate.getLeaveID(log, item.getType(), year, p, period);
        String reason = item.getReason();
        try {//sdsd
            String q = ("insert into leave_multi_temp(leaveID,dateapply,dateend,datestart,reason,type, staffID, days, year, period, status,timeapply,sessionID) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, leaveID);
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, item.getDateend());
            ps.setString(4, item.getDatestart());
            ps.setString(5, reason);
            ps.setString(6, item.getType());
            ps.setString(7, log.getUserID());
            ps.setInt(8, item.getDays());
            ps.setString(9, year);
            ps.setString(10, period);
            ps.setString(11, "Preparing");
            ps.setString(12, AccountingPeriod.getCurrentTime());
            ps.setString(13, sessionid);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return leaveID;

    }

    public static int deleteMultiLeave(LoginProfile log, String id) throws Exception {

        int i = 0;
        try {
            String q = ("DELETE FROM leave_multi_temp WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

            i = 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;

    }

    private static LeaveMultiTemp getLeaveMultiTempRS(ResultSet rs) throws SQLException, Exception {
        LeaveMultiTemp cv = new LeaveMultiTemp();

        cv.setDateapply(rs.getString("dateapply"));
        cv.setDateend(rs.getString("dateend"));
        cv.setDatestart(rs.getString("datestart"));
        cv.setDays(rs.getInt("days"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setPeriod(rs.getString("period"));
        cv.setReason(rs.getString("reason"));
        cv.setSessionID(rs.getString("sessionID"));
        cv.setStaffID(rs.getString("staffID"));
        cv.setStatus(rs.getString("status"));
        cv.setTimeapply(rs.getString("timeapply"));
        cv.setType(rs.getString("type"));
        cv.setYear(rs.getString("year"));

        return cv;
    }

    public static List<LeaveMultiTemp> getAllLeaveMultiTemp(LoginProfile log, String sessionID) throws Exception {

        ResultSet rs = null;
        List<LeaveMultiTemp> CVi;
        CVi = new ArrayList();
        LeaveMultiTemp c = new LeaveMultiTemp();

        String q = "";

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_multi_temp WHERE sessionID = ?");
            stmt.setString(1, sessionID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveMultiTempRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static void saveMultiLeave(LoginProfile log, String sessionID) throws Exception {

        List<LeaveMultiTemp> list = (List<LeaveMultiTemp>) getAllLeaveMultiTemp(log, sessionID);

        for (LeaveMultiTemp j : list) {

            Leave cv = new Leave();

            cv.setDateapply(j.getDateapply());
            cv.setDateend(j.getDateend());
            cv.setDatestart(j.getDatestart());
            cv.setDays(j.getDays());
            cv.setReason(j.getReason());
            cv.setStaffID(j.getStaffID());
            cv.setType(j.getType());
            cv.setStatus(j.getStatus());
            cv.setYear(j.getYear());
            cv.setPeriod(j.getPeriod());
            cv.setTimeapply(j.getTimeapply());

            String leaveID = LeaveDAO.saveLeave(log, cv, sessionID);

            LeaveDAO.updateLeaveMultiTemp(log, leaveID, j.getLeaveID());

        }

    }

    public static void updateLeaveMultiTemp(LoginProfile log, String leaveID, String leaveIDtemp) throws Exception {

        try {

            String q = ("UPDATE leave_multi_temp SET leaveID = ?, final = ? WHERE leaveID = ?");

            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, leaveID);
            ps.setBoolean(2, true);
            ps.setString(3, leaveIDtemp);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static boolean isMultiLeave(LoginProfile log, String leaveID) throws SQLException {

        boolean b = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_multi_temp WHERE leaveID=?");
        stmt.setString(1, leaveID);
        rs = stmt.executeQuery();
        if (rs.next()) {
            b = true;
        }

        return b;

    }

    public static LeaveMultiTemp getLeaveMultiTemp(LoginProfile log, String no) throws SQLException, Exception {
        LeaveMultiTemp c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_multi_temp WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveMultiTempRS(rs);
        }

        return c;
    }

    public static String getLeaveNo(LoginProfile log, String table, String voucherno) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(lpad(max(" + voucherno + ")+1,5,'0')), '00001') as new from " + table + " WHERE  final = ?");
        stmt.setBoolean(1, false);
        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }

//    public static String getLastSync(LoginProfile log) throws SQLException, Exception {
//        String lastsync = "";
//        ResultSet rs = null;
//        try {
//
//            PreparedStatement stmt = log.getCon().prepareStatement("SELECT CONCAT(datesync,' ',timesync) as sync FROM ar_agree_lastsync WHERE id = (select max(id) from ar_agree_lastsync)");
//            rs = stmt.executeQuery();
//            if (rs.next()) {
//                lastsync = rs.getString("sync");
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return lastsync;
//    }
//    public static JSONArray getMonthActivity(LoginProfile log) throws Exception {
//
//        JSONArray array = new JSONArray();
//        
//
//        String year = AccountingPeriod.getCurrentTimeStamp().substring(0,4);
//        String period = AccountingPeriod.getCurrentTimeStamp().substring(5,7);
//
//        for (int i = 1; i <= AccountingPeriod.getDayPerMonth(Integer.parseInt(period), Integer.parseInt(year)); i++) {
//            JSONObject jo = new JSONObject();
//            String j = String.valueOf(i);
//
//            if (j.length() == 1) {
//                j = "0" + j;
//            }
//            String dt = year + "-" + period + "-" + j;
//            jo.put("year", year);
//            jo.put("month", period);
//            jo.put("total", getUserLoginPerday(log, dt));
//            jo.put("totaltrans", getTransactionPerday(log, dt));
//            array.add(jo);
//            jo = null;
//        }
//
//        return array;
//
//    }
}
