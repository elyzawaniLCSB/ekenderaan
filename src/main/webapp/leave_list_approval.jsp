<%-- 
    Document   : leave_list_approval
    Created on : Jan 21, 2020, 9:12:03 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.model.CoStaffDepartment"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%-- 
    Document   : leave_list
    Created on : Jan 20, 2020, 3:17:13 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String tab = request.getParameter("tab");

    String tab1 = "";
    String tab2 = "";

    String show1 = "";
    String show2 = "";

    if (tab.equals("2")) {
        tab1 = "";
        tab2 = "active";
        show1 = "";
        show2 = "show";
    } else if (tab.equals("1")) {
        tab1 = "active";
        tab2 = "";
        show1 = "show";
        show2 = "";
    }

    /*if(log == null) {
    //chain.doFilter(request, response);
    out.println("sds");
} else {
    
}*/
    String url = "leave_list_approval_load_filter";

    if (log.getAccessLevel() == 1 || log.getAccessLevel() == 4) {
        url = "leave_list_approval_load_all";
    }

%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $.ajax({
            url: "<%=url%>.jsp?tab=<%=tab%>",
                        success: function (result) {
                            $('.tab-content').empty().html(result).hide().fadeIn(300);
                        }
                    });

                    $('#refresh-page').click(function (e) {
                        e.preventDefault();
                        var tab = $('#current-tab').val();
                        $.ajax({
                            url: "<%=url%>.jsp?tab="+tab,
                                            success: function (result) {
                                                $('.tab-content').empty().html(result).hide().fadeIn(300);
                                            }
                                        });

                                        return false;
                                    });
                                    
                                    $('.currenttab').click(function (e) {
                                        e.preventDefault();
                                        
                                        var tab = $(this).attr('id');
                                        $('#current-tab').val(tab);

                                        return false;
                                    });

                                    $('#gotochecklist').click(function (e) {
                                        e.preventDefault();
                                        $(location).attr('href', 'leave_list_approval.jsp');

                                        return false;
                                    });

                                    $('.leave-view').click(function (e) {

                                        var id = $(this).attr('id');

                                        $(location).attr('href', 'PathController?process=viewleavedetail&id=' + id);

                                        return false;
                                    });

                                    $('#datatable tbody').on('click', 'tr', function (e) {
                                        e.preventDefault();

                                        var id = $(this).attr('id');
                                        $.ajax({
                                            async: false,
                                            url: "PathController?process=addmodal&carID=" + id,
                                            success: function (result) {
                                                $('#modalhere').empty().html(result).hide().fadeIn(300);
                                            }
                                        });


                                        $('#myModal').modal('toggle')
                                        return false;
                                    });

                                    $('#modalhere').on('click', '#editCarButton', function (e) {
                                        e.preventDefault();

                                        var id = $(this).attr('title');
                                        $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

                                        return false;
                                    });

                                    $('#modalhere').on('click', '#deleteCarButton', function (e) {
                                        e.preventDefault();

                                        var id = $(this).attr('title');

                                        swal({
                                            title: "Are you sure?",
                                            text: "You will not be able to recover this!",
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonClass: 'btn-warning',
                                            confirmButtonText: 'Yes, delete it!',
                                            closeOnConfirm: false,
                                        }, function () {

                                            $.ajax({
                                                async: false,
                                                url: "PathController?process=deletecar&carID=" + id,
                                                success: function (result) {
                                                    //swal("Deleted!", "Car has been deleted.", "success");
                                                    $(location).attr('href', 'conf_car.jsp');
                                                }
                                            });

                                        });


                                        return false;
                                    });

                                    $('.tab-content').on('click', '.view-modal', function (e) {
                                        e.preventDefault();

                                        var leaveid = $(this).attr('id');
                                        var id = $(this).attr('href');
                                        $.ajax({
                                            async: false,
                                            url: "PathController?process=viewleavemodal&leaveID=" + leaveid + "&id=" + id,
                                            success: function (result) {
                                                $('#modalhere').empty().html(result).hide().fadeIn(300);
                                            }
                                        });


                                        $('#myModal').modal('toggle')
                                        return false;
                                    });

                                    //$('.update-modal').click(function (e) {
                                    $('.tab-content').on('click', '.update-modal', function (e) {
                                        e.preventDefault();

                                        var leaveid = $(this).attr('id');
                                        var id = $(this).attr('href');
                                        $.ajax({
                                            async: false,
                                            url: "PathController?process=updateleavemodal&leaveID=" + leaveid + "&id=" + id,
                                            success: function (result) {
                                                $('#modalhere').empty().html(result).hide().fadeIn(300);
                                            }
                                        });


                                        $('#myModal').modal('toggle')
                                        return false;
                                    });


                                    $('.tab-content').on('click', '.edit-leave', function (e) {
                                        e.preventDefault();

                                        var id = $(this).attr('id');
                                        $(location).attr('href', 'leave_detail_edit.jsp?id=' + id);

                                        return false;
                                    });


                                    $('.tab-content').on('click', '.delete-leave', function (e) {

                                        var id = $(this).attr('id');
                                        var tab = $(this).attr('title');
                                        swal({
                                            title: "Anda pasti untuk padam?",
                                            text: "Anda tidak akan dapat mengembalikannya semula!",
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonClass: 'btn-warning',
                                            confirmButtonText: 'Ya, padam!',
                                            closeOnConfirm: false,
                                        }, function () {

                                            $.ajax({
                                                async: false,
                                                url: "PathController?process=deleteleave&id=" + id,
                                                success: function (result) {
                                                    //swal("Deleted!", "Car has been deleted.", "success");
                                                    $(location).attr('href', 'leave_list_approval.jsp?tab=' + tab);
                                                }
                                            });

                                        });

                                        //} 

                                        e.stopPropagation();
                                        return false;
                                    });


                                });

</script>
<body>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $(this).scrollTop(0);
        });
    </script>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">

            <!-- START PAGE CONTENT-->

            <div class="page-content fade-in-up">
                <!--<div class="alert alert-danger alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>

                    <div class="d-flex align-items-center justify-content-between">
                        <div><strong>Perhatian</strong><br>Ada beberapa permohonan yang perlu diambil tindakan.</div>
                <!--<div>
                    <button class="btn btn-sm btn-danger btn-rounded" id="gotochecklist"><%//= LeaveDAO.getPreparedLeave(log)%> Permohonan</button>
                </div>
            </div>
        </div>-->
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Senarai Permohonan</div>
                        <button class="btn btn-primary btn-rounded btn-air my-3" id="refresh-page">
                            <i class="fa fa-refresh" aria-hidden="true"></i><span class="button-text-approve"></span> 
                        </button>
                        

                    </div>
                    <div class="ibox-body">
                        <input type="hidden" id="current-tab" value="1">
                        <ul class="nav nav-tabs tabs-line tabs-line-2x nav-fill">
                            <li class="nav-item">
                                <a class="nav-link <%= tab1%>" href="#tab-11-1" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i><span class="button-text-approve"> Cuti Anda</span></a>
                            </li>
                            <%
                                if (log.getAccessLevel() > 0) {

                                    int countSupervisor = LeaveDAO.getLeaveCountForSupervisee(log);

                            %>
                            <li class="nav-item">
                                <a class="nav-link <%= tab2%>" href="#tab-11-2" data-toggle="tab"><i class="fa fa-users" aria-hidden="true"></i><span class="button-text-approve"> Cuti Seliaan</span> 

                                    <%
                                        if (countSupervisor > 0) {
                                    %>
                                    <span class="badge badge-blue badge-circle"><%= countSupervisor%></span>
                                    <%
                                        }
                                    %>
                                </a>
                            </li>
                            <%
                                }
                            %>
                        </ul>
                        <div class="tab-content">

                            <div class="row d-flex justify-content-center mt-5"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>

                        </div>
                    </div>
                </div>


            </div>

            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>

        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>







