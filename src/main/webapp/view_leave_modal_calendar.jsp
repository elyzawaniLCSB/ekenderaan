<%-- 
    Document   : view_leave_modal_calendar
    Created on : Jan 24, 2020, 11:55:57 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.eleave.model.Book"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%@page import="com.lcsb.eleave.model.Driver"%>
<%@page import="com.lcsb.eleave.dao.DriverDAO"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //Car c = (Car) CarDAO.getCarInfo(log, request.getParameter("carID"));
    //Driver d = (Driver) DriverDAO.getDriverInfo(log, c.getDriverID());
    Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));
    CoStaff st = (CoStaff) StaffDAO.getInfo(log, l.getStaffID());
    String destID = request.getParameter("id");

    String yearApply = AccountingPeriod.getCurYearByDate(AccountingPeriod.getCurrentTimeStamp());
    String monthApply = AccountingPeriod.getCurPeriodByDate(AccountingPeriod.getCurrentTimeStamp());

    int totLeaveUse = LeaveDAO.getTotalLeaveOfTheYear(log, st.getStaffid(), yearApply);
    int totLeaveUseMonth = LeaveDAO.getTotalLeaveOfTheMonth(log, st.getStaffid(), yearApply, monthApply);
    int totLeaveYear = LeaveDAO.getEligibleLeaveOfTheYear(log, st.getStaffid(), yearApply);
    int percentYear = LeaveDAO.getPercentLeaveUseYear(log, st.getStaffid(), yearApply);
    int percentMonth = LeaveDAO.getPercentLeaveUseMonth(log, st.getStaffid(), yearApply, monthApply, l.getDatestart());

    String buttonValid = "";
    String titleValid = "";

    if (log.getAccessLevel() == 2) {

        buttonValid = "Sokong";
        titleValid = "Supported";
    }else if (log.getAccessLevel() == 1) {

        buttonValid = "Semak";
        titleValid = "Checked";
    }else if (log.getAccessLevel() == 3) {

        buttonValid = "Lulus";
        titleValid = "Approved";
    }else if (log.getAccessLevel() == 4) {

        buttonValid = "Sah";
        titleValid = "Verified";
    }

%>

<div class="modal fade" id="event-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="savecardestination">
            <input type="hidden" name="destID" id="destID" value="<%= destID%>">
            <input type="hidden" name="bookID" id="bookID" value="<%= l.getLeaveID()%>">
            <div class="modal-header p-4">
                <h5 class="modal-title">Permohonan Cuti</h5><span class="badge badge-<%= LeaveDAO.getBadgeColor(l.getStatus())%>"><%= l.getStatus()%></span>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="ibox">
                    <div class="ibox-body">
                        <!--<h5 class="font-strong mb-4"><%= st.getName()%></h5>-->
                        <div class="flexbox-b mb-4">
                            <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="40" />
                            <div class="flex-1">

                                <div class="font-strong font-14">&nbsp;&nbsp;<%= st.getName()%><small class="text-muted float-right"><%= l.getDateapply()%></small></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getPosition()%></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getLocation()%></div><br>


                            </div>

                        </div>

                        <div class="row">

                            <div class="col-6 flexbox-b"><i class="la la-calendar-check-o font-26 text-light mr-3"></i>
                                <div class="flex-1">
                                    <div>
                                        <span class="text-light mr-3">Tahunan</span><%= totLeaveUse%> / <%= totLeaveYear%></div>
                                    <div class="progress mt-1">
                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentYear)%>" role="progressbar" style="width:<%= percentYear%>%; height:5px;" aria-valuenow="<%= totLeaveUse%>" aria-valuemin="0" aria-valuemax="<%= totLeaveYear%>"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 flexbox-b"><i class="la la-hourglass-1 font-26 text-light mr-3"></i>
                                <div class="flex-1">
                                    <div>
                                        <span class="text-light mr-3">Bulanan</span><%= totLeaveUseMonth%> / <%= LeaveDAO.getEligibleLeaveForTheMonth(log, st.getStaffid(), yearApply, l.getDatestart())%></div>
                                    <div class="progress mt-1">
                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentMonth)%>" role="progressbar" style="width:<%= percentMonth%>%; height:5px;" aria-valuenow="<%= percentMonth%>" aria-valuemin="0" aria-valuemax="<%= LeaveDAO.getEligibleLeaveForTheMonth(log, st.getStaffid(), yearApply, l.getDatestart())%>"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-body">
                        <!--<h5 class="font-strong mb-4"><%= st.getName()%></h5>-->
                        <div class="cd-timeline cd-timeline-dark  timeline-2">

                            <div class="cd-timeline-block">
                                <div class="cd-timeline-icon bg-primary text-white"><i class="fa fa-briefcase"></i></div>
                                <div class="cd-timeline-content">
                                    <h4><%= l.getType()%></h4>
                                    <div class="col-lg-6 col-sm-12">
                                        <p><%= l.getReason()%></p>
                                        <small>Dari : <strong><%= AccountingPeriod.fullDateMonth(l.getDatestart())%></strong></small><br>
                                        <small>Hingga : <strong><%= AccountingPeriod.fullDateMonth(l.getDateend())%></strong></small><br><br>




                                    </div>
                                    <span class="cd-date"><%= l.getDays()%> Hari</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                    <%
                        if(!LeaveDAO.isButtonDisable(log, l.getLeaveID())){
                        %>
                    <button class="btn btn-primary btn-rounded mr-1 actionto" title="<%= titleValid %>" id="<%=l.getLeaveID()%>"><%= buttonValid %></button>
                    <button class="btn btn-warning btn-rounded mr-3 actionto" title="Rejected" id="<%=l.getLeaveID()%>">Tolak</button>
                    
                     <%
                         }
                        %>
                    <!--<label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-edit font-20"></i>
                        <input type="file">
                    </label>
                    <label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-image font-20"></i>
                        <input type="file">
                    </label>-->


                </div>
            </div>
        </form>
    </div>
</div>