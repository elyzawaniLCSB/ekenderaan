<%-- 
    Document   : book_car_edit
    Created on : Oct 10, 2019, 11:14:53 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.Book"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Book b = (Book) BookingDAO.getBookingInfo(log, request.getParameter("id"));

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $("#savebutton").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();

            var a = $("#saveform :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=savebook",
                success: function (result) {
                    $(location).attr('href', 'book_list.jsp');
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


        $("#back").click(function () {
            parent.history.back();
            return false;
        });

        $('#date_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('.clockpicker').clockpicker();

    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="ibox">
                    <div class="ibox-body">
                        <h5 class="font-strong mb-5">BOOK A CAR</h5>
                        <div class="row">
                            <!-- <div class="col-lg-4">
                                  <div>
                                    <img src="./assets/img/products/27.jpg" alt="image" />
                                 </div>
                                 <div class="flexbox-b mt-4">
                                     <div class="mr-2">
                                         <img src="./assets/img/products/28.jpg" alt="image" />
                                     </div>
                                     <div class="mr-2">
                                         <img src="./assets/img/products/29.jpg" alt="image" />
                                     </div>
                                     <div class="mr-2">
                                         <img src="./assets/img/products/30.jpg" alt="image" />
                                     </div>
                                     <div class="file-input-plus file-input"><i class="la la-plus-circle"></i>
                                         <input type="file">
                                     </div>
                                 </div>
                             </div>-->
                            <div class="col-lg-8">
                                <form action="javascript:;" id="saveform">
                                    <div class="form-group" id="date_5">
                                        <label class="font-normal">From and To Date</label>
                                        <div class="input-daterange input-group" id="datepicker">
                                            <input class="input-sm form-control" type="text" name="startdate" value="<%= DateAndTime.getCurrentTimeStamp()%>">
                                            <span class="input-group-addon pl-2 pr-2">to</span>
                                            <input class="input-sm form-control" type="text" name="enddate" value="<%= DateAndTime.getCurrentTimeStamp()%>">
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6 form-group mb-4">
                                            <div class="form-group mb-4">
                                                <label>Start Time</label>
                                                <div class="input-group clockpicker" data-autoclose="true">
                                                    <input class="form-control" type="text" value="00:00" name="starttime">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-clock-o"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 form-group mb-4">
                                            <div class="form-group mb-4">
                                                <label>End Time</label>
                                                <div class="input-group clockpicker" data-autoclose="true">
                                                    <input class="form-control" type="text" value="00:00" name="endtime">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-clock-o"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6 form-group mb-4">
                                            <label>Destination</label>
                                            <input class="form-control form-control-solid" type="text" name="destination">
                                        </div>
                                    </div>


                                    <div class="form-group mb-4">
                                        <label>Reason</label>
                                        <textarea class="form-control form-control-solid" rows="4"  name="reason"></textarea>
                                    </div>
                                    <!--<div class="form-group mb-4">
                                        <label class="ui-switch switch-icon mr-3 mb-0">
                                            <input type="checkbox" checked="" name="status">
                                            <span></span>
                                        </label>Available</div>-->
                                    <div class="text-right">
                                        <button class="btn btn-primary btn-air mr-2" id="savebutton">Save</button>
                                        <button class="btn btn-secondary" id="back">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
</body>
</html>
