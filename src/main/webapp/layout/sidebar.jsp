<%-- 
    Document   : sidebar
    Created on : Sep 29, 2019, 10:06:03 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String sessionid = request.getParameter("sessionid");

%>
<!DOCTYPE html>
<nav class="page-sidebar">
    <ul class="side-menu metismenu scroller">
        <li class="active">
            <a href="Login"><i class="sidebar-item-icon ti-home"></i>
                <span class="nav-label">Paparan Utama</span></a>

        </li>
        
        <%
            if(log.getAccessLevel()==1){
            %>
        
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-paint-roller"></i>
                <span class="nav-label">Konfigurasi</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="staff_list.jsp">Maklumat Pekerja</a>
                </li>
                <li>
                    <a href="leave_record.jsp">Kemas Kini Rekod</a>
                </li>
                
            </ul>
        </li>
        
        <%
            }
            %>

        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-anchor"></i>
                <span class="nav-label">Cuti</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="leave_list_approval.jsp?tab=1">Permohonan Cuti</a>
                </li><li>
                    <a href="leave_calendar.jsp?filter=<%= StaffDAO.getInfo(log, log.getUserID()).getDepartmentID() %>">Kalendar Cuti</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-anchor"></i>
                <span class="nav-label">Laporan</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                
                <li>
                    <a href="report_staff_list.jsp?filter=none">Senarai Anggota</a>
                </li>
                <li>
                    <a href="report_leave_balance.jsp">Maklumat Cuti</a>
                </li>
                <li>
                    <a href="report_leave_application.jsp">Permohonan Cuti</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>