<%-- 
    Document   : leave_calendar
    Created on : Jan 24, 2020, 9:30:36 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaffDepartment"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.BookingDestination"%>
<%@page import="com.lcsb.eleave.model.BookingMaster"%>
<%@page import="com.lcsb.eleave.dao.DateAndTime"%>
<%@page import="com.lcsb.eleave.model.Book"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");


%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#addnew').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'conf_car_add.jsp');

            return false;
        });
        
        $('#change-view').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            $(location).attr('href', 'leave_calendar.jsp?filter='+id);

            return false;
        });

        $('#viewcarmodal').click(function (e) {

            e.preventDefault();
            $.ajax({
                async: false,
                url: "PathController?process=addmodal",
                success: function (result) {
                    //alert(result);
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=viewbooking&id=" + id,
                success: function (result) {
                    $(location).attr('href', 'book_car_view.jsp?id=' + id);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('.deletebutton').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletebooking&bookID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'book_list.jsp');
                    }
                });

            });


            return false;
        });



        /* var calendarEl = document.getElementById('calendar');
         
         var calendar = new FullCalendar.Calendar(calendarEl, {
         plugins: [ 'interaction', 'dayGrid' ],
         defaultDate: '2019-08-12',
         editable: true,
         eventLimit: true, // allow "more" link when too many events
         events: [
         {
         title: 'All Day Event',
         start: '2019-08-01'
         },
         {
         title: 'Long Event',
         start: '2019-08-07',
         end: '2019-08-10'
         },
         {
         groupId: 999,
         title: 'Repeating Event',
         start: '2019-08-09T16:00:00'
         },
         {
         groupId: 999,
         title: 'Repeating Event',
         start: '2019-08-16T16:00:00'
         },
         {
         title: 'Conference',
         start: '2019-08-11',
         end: '2019-08-13'
         },
         {
         title: 'Meeting',
         start: '2019-08-12T10:30:00',
         end: '2019-08-12T12:30:00'
         },
         {
         title: 'Lunch',
         start: '2019-08-12T12:00:00'
         },
         {
         title: 'Meeting',
         start: '2019-08-12T14:30:00'
         },
         {
         title: 'Happy Hour',
         start: '2019-08-12T17:30:00'
         },
         {
         title: 'Dinner',
         start: '2019-08-12T20:00:00'
         },
         {
         title: 'Birthday Party',
         start: '2019-08-13T07:00:00'
         },
         {
         title: 'Click for Google',
         url: 'http://google.com/',
         start: '2019-08-28'
         }
         ]
         });
         
         calendar.render();*/


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="flexbox-b mb-5">
                            <span class="mr-4 static-badge badge-blue"><i class="la la-calendar-check-o font-36"></i></span>
                            <div>
                                <h5 class="font-strong">KALENDAR CUTI</h5>
                                <div class="text-light"><span class="badge badge-danger badge-circle"><%= LeaveDAO.getCountConfirmedLeave(log)%></span> cuti dijumpai pada bulan ini.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="flexbox-b pull-right">
                            <div class="form-group mb-4">
                                <label>Papar mengikut</label>
                                <select class="form-control" id="change-view">
                                    <option value="none">Semua</option>
                                    <%
                                        List<CoStaffDepartment> listAlldept = (List<CoStaffDepartment>) StaffDAO.getAllDepartmentWithoutGMMD(log);
                                        int l = 0;

                                        for (CoStaffDepartment k : listAlldept) {
                                            String selected = "";

                                            if (k.getId().equals(request.getParameter("filter"))) {
                                                selected = "selected";
                                            }
                                    %>
                                    <option value="<%= k.getId()%>" <%= selected%>><%= k.getDescp()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!--<div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">DRAGGABLE EVENTS</div>
                            </div>
                            <div class="ibox-body p-3">
                                <div id="external-events">
                                    <div class="ex-event" data-class="fc-event-primary"><i class="badge-point badge-primary mr-3"></i>Business</div>
                                    <div class="ex-event" data-class="fc-event-warning"><i class="badge-point badge-warning mr-3"></i>Reporting</div>
                                    <div class="ex-event" data-class="fc-event-success"><i class="badge-point badge-success mr-3"></i>Meeting</div>
                                    <div class="ex-event" data-class="fc-event-danger"><i class="badge-point badge-danger mr-3"></i>Important</div>
                                    <div class="ex-event" data-class="fc-event-info"><i class="badge-point badge-info mr-3"></i>Personal</div>
                                    <p class="ml-2 mt-4">
                                        <label class="checkbox checkbox-primary">
                                            <input id="drop-remove" type="checkbox">
                                            <span class="input-span"></span>remove after drop</label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-md-12">
                        <div class="ibox">

                            <div class="ibox-body">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- New Event Dialog-->
                <div class="modal fade" id="new-event-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <form class="modal-content form-horizontal" id="newEventForm" action="javascript:;">
                            <!--<div class="modal-header p-4">
                                <h5 class="modal-title">NEW EVENT</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>-->
                            <div class="modal-body p-4">
                                <div class="form-group mb-4">
                                    <label class="text-muted mb-3">Category</label>
                                    <div>
                                        <label class="radio radio-outline-primary radio-inline check-single" data-toggle="tooltip" data-original-title="General">
                                            <input type="radio" name="category" checked value="fc-event-primary">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-warning radio-inline check-single" data-toggle="tooltip" data-original-title="Payment">
                                            <input type="radio" name="category" value="fc-event-warning">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-success radio-inline check-single" data-toggle="tooltip" data-original-title="Technical">
                                            <input type="radio" name="category" value="fc-event-success">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-danger radio-inline check-single" data-toggle="tooltip" data-original-title="Registration">
                                            <input type="radio" name="category" value="fc-event-danger">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-info radio-inline check-single" data-toggle="tooltip" data-original-title="Security">
                                            <input type="radio" name="category" value="fc-event-info">
                                            <span class="input-span"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <input class="form-control form-control-line" id="new-event-title" type="text" name="title" placeholder="Title">
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">Start:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="new-event-start" type="text" name="start" value="">
                                        </div>
                                    </div>
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">End:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="new-event-end" type="text" name="end" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4 pt-3">
                                    <label class="ui-switch switch-icon mr-3 mb-0">
                                        <input id="new-event-allDay" type="checkbox" checked>
                                        <span></span>
                                    </label>All Day</div>
                            </div>
                            <div class="modal-footer justify-content-start bg-primary-50">
                                <button class="btn btn-primary btn-rounded" id="addEventButton" type="submit">Add event</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End New Event Dialog-->
                <!-- Event Detail Dialog-->
                <div id="modalhere">

                </div>
                <!-- End Event Detail Dialog-->
            </div>
            <!-- END PAGE CONTENT-->
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>

<script>
    $(function () {

        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });

        $('.view-modal').click(function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "PathController?process=viewleavemodal&leaveID=" + leaveid + "&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });



        var CalendarApp = function () {
            this.$body = $("body")
            this.$calendar = $('#calendar'),
                    this.$event = ('#external-events div.ex-event'),
                    this.$categoryForm = $('#add-new-event form'),
                    this.$extEvents = $('#calendar-events'),
                    this.$modal = $('#new-event-modal'),
                    this.$eventModal = $('#event-modal'),
                    this.$saveCategoryBtn = $('.save-category'),
                    this.$calendarObj = null
        };

        // handler for clicking on an empty calendar field
        CalendarApp.prototype.onSelect = function (start, end, allDay) {

            var $this = this;
            $this.$modal.modal();
            //// fill in the values
            this.$modal.find('#new-event-start').val($.fullCalendar.formatDate(start, "YYYY-MM-DD HH:mm:ss"));
            this.$modal.find('#new-event-end').val($.fullCalendar.formatDate(end, "YYYY-MM-DD HH:mm:ss"));

            $this.$calendarObj.fullCalendar('unselect');
        },
                // Update event
                CalendarApp.prototype.updateEvent = function (calEvent, revertFunc) {

                    console.log(calEvent['id']);
                    var $this = this;

                    var leaveid = calEvent['id']
                    //var id = $(this).attr('href');
                    $.ajax({
                        async: true,
                        url: "PathController?process=viewleavemodalcalendar&leaveID=" + leaveid,
                        success: function (result) {
                            $('#modalhere').empty().html(result).hide().fadeIn(300);
                            $this.$eventModal.modal();
                        }
                    });


                    // The same can be done for eventDrop and eventResize

                    // fill in the values
                    /*$this.$eventModal.find('#event-title').val(calEvent.title);
                     $this.$eventModal.find('#event-start').val($.fullCalendar.formatDate(calEvent.start, "YYYY-MM-DD HH:mm:ss"));
                     $this.$eventModal.find('#event-end').val(calEvent.end ? $.fullCalendar.formatDate(calEvent.end, "YYYY-MM-DD HH:mm:ss") : '');
                     if (calEvent.className.length)
                     $this.$eventModal.find('input[name="category"][value="' + calEvent.className + '"]').prop("checked", true);
                     else
                     $this.$eventModal.find('#event-color :first-child').prop("selected", true);
                     $this.$eventModal.find('#event-allDay').prop("checked", calEvent.allDay);*/

                    // set the handler to delete the event

                    // set the handler to update the event

                }

        // Called when a valid jQuery UI draggable has been dropped onto the calendar.
        CalendarApp.prototype.onDrop = function (eventObj, date) {
            var $this = this;
            // retrieve the dropped element's stored Event Object
            var originalEventObject = eventObj.data('eventObject');
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            //var $categoryClass = eventObj.attr('data-class');
            // assign it the date that was reported
            copiedEventObject.start = $.fullCalendar.formatDate(date, "YYYY-MM-DD HH:mm:ss");
            // execute the query to save the event in the database and get its id
            // For example

            /*$.post(APP.ASSETS_PATH+'demo/server/add-event.php', {event: copiedEventObject}).then(function(id){
             copiedEventObject.id = id;
             $this.$calendarObj.fullCalendar('renderEvent', copiedEventObject, true); // stick? = true
             }).fail(function(){
             alert('error');
             });*/

            // Create event
            copiedEventObject.id = Math.random();
            ;
            $this.$calendarObj.fullCalendar('renderEvent', copiedEventObject, true); // stick? = true

            toastr.success('event successfully created');

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                eventObj.remove();
            }
        },
                // initialize the external events
                CalendarApp.prototype.enableDrag = function () {
                    $(this.$event).each(function () {
                        // store data so the calendar knows to render an event upon drop
                        $(this).data('eventObject', {
                            title: $.trim($(this).text()), // use the element's text as the event title
                            stick: true, // maintain when user navigates (see docs on the renderEvent method)
                            className: $(this).attr('data-class')
                        });

                        // make the event draggable using jQuery UI
                        $(this).draggable({
                            zIndex: 999,
                            revert: true, // will cause the event to go back to its
                            revertDuration: 0  //  original position after the drag
                        });
                    });
                }

        /* Initializing */
        CalendarApp.prototype.init = function () {
            this.enableDrag();
            var $this = this;
            $this.$calendarObj = $this.$calendar.fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                events: 'ProcessController?process=getLeaveForCalendar&filter=<%= request.getParameter("filter")%>',
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar
                navLinks: true, // can click day/week names to navigate views
                eventLimit: true, // allow "more" link when too many events
                selectable: true,
                drop: function (date) {
                    $this.onDrop($(this), date);
                },
                select: function (start, end, allDay) {
                    $this.onSelect(start, end, allDay);
                },
                eventClick: function (calEvent, jsEvent, view) {
                    $this.updateEvent(calEvent);
                },
                // The same can be done for these events
                eventResize: function (event, delta, revertFunc) {
                    $this.updateEvent(event, revertFunc);
                },
                eventDrop: function (event, delta, revertFunc) {
                    $this.updateEvent(event, revertFunc);
                },

                eventRender: function (event, element, view) {
                    event.allDay = event.allDay == true ? true : false;
                    if (event.imageurl) {
                        element.find("div.fc-content").prepend("<img src='" + event.imageurl + "' width='16' height='16'>");
                    }
                },
            });
        }


        // initializing CalendarApp

        $.CalendarApp = new CalendarApp;
        $.CalendarApp.init();

        // initialize datetimepicker
        $('.datepicker').datetimepicker({
            format: 'yyyy-mm-dd hh:ii:ss',
            useCurrent: false
        });

        // Validate Forms
        $('#newEventForm').validate({
            errorClass: "help-block",
            rules: {
                title: {required: true},
                start: {required: true},
            },
            highlight: function (e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function (e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });

        $('#eventForm').validate({
            errorClass: "help-block",
            rules: {
                title: {required: true},
                start: {required: true},
            },
            highlight: function (e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function (e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });


        // Handler to add new event 
        $('#newEventForm').submit(function () {
            if ($(this).valid()) {
                var CalendarApp = $.CalendarApp;
                var newEvent = {
                    id: Math.random(),
                    title: $('#new-event-title').val(),
                    start: CalendarApp.$modal.find('#new-event-start').val(),
                    end: CalendarApp.$modal.find('#new-event-end').val(),
                    allDay: CalendarApp.$modal.find('#new-event-allDay').prop('checked'),
                    className: CalendarApp.$modal.find('input[name="category"]:checked').val(),
                }

                // execute the query to save the event in the database and get its id
                // for example

                /*$.post('add-event.php', {event: newEvent}).then(function(id){
                 newEvent.id = id;
                 CalendarApp.$calendarObj.fullCalendar('renderEvent', newEvent, true);
                 }).fail(function(){
                 alert('error');
                 });*/

                // Create Event
                CalendarApp.$calendarObj.fullCalendar('renderEvent', newEvent, true); // stick? = true

                toastr.success('event successfully created');
                CalendarApp.$modal.modal('hide');
            }
        });



    });
</script>
</html>
