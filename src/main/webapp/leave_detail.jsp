<%-- 
    Document   : leave_detail
    Created on : Jan 20, 2020, 3:40:55 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%-- 
    Document   : leave_list
    Created on : Jan 20, 2020, 3:17:13 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Leave v = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("id"));

%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#addnew').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'conf_car_add.jsp');

            return false;
        });

        $('.edit-leave').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $(location).attr('href', 'leave_detail_edit.jsp?id=' + id);

            return false;
        });


        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });

        $('.leave-view').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavedetail&id=' + id);

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('#modalhere').on('click', '#deleteCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletecar&carID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'conf_car.jsp');
                    }
                });

            });


            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">

            <!-- START PAGE CONTENT-->

            <div class="page-content fade-in-up">
                <div class="col-lg-8">
                    <div class="ibox ibox-fullheight">
                        <div class="ibox-head">
                            <div class="ibox-title">
                                Detail Permohonan Cuti</div>
                            <div class="ibox-tools">
                                <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-more-alt"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item edit-leave" id="<%//= v.getLeaveID()%>"> <i class="ti-pencil-alt"></i>Edit</a>
                                    <a class="dropdown-item delete-leave"  id="<%//= v.getLeaveID()%>"> <i class="ti-close"></i>Remove</a>
                                </div>
                            </div>
                            </button>
                        </div>
                        <div class="ibox-body">
                            <div class="flexbox mb-4">
                                <div class="flexbox">
                                    <span class="flexbox mr-3">
                                        <span class="mr-2 text-muted">Baki Cuti Tahunan</span>
                                        <span class="h3 mb-0 text-primary font-strong"><%= LeaveDAO.getBalanceEligibleLeave(log, v.getStaffID(), AccountingPeriod.getCurYearByCurrentDate())%></span>
                                    </span>
                                    <span class="flexbox mr-3">
                                        <span class="mr-2 text-muted">Cuti Layak Bulan Ini</span>
                                        <span class="h3 mb-0 text-primary font-strong"><%= LeaveDAO.getEligibleLeaveForTheMonth(log, v.getStaffID(), v.getYear(), v.getDatestart()) %></span>
                                    </span>
                                </div>
                            </div>
                            <div class="ibox-fullwidth-block">
                                <table class="table">
                                    <thead class="thead-default thead-lg">
                                        <tr>
                                            <th class="pl-4">Subjek</th>
                                            <th>Hari</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="pl-4">
                                                <div class="flexbox-b">

                                                    <div>
                                                        <h5 class="mb-1">Sebab Bercuti : <%=  v.getReason()%></h5>
                                                        <div>
                                                            <span class="font-13"><i class="ti-calendar mr-2"></i><%= AccountingPeriod.fullDateMonth(v.getDatestart())%> - <%= AccountingPeriod.fullDateMonth(v.getDateend())%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <h4 class="font-strong text-light mb-0"><%= v.getDays()%></h4>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>

        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>
