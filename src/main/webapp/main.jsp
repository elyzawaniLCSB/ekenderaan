<%-- 
    Document   : main
    Created on : Sep 29, 2019, 9:59:49 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //String balLeave = "";
    double balLeave = 0;

    String curYear = AccountingPeriod.getCurYearByCurrentDate();
    String curMonth = AccountingPeriod.getCurPeriodByCurrentDate();
    String curDate = AccountingPeriod.getCurrentTimeStamp();

    if (LeaveDAO.isLeaveInfoExist(log, log.getUserID(), AccountingPeriod.getCurrentTimeStamp())) {
        balLeave = LeaveDAO.getBalanceEligibleLeave(log, log.getUserID(), AccountingPeriod.getCurYearByCurrentDate());
    }

    double bakiCutiLayak = LeaveDAO.getEligibleLeaveForTheMonth(log, log.getUserID(), curYear, curDate);
    double bakiMC = LeaveDAO.getTotalSickLeaveOfTheYear(log, log.getUserID(), curYear);
%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {



        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });

        $('.gotoleavetab').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavetab&tabid=' + id);

            return false;
        });


        $('.view-modal').click(function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "PathController?process=viewleavemodal&leaveID=" + leaveid + "&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">

                <div class="row mb-4">
                    <div class="col-sm-12 col-lg-12">
                        <div class="card overflow-visible mt-5 bg-primary" >
                            <div class="card-body text-center mt-4">
                                <img class="img-circle img-bordered card-abs-top-center" src="<%= log.getImageURL()%>" alt="image" width="60" />
                                <h6 class="mb-1">
                                    <a class="text-white"><%= log.getFullname()%></a>
                                </h6><small class="text-white"><%= StaffDAO.getInfo(log, log.getUserID()).getPosition()%></small><br>
                                <small class="text-white"><%= StaffDAO.getInfo(log, log.getUserID()).getDepartment()%></small>
                                <%
                                    int leavePrep = LeaveDAO.getLeaveInProcess(log);
                                    if (leavePrep > 0) {
                                %>
                                <div class="d-flex justify-content-around align-items-center mt-3 font-16">
                                    <a href="leave_list_approval.jsp?tab=1"><small class="text-white"><span class="badge badge-danger badge-circle"><%=leavePrep%></span> Cuti sedang diproses.</small></a>
                                    <a class="text-white" href="javascript:;"><small><i class="fa fa-calendar"></i> <%= AccountingPeriod.getFullCurrentDate() %></small></a>

                                </div>
                                <%
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-lg-4 col-md-6">
                        <div class="alert alert-warning alert-dismissable fade show alert-bordered has-icon"><i class="la la-warning alert-icon"></i>
                            <strong>Makluman</strong><br><span class="badge badge-danger badge-circle"><%=leavePrep%></span> cuti sedang diproses. <a href="leave_list_approval.jsp?tab=1"><u>Lihat</u></a></div>

                    </div>-->
                    <!--<div class="col-lg-4 col-md-6">
                        <div class="alert alert-warning alert-dismissable fade show alert-bordered has-icon"><i class="la la-warning alert-icon"></i>
                            <strong>Makluman</strong><br>Anda mempunyai <span class="badge badge-danger badge-circle">1</span> cuti yang sedang diproses. </div>
                    </div>-->
                </div>
                <%
                        // }
%>

                <div class="row mb-4">
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b">
                                <div class="easypie mr-4" data-percent="<%=balLeave%>" data-bar-color="#18C5A9" data-size="80" data-line-width="8">
                                    <span class="easypie-data text-success" style="font-size:32px;"><i class="la la-calendar"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-success">
                                        <%= GeneralTerm.PrecisionDoubleWithoutDecimal(balLeave)%></h3> 
                                    <div class="text">Baki Cuti Tahunan</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b gotoleavetab" id="1">
                                <div class="easypie mr-4" data-percent="<%= bakiCutiLayak%>" data-bar-color="#5c6bc0" data-size="80" data-line-width="8">
                                    <span class="easypie-data font-26 text-primary"><i class="ti-archive"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-primary"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(bakiCutiLayak)%></h3>
                                    <div class="text-muted">Layak Bulan Ini</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b gotoleavetab" id="1">
                                <div class="easypie mr-4" data-percent="<%= bakiMC%>" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                                    <span class="easypie-data font-26 text-pink"><i class="fa fa-medkit"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-pink"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(bakiMC)%></h3>
                                    <div class="text-muted">Baki Cuti Sakit</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%
                        if (log.getAccessLevel() == 2 || log.getAccessLevel() == 3) {
                    %>
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b gotoleavetab" id="2">
                                <div class="easypie mr-4" data-percent="70" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                                    <span class="easypie-data text-pink" style="font-size:32px;"><i class="la la-tags"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-pink"><%= LeaveDAO.getCountSuperviseeLeave(log)%></h3>
                                    <div class="text-muted">Cuti Seliaan</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%}%>
                </div>

                <div class="row mb-4">
                    <div class="col-lg-4">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Cuti Hari Ini </div>
                                <div class="ibox-tools">
                                    <a href="leave_calendar.jsp?filter=<%= StaffDAO.getInfo(log, log.getUserID()).getDepartmentID() %>"><span class="badge badge-blue">Kalendar</span></a>
                                    <!-- <div class="dropdown-menu dropdown-menu-right">
                                         <a class="dropdown-item"><i class="ti-pencil mr-2"></i>Create</a>
                                         <a class="dropdown-item"><i class="ti-pencil-alt mr-2"></i>Edit</a>
                                         <a class="dropdown-item"><i class="ti-close mr-2"></i>Remove</a>
                                     </div>-->
                                </div>
                            </div>
                            <div class="ibox-body">
                                <ul class="media-list media-list-divider mr-2 scroller"><!--data-height="580px"-->

                                    <%
                                        List<Leave> listAlls = (List<Leave>) LeaveDAO.getAllConfirmedLeaveToday(log, "none");
                                        int m = 0;

                                        if (listAlls.isEmpty()) {

                                    %>
                                    <li class="media align-items-center">
                                        Tiada rekod

                                    </li>
                                    <%                                            }

                                        for (Leave j : listAlls) {
                                            m++;

                                            CoStaff st = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());


                                    %>
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading"><%= st.getName()%></div><span class="badge-<%= LeaveDAO.getBadgeColorTypeLeave(j.getType())%> badge-point"></span> <small class="text-muted"><%= j.getReason()%></small></div>
                                            <button class="btn btn-sm btn-primary btn-rounded view-modal"  id="<%= j.getLeaveID()%>">Lihat</button>
                                        </div>
                                    </li>

                                    <%
                                        }
                                    %>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="d-flex justify-content-between mb-4">
                                    <div>
                                        <h3 class="m-0">Analisis Cuti</h3>
                                        <div>Analisa cuti anda pada tahun dan bulan</div>
                                    </div>
                                    <ul class="nav nav-pills nav-pills-rounded nav-pills-air" id="chart_tabs">
                                        <li class="nav-item ml-1">
                                            <a class="nav-link active" data-toggle="tab" data-id="1" href="javascript:;">Tahun</a>
                                        </li>
                                        <li class="nav-item ml-1">
                                            <a class="nav-link" data-toggle="tab" data-id="2" href="javascript:;">Bulan</a>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <canvas id="visitors_chart" style="height:260px;"></canvas>
                                </div>
                            </div>
                            <!--<hr>
                            <div class="ibox-body">
                                <div class="row">
                                    <div class="col-6 pl-4">
                                        <h6 class="mb-3">GENDER</h6>
                                        <span class="h2 mr-3"><i class="fa fa-male text-primary h1 mb-0 mr-2"></i>
                                            <span>56<sup>%</sup></span>
                                        </span>
                                        <span class="h2 mr-3"><i class="fa fa-female text-pink h1 mb-0 mr-2"></i>
                                            <span>32<sup>%</sup></span>
                                        </span>
                                        <span class="h2"><i class="fa fa-question text-light h1 mb-0 mr-2"></i>
                                            <span>12<sup>%</sup></span>
                                        </span>
                                    </div>
                                    <div class="col-6">
                                        <h6 class="mb-3">SCREENS</h6>
                                        <span class="h2 mr-3"><i class="ti-desktop text-primary mr-2"></i>
                                            <span>49<sup>%</sup></span>
                                        </span>
                                        <span class="h2 mr-3"><i class="ti-tablet text-pink mr-2"></i>
                                            <span>29<sup>%</sup></span>
                                        </span>
                                        <span class="h2"><i class="ti-mobile text-success mr-2"></i>
                                            <span>22<sup>%</sup></span>
                                        </span>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>

                <div id="modalhere"></div>
            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            var data1 = [];
            var data3 = [];

            $.ajax({
                async: false,
                url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Tahunan",
                success: function (result) {
                    // $("#haha").html(result);
                    //console.log(result);
                    //data1 = result;
                    //console.log(data1);
                    var obj = jQuery.parseJSON(result);
                    var i = 0;
                    // console.log(obj);
                    $.each(obj, function (key, value) {
                        i++;
                        console.log(key);
                        data1.push(value);
                        //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                    });
                    console.log(data1);

                }});

           

            $.ajax({
                async: false,
                url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Sakit",
                success: function (result) {
                    // $("#haha").html(result);
                    //console.log(result);
                    //data1 = result;
                    //console.log(data1);
                    var obj = jQuery.parseJSON(result);
                    var i = 0;
                    // console.log(obj);
                    $.each(obj, function (key, value) {
                        i++;
                        console.log(key);
                        data3.push(value);
                        //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                    });
                    console.log(data3);

                }});

            var ctx = document.getElementById("visitors_chart").getContext("2d");
            var visitors_chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                    datasets: [
                        {
                            label: "Sakit",
                            data: data3,
                            borderColor: 'rgba(117,54,230,0.9)',
                            backgroundColor: 'rgba(117,54,230,0.9)',
                            pointBackgroundColor: 'rgba(117,54,230,0.9)',
                            pointBorderColor: 'rgba(117,54,230,0.9)',
                            borderWidth: 1,
                            pointBorderWidth: 1,
                            pointRadius: 0,
                            pointHitRadius: 30,
                        }, {
                            label: "Tahunan",
                            data: data1,
                            borderColor: 'rgba(104,218,221,1)',
                            backgroundColor: 'rgba(104,218,221,1)',
                            pointBackgroundColor: 'rgba(104,218,221,1)',
                            pointBorderColor: 'rgba(104,218,221,1)',
                            borderWidth: 1,
                            pointBorderWidth: 1,
                            pointRadius: 0,
                            pointHitRadius: 30,
                        },
                    ],
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    showScale: false,
                    scales: {
                        xAxes: [{
                                gridLines: {
                                    display: false,
                                },
                            }],
                        yAxes: [{
                                gridLines: {
                                    display: false,
                                    drawBorder: false,
                                },
                            }]
                    },
                    legend: {
                        labels: {
                            boxWidth: 12
                        }
                    },
                }
            });

            visitors_data = {

                1: {
                    data: [
                        data3,
                        data1,
                    ],
                    labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                },
                2: {
                    data: [
                        [2, 1, 4, 7],
                        [6, 2, 1, 3],
                        [3, 1, 2, 5],
                    ],
                    labels: ["Minggu 1", "Minggu 2", "Minggu 3", "Minggu 4"],
                },
            };

            $('#chart_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var id = $(this).attr('data-id');
                if (id && +id in visitors_data) {
                    visitors_chart.data.labels = visitors_data[id].labels;
                    var datasets = visitors_chart.data.datasets;
                    $.each(datasets, function (index, value) {
                        datasets[index].data = visitors_data[id].data[index];
                    });
                }
                visitors_chart.update();
            });
        });
    </script>
</body>
</html>
