<%-- 
    Document   : leave_detail_edit
    Created on : Jan 20, 2020, 4:06:24 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%@page import="com.lcsb.eleave.dao.EstateDAO"%>
<%@page import="com.lcsb.eleave.model.EstateInfo"%>
<%@page import="com.lcsb.eleave.dao.DateAndTime"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String sessionid = request.getParameter("sessionid");
    Leave v = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("id"));

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".select2_demo_1").select2();
        $("#savebutton").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();

var id = $('#leaveID').val();

            var a = $("#saveform :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=updateleaveform",
                success: function (result) {
                   swal({
                        title: "Cuti telah dikemaskini",
                        text: "Sila semak e-mail untuk maklumat lanjut",
                        type: "success"
                    }, function () {
                          $(location).attr('href', 'PathController?process=viewleavedetail&leaveID=' + id);
                    });
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });
        
        
        $("#back").click(function () {
            parent.history.back();
            return false;
        });
        $('#date_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
            orientation: "bottom",
        });
        $('.clockpicker').clockpicker();
        $(".select2_demo_2").select2({
            placeholder: "Select a state",
            allowClear: true
        });

        $('#dateend').change(function (e) {
            e.preventDefault();
            var a = $('#datestart').val();
            var b = $('#dateend').val();
            const date1 = new Date(a);
            const date2 = new Date(b);
            //const diffTime = Math.abs(date2 - date1);
            //const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            //console.log(diffDays + 1);
            //console.log(parseFloat(b) - parseFloat(a));
            
            console.log(calcBusinessDays(date1, date2));
            $('#days').val(calcBusinessDays(date1, date2));

            return false;
        });

        $('#gettypereceiver').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Receiver',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_receiver.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });
                    return $content;
                }
            });
            return false;
        });

        function calcBusinessDays(dDate1, dDate2) { // input given as Date objects
            var iWeeks, iDateDiff, iAdjust = 0;
            if (dDate2 < dDate1)
                return -1; // error code if dates transposed
            var iWeekday1 = dDate1.getDay(); // day of week
            var iWeekday2 = dDate2.getDay();
            iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
            iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
            if ((iWeekday1 > 5) && (iWeekday2 > 5))
                iAdjust = 1; // adjustment if both days on weekend
            iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1; // only count weekdays
            iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;

            // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
            iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

            if (iWeekday1 < iWeekday2) { //Equal to makes it reduce 5 days
                iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
            } else {
                iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
            }

            iDateDiff -= iAdjust // take into account both days on weekend

            return (iDateDiff + 1); // add 1 because dates are inclusive
        }
    });</script>

<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <div class="page-content fade-in-up">

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <form action="javascript:;" id="saveform">
                        <input type="hidden" id="leaveID" name="leaveID" value="<%= v.getLeaveID() %>">
                    <!--<h5 class="font-strong mb-5">BOOK A CAR</h5>-->
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Borang Permohonan Cuti
                                
                            </div>
                        </div>
                        <div class="ibox-body">
                            <div class="form-group">
                                <label class="form-control-label">Jenis Cuti</label>
                                <div>
                                    <select class="form-control selectpicker show-tick" id="type" name="type" data-width="200px">
                                        <%= ParameterDAO.parameterList(log, "Leave Type", v.getType())%>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="date_5">
                                <label class="font-normal">Tarikh Cuti Dipohon</label>
                                <div class="input-daterange input-group" id="datepicker">
                                    <input class="input-sm form-control" type="text" name="datestart" id="datestart" autocomplete="off" value="<%= v.getDatestart()%>">
                                    <span class="input-group-addon pl-2 pr-2">to</span>
                                    <input class="input-sm form-control" type="text" name="dateend" id="dateend"  autocomplete="off" value="<%= v.getDateend()%>">
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label>Tempoh Cuti</label>
                                <input class="form-control form-control-air" id="days" name="days" type="text" value="<%= v.getDays() %>" readonly="">
                            </div>
                            <div class="form-group mb-4">
                                <label>Sebab Bercuti</label>
                                <textarea class="form-control" rows="3" name="reason"><%= v.getReason() %></textarea>
                            </div>
                                <div class="text-right">
                                        <button class="btn btn-primary btn-air mr-2" id="savebutton">Kemaskini Permohonan</button>
                                        <button class="btn btn-secondary" id="back">Kembali</button>
                                    </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>

            <div id="modalhere"></div>
            <div id="modalpassengerdiv"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {




        $("#changedestination").change(function (e) {

        e.preventDefault();
                var type = $(this).val();
                $.ajax({
                url: "list_destination.jsp?type=" + type,
                        success: function (result) {
                        $('#destinationlist').empty().html(result).hide().fadeIn(300);
                        }
                });
                return false;
        });
                $('#destinationlist').on('change', '#selectdestination', function (e) {

        var destcode = $(this).val();
                var destdescp = ($(this).find(':selected').data('descp'));
                var desttype = ($(this).find(':selected').data('type'));
//var a = $("#saveform :input").serialize();
                $.ajax({
                async: true,
//data: a,
                        type: 'POST',
                        url: "ProcessController?process=savedestinationselected&desttype=" + desttype + "&destcode=" + destcode + "&destdescp=" + destdescp + "&sessionid=<%=sessionid%>",
                        success: function (result) {
                        if (result > 0) {
                        var appendthis = '<li class="media align-items-center" id="lielement' + result + '">'
                                //+ '<a class="media-img" href="javascript:;">'
                                //+ '<img class="img-circle" src="./assets/img/users/u6.jpg" alt="image" width="54" />'
                                //+ '</a>'
                                + '<div class="media-body d-flex align-items-center">'
                                + '<div class="flex-1">'
                                + '<div class="media-heading">' + desttype + '</div><small class="text-muted">' + destdescp + '</small></div>'
                                + '<button class="btn btn-sm btn-danger btn-rounded deletedestination" id="' + result + '">Delete</button>'
                                + ' </div>'
                                + ' </li>';
                                $('#destinationlistselected').append(appendthis);
                        } else {

                        $("#selectdestination").addClass("has-error")
// $(e).closest(".form-group").removeClass("has-error")
                        }

                        $('#selectdestination').prop('selectedIndex', 0);
                        }
                });
                return false;
        });
//$( "#otherdestination" ).keypress(function( e ) {
//$('#destinationlist').on('click', '#otherdestination', function(e){
                $('#destinationlist').on('click', '#enterdestination', function (e) {


//var dest = $(this : input).closest('input').find("input[name='otherdestination']").val();

        var dest = $('#otherdestination').val();
                $.ajax({
                async: true,
//data: a,
                        type: 'POST',
                        url: "ProcessController?process=savedestinationselected&desttype=Others&destcode=None&destdescp=" + dest + "&sessionid=<%=sessionid%>",
                        success: function (result) {
                        if (result > 0) {
                        var appendthis = '<li class="media align-items-center" id="lielement' + result + '">'
                                //+ '<a class="media-img" href="javascript:;">'
                                //+ '<img class="img-circle" src="./assets/img/users/u6.jpg" alt="image" width="54" />'
                                //+ '</a>'
                                + '<div class="media-body d-flex align-items-center">'
                                + '<div class="flex-1">'
                                + '<div class="media-heading">Others</div><small class="text-muted">' + dest + '</small></div>'
                                + '<button class="btn btn-sm btn-danger btn-rounded deletedestination" id="' + result + '">Delete</button>'
                                + ' </div>'
                                + ' </li>';
                                $('#destinationlistselected').append(appendthis);
                        } else {


                        }


                        }
                });
                return false;
        });
                $('#destinationlistselected').on('click', '.deletedestination', function (e) {

        var thisid = $(this).attr('id');
//$(this).closest("li").remove();
                var $k = $(this).closest("li");
                $.ajax({
                async: true,
//data: a,
                        type: 'POST',
                        url: "ProcessController?process=deletedestinationselected&id=" + thisid,
                        success: function (result) {

                        if (result == 1) {
                        $k.remove();
                        } else {

//$("#selectdestination").addClass("has-error")
// $(e).closest(".form-group").removeClass("has-error")
                        }
                        }
                });
                return false;
        });
                $('#list_destination_selected').on('click', '.viewmodaldate', function (e) {
        e.preventDefault();
                var id = $(this).attr('id');
                $.ajax({
                async: false,
                        url: "PathController?process=setdatefordestination&id=" + id,
                        success: function (result) {
                        $('#modalhere').empty().html(result).hide().fadeIn(300);
                        }
                });
                $('#myModal').modal('toggle')
                return false;
        });
                $('#list_passenger').on('click', '.viewmodalpassenger', function (e) {
        e.preventDefault();
                var id = $(this).attr('id');
                var sessiondid = $('#sessionid').val();
                $.ajax({
                async: false,
                        url: "PathController?process=viewmodalpassenger&sessionid=" + sessiondid + "&id=" + id,
                        success: function (result) {
                        $('#modalpassengerdiv').empty().html(result).hide().fadeIn(300);
                        }
                });
        });
    </script>
</body>


<script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- CORE SCRIPTS-->

</html>
