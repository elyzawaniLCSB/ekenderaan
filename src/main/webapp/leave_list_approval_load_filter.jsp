<%-- 
    Document   : leave_list_approval_load_filter
    Created on : Mar 2, 2020, 12:28:03 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.model.LeaveRequest"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.model.CoStaffDepartment"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String tab = request.getParameter("tab");

    String tab1 = "";
    String tab2 = "";

    String show1 = "";
    String show2 = "";

    if (tab.equals("2")) {
        tab1 = "";
        tab2 = "active";
        show1 = "";
        show2 = "show";
    } else if (tab.equals("1")) {
        tab1 = "active";
        tab2 = "";
        show1 = "show";
        show2 = "";
    }

    String year = AccountingPeriod.getCurYearByCurrentDate();
    String period = AccountingPeriod.getCurPeriodByCurrentDate();


%>
<div class="tab-pane fade <%= show1%> <%= tab1%> text-left" id="tab-11-1">


    <div class="col-lg-12">

        <ul class="media-list media-list-divider mr-2" data-height="470px">
            <!--<li class="media align-items-center">
                <a class="media-img" href="javascript:;">
                    <img class="img-circle" src="./assets/img/users/u8.jpg" alt="image" width="54" />
                </a>
                <div class="media-body d-flex align-items-center">
                    <div class="flex-1">
                        <div class="media-heading">Lynn Weaver</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                    <button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>
                </div>
            </li>-->
            <%
                List<Leave> listAlls = (List<Leave>) LeaveDAO.getAllLeaveOfYou(log);
                int m = 0;

                if (listAlls.isEmpty()) {

            %>
            <li class="media align-items-center">
                Tiada permohonan cuti

            </li>
            <%                                            }

                for (Leave j : listAlls) {
                    m++;

                    CoStaff st = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());
                    LeaveRequest lr = (LeaveRequest) LeaveDAO.getLeaveRequest(log, j.getLeaveID());


            %>
            <li class="media align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-sm-12 mb-2">
                            <div class="media-body d-flex align-items-center">
                                <div class="flex-1">
                                    <span class="badge badge-default"><span class="badge-<%= LeaveDAO.getBadgeColorTypeLeave(j.getType())%> badge-point"></span> <%= j.getType()%></span>
                                    <span class="badge badge-<%= LeaveDAO.getBadgeColor(j.getStatus())%> "><%= LeaveDAO.getMalayWord2(j.getStatus())%></span>
                                    <%
                                    if(lr != null && !j.getStatus().equals("Rejected")){
                                    %>
                                    <span class="badge badge-primary"><%= LeaveDAO.getMalayWord2(lr.getRequestto()) %></span>
                                    <%
                                        }
                                    %>
                                    <%
                                        if (LeaveDAO.hasComment(log, j.getLeaveID())) {
                                    %>

                                    <i class="fa fa-comments-o" aria-hidden="true"></i>
                                    <%
                                        }
                                    %>

                                    <p></p>
                                    <small><%= j.getReason()%></small><br>
                                    <span class="text-muted">Tarikh Permohonan :</span> <strong><%= AccountingPeriod.fullDateMonth(j.getDateapply())%></strong>
                                    <br>
                                    <span class="text-muted">Dari : </span><%= AccountingPeriod.fullDateMonth(j.getDatestart())%> <span class="text-muted">hingga</span> <%= AccountingPeriod.fullDateMonth(j.getDateend())%>
                                    <br>
                                    <span class="text-muted">Bilangan Hari : </span><%= j.getDays()%>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-12 mb-2">
                            <%
                                if (j.getStatus().equals("Preparing")) {

                            %>
                            <button class="btn btn-sm btn-warning btn-rounded edit-leave" id="<%= j.getLeaveID()%>"><i class="fa fa-pencil"></i></button>&nbsp;

                            <button class="btn btn-sm btn-danger btn-rounded delete-leave" id="<%= j.getLeaveID()%>" title="1"><i class="fa fa-trash"></i></button>&nbsp;
                                <%
                                    }
                                %>

                            <%
                                if (LeaveDAO.isLeaveInfoExist(log, st.getStaffid(), j.getDatestart())) {

                            %>

                            <button class="btn btn-sm btn-blue btn-rounded view-modal" id="<%= j.getLeaveID()%>"><i class="fa fa-eye" aria-hidden="true"></i> Lihat</button>&nbsp;

                            <%
                            } else if (!LeaveDAO.isLeaveInfoExist(log, st.getStaffid(), j.getDatestart()) && log.getAccessLevel() == 1) {
                            %>
                            <button class="btn btn-sm btn-danger btn-rounded update-modal" id="<%= j.getLeaveID()%>">Kemaskini Cuti</button>&nbsp;

                            <%
                                }
                            %>
                            <!--<button class="btn btn-sm btn-primary btn-rounded">Sah</button>&nbsp;
                            <button class="btn btn-sm btn-warning btn-rounded">Batal</button>-->

                        </div>
                    </div>
                </div>

            </li>
            <%
                }
            %>

        </ul>
    </div>

</div>
<%
    if (log.getAccessLevel() > 0) {

%>
<div class="tab-pane fade <%= show2%> <%= tab2%> text-left" id="tab-11-2">
    <div class="col-lg-12">
        <!--<div class="btn-group btn-square" data-toggle="buttons">
            <label class="btn btn-outline-primary"><span class="badge-blue badge-point"></span><span class="button-text-approve"> Baru</span>
                <input type="radio">
            </label>
            <label class="btn btn-outline-primary"><span class="badge-warning badge-point"></span><span class="button-text-approve"> Semak</span>
                <input type="radio">
            </label>
            <label class="btn btn-outline-primary"><span class="badge-pink badge-point"></span><span class="button-text-approve"> Sokong</span>
                <input type="radio">
            </label>
            <label class="btn btn-outline-primary"><span class="badge-success badge-point"></span><span class="button-text-approve"> Lulus</span>
                <input type="radio">
            </label>
            <label class="btn btn-outline-primary"><span class="badge-danger badge-point"></span><span class="button-text-approve"> Batal</span>
                <input type="radio">
            </label>
        </div>-->

        <!--<div class="dropdown">
            <button class="btn btn-primary btn-rounded dropdown-toggle dropdown-arrow" data-toggle="dropdown">Susun</button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="javascript:;"><span class="badge-blue badge-point"></span>&nbsp;&nbsp;Baru</a>
                <a class="dropdown-item" href="javascript:;"><span class="badge-warning badge-point"></span>&nbsp;&nbsp;Semak</a>
                <a class="dropdown-item" href="javascript:;"><span class="badge-pink badge-point"></span>&nbsp;&nbsp;Sokong</a>
                <a class="dropdown-item" href="javascript:;"><span class="badge-success badge-point"></span>&nbsp;&nbsp;Lulus</a>
                <a class="dropdown-item" href="javascript:;"><span class="badge-danger badge-point"></span>&nbsp;&nbsp;Batal</a>
            </div>
        </div>-->

        <div class="tab-pane fade active show" id="accordion-1">
            <ul class="media-list media-list-divider mr-2" data-height="470px">
                <!--<li class="media align-items-center">
                    <a class="media-img" href="javascript:;">
                        <img class="img-circle" src="./assets/img/users/u8.jpg" alt="image" width="54" />
                    </a>
                    <div class="media-body d-flex align-items-center">
                        <div class="flex-1">
                            <div class="media-heading">Lynn Weaver</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                        <button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>
                    </div>
                </li>-->
                <%List<Leave> listAll = (List<Leave>) LeaveDAO.getAllLeaveExcludeYou(log, year, period, "None");
                    int i = 0;

                    if (listAll.isEmpty()) {

                %>
                <li class="media align-items-center">
                    Tiada permohonan cuti

                </li>

                <%                                            }

                    for (Leave j : listAll) {
                        i++;

                        CoStaff st = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());

LeaveRequest lr = (LeaveRequest) LeaveDAO.getLeaveRequest(log, j.getLeaveID());


                %>
                <li class="media align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-1 col-sm-12 mb-2">
                                <a class="media-img" href="javascript:;">
                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                </a>
                            </div>
                            <div class="col-lg-10 col-sm-12 mb-2">
                                <div class="media-body d-flex align-items-center">
                                    <div class="flex-1">
                                        <span class="badge badge-default"><span class="badge-<%= LeaveDAO.getBadgeColorTypeLeave(j.getType())%> badge-point"></span> <%= j.getType()%></span>
                                        <span class="badge badge-<%= LeaveDAO.getBadgeColor(j.getStatus())%> "><%= LeaveDAO.getMalayWord2(j.getStatus())%></span>
                                         <%
                                    if(lr != null && !j.getStatus().equals("Rejected")){
                                    %>
                                    <span class="badge badge-primary"><%= LeaveDAO.getMalayWord2(lr.getRequestto()) %></span>
                                    <%
                                        }
                                    %>
                                        <%
                                            if (LeaveDAO.hasComment(log, j.getLeaveID())) {
                                        %>

                                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                                        <%
                                            }
                                        %>
                                        <p></p>
                                        <div class="media-heading"><%= GeneralTerm.capitalizeFirstLetter(st.getName())%> </div><small><%= j.getReason()%></small><br>

                                        <span class="text-muted">Tarikh Permohonan :</span> <strong><%= AccountingPeriod.fullDateMonth(j.getDateapply())%></strong>
                                        <br>
                                        <span class="text-muted">Dari : </span><%= AccountingPeriod.fullDateMonth(j.getDatestart())%> <span class="text-muted">hingga</span> <%= AccountingPeriod.fullDateMonth(j.getDateend())%>
                                        <br>
                                        <span class="text-muted">Bilangan Hari : </span><%= j.getDays()%>
                                    </div>


                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-12">
                                <%
                                    if (LeaveDAO.isLeaveInfoExist(log, st.getStaffid(), j.getDatestart())) {

                                %>

                                <button class="btn btn-sm btn-blue btn-rounded view-modal" id="<%= j.getLeaveID()%>"><i class="fa fa-eye" aria-hidden="true"></i> Lihat</button>&nbsp;

                                <%
                                } else if (!LeaveDAO.isLeaveInfoExist(log, st.getStaffid(), j.getDatestart()) && log.getAccessLevel() == 1) {
                                %>
                                <button class="btn btn-sm btn-danger btn-rounded update-modal" id="<%= j.getLeaveID()%>"><i class="fa fa-pencil" aria-hidden="true"></i><span class="button-text"> Kemaskini Cuti</span></button>&nbsp;

                                <%
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </li>
                <%
                    }
                %>

            </ul>

        </div>

    </div>
</div>
<%                                }
%>