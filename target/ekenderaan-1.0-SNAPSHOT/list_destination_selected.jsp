<%-- 
    Document   : list_destination_selected
    Created on : Nov 6, 2019, 10:55:57 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.BookingDestinationTemp"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

%>
<div class="col-sm-12 mr-5">
    <ul class="media-list media-list-divider mr-2 pre-scrollable" data-height="480px">

        <%List<BookingDestinationTemp> listAll = (List<BookingDestinationTemp>) BookingDAO.getListTempDestination(log, request.getParameter("sessionid"));

            for (BookingDestinationTemp j : listAll) {

                String dateinfo = "";
                String datebtn = "Get";

                if (!j.getStartdate().equals("0000-00-00")) {
                    dateinfo = "(From " + j.getStartdate() + "to " + j.getEnddate()+")";
                    datebtn = "Change";
                }

        %>
        <li class="media align-items-center">

            <div class="media-body d-flex align-items-center">
                <div class="flex-1">
                    <div class="media-heading"><%= j.getDesttype()%> <span id="datechange<%= j.getId()%>"><%= dateinfo%></span></div><small class="text-muted"><%= j.getDestdescp()%></small></div>
                <button class="btn btn-sm btn-outline-secondary btn-rounded viewmodaldate" id="<%= j.getId()%>"><span id="datebutton<%= j.getId()%>"><%= datebtn%></span> Date</button>
            </div>
        </li>
        <%                                        }
        %>
    </ul>
</div>