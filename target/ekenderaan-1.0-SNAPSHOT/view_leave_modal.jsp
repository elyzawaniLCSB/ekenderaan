<%-- 
    Document   : view_leave_modal
    Created on : Jan 22, 2020, 9:13:33 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.eleave.model.Book"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%@page import="com.lcsb.eleave.model.Driver"%>
<%@page import="com.lcsb.eleave.dao.DriverDAO"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //Car c = (Car) CarDAO.getCarInfo(log, request.getParameter("carID"));
    //Driver d = (Driver) DriverDAO.getDriverInfo(log, c.getDriverID());
    Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));
    CoStaff st = (CoStaff) StaffDAO.getInfo(log, l.getStaffID());
    String destID = request.getParameter("id");

    String yearApply = AccountingPeriod.getCurYearByDate(l.getDatestart());
    String monthApply = AccountingPeriod.getCurPeriodByDate(l.getDatestart());

    int totLeaveUse = LeaveDAO.getTotalLeaveOfTheYear(log, st.getStaffid(), yearApply);
    int totMedicalLeaveUse = LeaveDAO.getTotalSickLeaveUseOfTheYear(log, st.getStaffid(), yearApply);
    int totLeaveUseMonth = LeaveDAO.getTotalLeaveOfTheMonth(log, st.getStaffid(), yearApply, monthApply);
    double totLeaveYear = LeaveDAO.getEligibleLeaveOfTheYear(log, st.getStaffid(), yearApply);
    double totMedicalLeaveYear = LeaveDAO.getMedicalLeaveOfTheYear(log, st.getStaffid(), yearApply);
    double percentYear = LeaveDAO.getPercentLeaveUseYear(log, st.getStaffid(), yearApply);
    double percentYearMedical = LeaveDAO.getPercentMedicalLeaveUseYear(log, st.getStaffid(), yearApply);
    double percentMonth = LeaveDAO.getPercentLeaveUseMonth(log, st.getStaffid(), yearApply, monthApply, l.getDatestart());

    String buttonValid = "";
    String titleValid = "";

    if (log.getAccessLevel() == 2) {

        buttonValid = "Sokong";
        titleValid = "Supported";
    } else if (log.getAccessLevel() == 1) {

        buttonValid = "Semak";
        titleValid = "Checked";
    } else if (log.getAccessLevel() == 3) {

        buttonValid = "Lulus";
        titleValid = "Approved";
    } else if (log.getAccessLevel() == 4) {

        buttonValid = "Sah";
        titleValid = "Verified";
    }

%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".actionto").unbind('click').bind('click', function (e) {

            $(this).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Tunggu ... ');
//var a = $("form").serialize();
            var id = $(this).attr('id');
            var action = $(this).attr('title');

            var stringnoti = 'disahkan';

            if (action == 'Rejected') {
                stringnoti = 'dibatalkan';
            }

            $.ajax({
                async: true,
                url: "PathController?process=validateleave&leaveID=" + id + "&action=" + action,
                success: function (result) {
                    //if (result > 0) {

                    //}

                    if (action == 'Rejected') {
                        alertify
                                .defaultValue("")
                                .prompt("Nyatakan sebab anda tolak permohonan ini.",
                                        function (val, ev) {
                                            ev.preventDefault();
                                            $.ajax({
                                                async: true,
                                                url: "PathController?process=leavecomment&leaveID=" + id + "&comment=" + val,
                                                success: function (result) {
                                                    //if (result > 0) {

                                                    //}


                                                    alertify.success("Mesej anda telah disimpan.");
                                                    //$(location).attr('href', 'leave_list_approval.jsp?tab=2');
                                                    $.ajax({
                                                        url: "leave_comment.jsp?leaveID=<%= l.getLeaveID()%>",
                                                        success: function (result) {
                                                            $('#comment-section').empty().html(result).hide().fadeIn(300);
                                                            swal({
                                                                title: "Cuti telah " + stringnoti,
                                                                text: "Sila semak e-mail untuk maklumat lanjut",
                                                                type: "success"
                                                            }, function () {
                                                                $(location).attr('href', 'leave_list_approval.jsp?tab=2');
                                                            });
                                                        }
                                                    });


                                                }
                                            });


                                        }, function (ev) {
                                    ev.preventDefault();
                                    //alertify.error("You've clicked Cancel");
                                }
                                );
                    } else {
                        swal({
                            title: "Cuti telah " + stringnoti,
                            text: "Sila semak e-mail untuk maklumat lanjut",
                            type: "success"
                        }, function () {
                            $(location).attr('href', 'leave_list_approval.jsp?tab=2');
                        });

                    }

                }
            });
            //} 

            e.stopPropagation();
            return false;
        });

        $(".askdetailfirst").click(function () {


            var id = $(this).attr('id');

            alertify
                    .defaultValue("")
                    .prompt("Nyatakan soalan dan komen anda",
                            function (val, ev) {
                                ev.preventDefault();
                                $.ajax({
                                    async: true,
                                    url: "PathController?process=leavecomment&leaveID=" + id + "&comment=" + val,
                                    success: function (result) {
                                        //if (result > 0) {

                                        //}
                                        alertify.success("Mesej anda telah disimpan.");
                                        //$(location).attr('href', 'leave_list_approval.jsp?tab=2');
                                        $.ajax({
                                            url: "leave_comment.jsp?leaveID=<%= l.getLeaveID()%>",
                                            success: function (result) {
                                                $('#comment-section').empty().html(result).hide().fadeIn(300);
                                            }
                                        });


                                    }
                                });


                            }, function (ev) {
                        ev.preventDefault();
                        alertify.error("You've clicked Cancel");
                    }
                    );
            return false;
        });

        $(".viewdetail").unbind('click').bind('click', function (e) {


            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavedetail&leaveID=' + id);


            e.stopPropagation();
            return false;
        });


    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="savecardestination">
            <input type="hidden" name="destID" id="destID" value="<%= destID%>">
            <input type="hidden" name="bookID" id="bookID" value="<%= l.getLeaveID()%>">
            <div class="modal-header p-4">
                <h5 class="modal-title">Permohonan Cuti</h5><span class="badge badge-<%= LeaveDAO.getBadgeColor(l.getStatus())%>"><%= LeaveDAO.getMalayWord2(l.getStatus()) %></span>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                    <%
                        if (!LeaveDAO.isButtonDisable(log, l.getLeaveID())) {
                    %>
                    <button class="btn btn-success btn-rounded mr-1 actionto" title="<%= titleValid%>" id="<%=l.getLeaveID()%>"><i class="fa fa-check"></i><span class="button-text-approve"> <%= buttonValid%></span></button>
                    <button class="btn btn-danger btn-rounded mr-3 actionto" title="Rejected" id="<%=l.getLeaveID()%>"><i class="fa fa-remove"></i><span class="button-text-approve"> Tolak</span></button>


                    <%
                        }
                    %>
                    <!--<label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-edit font-20"></i>
                        <input type="file">
                    </label>
                    <label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-image font-20"></i>
                        <input type="file">
                    </label>-->


                </div>
                <button class="btn btn-blue btn-rounded mr-3 viewdetail pull-right" title="Ask" id="<%=l.getLeaveID()%>"><i class="fa fa-expand"></i></button>
            </div>
            <div class="modal-body p-4">
                <div class="ibox">
                    <div class="ibox-body">
                        <!--<h5 class="font-strong mb-4"><%= st.getName()%></h5>-->
                        <div class="flexbox-b mb-4">
                            <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="40" />
                            <div class="flex-1">

                                <div class="font-strong font-14">&nbsp;&nbsp;<%= st.getName()%><small class="text-muted float-right"><%= l.getDateapply()%></small></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getPosition()%></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getLocation()%></div><br>


                            </div>

                        </div>

                        <div class="row">

                            <%
                                if (l.getType().equals("Cuti Tahunan")) {
                            %>
                            <div class="col-6 flexbox-b"><i class="la la-calendar-check-o font-26 text-light mr-3"></i>
                                <div class="flex-1">
                                    <div>
                                        <span class="text-light mr-3">Tahun</span><%= totLeaveUse%> / <%= GeneralTerm.PrecisionDoubleWithoutDecimal(totLeaveYear)%></div>
                                    <div class="progress mt-1">
                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentYear)%>" role="progressbar" style="width:<%= percentYear%>%; height:5px;" aria-valuenow="<%= totLeaveUse%>" aria-valuemin="0" aria-valuemax="<%= totLeaveYear%>"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 flexbox-b"><i class="la la-hourglass-1 font-26 text-light mr-3"></i>
                                <div class="flex-1">
                                    <div>
                                        <span class="text-light mr-3">Bulan</span><%= totLeaveUseMonth%> / <%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveForTheMonthExcludeApprovedThisMonth(log, st.getStaffid(), yearApply, l.getDatestart()))%></div>
                                    <div class="progress mt-1">
                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentMonth)%>" role="progressbar" style="width:<%= percentMonth%>%; height:5px;" aria-valuenow="<%= percentMonth%>" aria-valuemin="0" aria-valuemax="<%= LeaveDAO.getEligibleLeaveForTheMonth(log, st.getStaffid(), yearApply, l.getDatestart())%>"></div>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else if (l.getType().equals("Cuti Sakit")) {
                            %>
                            <div class="col-12 flexbox-b"><i class="la la-medkit font-26 text-light mr-3"></i>
                                <div class="flex-1">
                                    <div>
                                        <span class="text-light mr-3">Tahun</span><%= totMedicalLeaveUse%> / <%= GeneralTerm.PrecisionDoubleWithoutDecimal(totMedicalLeaveYear)%></div>
                                    <div class="progress mt-1">
                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentYearMedical)%>" role="progressbar" style="width:<%= percentYearMedical%>%; height:5px;" aria-valuenow="<%= totMedicalLeaveUse%>" aria-valuemin="0" aria-valuemax="<%= totMedicalLeaveYear%>"></div>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-body">
                        <%
                            boolean t = LeaveDAO.isNewLeaveExceedEligibleMonth(log, st.getStaffid(), yearApply, l.getDatestart(), l.getDays(), l.getType());

                            if (!l.getStatus().equalsIgnoreCase("Approved")) {
                                if (t) {
                        %>
                        <div class="alert alert-danger alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>

                            <div class="d-flex align-items-center justify-content-between">
                                <div><strong>Tidak Layak!</strong><br>Cuti melebihi baki layak.</div>

                            </div>
                        </div>
                        <%
                        } else {
                        %>
                        <div class="alert alert-success alert-dismissable fade show alert-outline has-icon"><i class="la la-check alert-icon"></i>

                            <div class="d-flex align-items-center justify-content-between">
                                <div><strong>Layak!</strong><br>Cuti tidak melebihi baki.</div>

                            </div>
                        </div>
                        <%
                                }
                            }

                        %>

                        <div class="col-md-12">
                            <div class="card mb-4">
                                <div class="rel">
                                    <%                                        if (l.getType().equals("Cuti Sakit")) {
                                    %>
                                    <img class="card-img-top"  src="uploaded_attachment/<%= LeaveDAO.getLeaveAttachment(log, l.getLeaveID()).getFilename()%>" alt="image" />



                                    <div class="card-img-overlay">
                                        <span class="badge badge-default">LAMPIRAN</span>
                                    </div>
                                    <%}%>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title mb-2">
                                        <a class="color-inherit" href="javascript:;"><%= l.getType()%></a>
                                    </h4>
                                    <div class="text-muted mb-3"><%= l.getReason()%></div>
                                    <p class="text-light">Dari : <strong><%= AccountingPeriod.fullDateMonth(l.getDatestart())%></strong></p>
                                    <p class="text-light">Hingga : <strong><%= AccountingPeriod.fullDateMonth(l.getDateend())%></strong></p>
                                    <p class="text-light">Jumlah : <strong><%= l.getDays()%> hari</strong></p>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>


            </div>

        </form>
    </div>
</div>