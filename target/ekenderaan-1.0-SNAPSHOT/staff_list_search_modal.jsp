<%-- 
    Document   : staff_list_search_modal
    Created on : Jan 24, 2020, 10:02:10 AM
    Author     : fadhilfahmi
--%>

<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.MemberDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Members"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String sessionid = request.getParameter("sessionid");


%>
<script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $.ajax({
            url: "staff_list_search_modal_view.jsp",
            success: function (result) {
                $('#result-payee').empty().html(result).hide().fadeIn(300);

            }});

        $('#keyword-payee').keyup(function () {
            //var l = $('input[name=searchby]:checked').val();
            var keyword = $(this).val();

            $.ajax({
                url: "staff_list_search_modal_view.jsp?type=<%= request.getParameter("type")%>&keyword=" + keyword,
                success: function (result) {
                    $('#result-payee').empty().html(result).hide().fadeIn(300);

                }});
        });



        $('#result-payee').on('click', '.thisresult-select', function (e) {
            var b = $(this).attr('id');
            var c = $(this).attr('title');
            var i = $(this).attr('href');

            $('#name').val(c);
            $('#staffIDX').val(b);
            $('#modalpassenger').modal('toggle');
            e.preventDefault();
        });

        $('#list-passenger-selected').on('click', '.deletepassenger', function (e) {

            var thisid = $(this).attr('id');
            //$(this).closest("li").remove();
            var $k = $(this).closest("li");

            $.ajax({
                async: true,
                //data: a,
                type: 'POST',
                url: "ProcessController?process=deletepassengerselected&id=" + thisid,
                success: function (result) {

                    if (result == 1) {
                        $k.remove();

                    } else {

                        //$("#selectdestination").addClass("has-error")
                        // $(e).closest(".form-group").removeClass("has-error")
                    }
                }
            });

            return false;

        });



    });
</script>
<div class="modal fade" id="modalpassenger">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="modalcontent">
            <input type="hidden" name="id" id="id" value="<%//= b.getId()%>">
            <div class="modal-header p-4">
                <h5 class="modal-title">Senarai Nama</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">

                <div class="form-group">
                    <div class="form-group input-group">
                        <input type="text" class="form-control" id="keyword-payee">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>

                <div id="result-payee">



                </div>



            </div>
            <!--<div class="modal-footer justify-content-between bg-primary-50">
                <div>
                    <button class="btn btn-primary btn-rounded mr-3" id="savedate">Save</button>
                </div>

            </div>-->
        </form>
    </div>
</div>