<%-- 
    Document   : top
    Created on : Sep 29, 2019, 10:02:16 PM
    Author     : fadhilfahmi
--%>

<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.NotifyDAO"%>
<%@page import="com.lcsb.eleave.model.Notification"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");


%>
<!DOCTYPE html>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(this).scrollTop(0);

        var idleState = false;
        var idleTimer = null;
        $('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
            //console.log('breakidle');
            clearTimeout(idleTimer);
            if (idleState == true) {
                console.log('idle1:' + idleTimer);
                $.ajax({
                    url: "notification.jsp",
                    success: function (result) {
                        $('.notify-section').empty().html(result).hide().fadeIn(300);
                    }
                });

            }
            idleState = false;
            idleTimer = setTimeout(function () {

                idleState = true;
                console.log('idle2:' + idleState);
            }, 10000);
        });
        $("body").trigger("mousemove");

        $.ajax({
            url: "notification.jsp",
            success: function (result) {
                $('.notify-section').empty().html(result).hide().fadeIn(300);
            }
        });


        $('#applyleave').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'PathController?process=applyleavestep');

            return false;
        });



    });

</script>
<header class="header">
    <!-- START TOP-LEFT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li>
            <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </li>
        <a class="page-brand" href="Login"><img src="./assets/img/android-chrome-192x192.png" width="90%"></a>
        <li class="dropdown mega-menu">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:;">eCuti</a>
            <!--<div class="dropdown-menu">
                <div class="dropdown-arrow"></div>
                <div class="mega-toolbar-menu">
                    <div class="d-flex">
                        <a class="mega-toolbar-item" href="javascript:;">
                            <div class="item-icon"><i class="ti-file"></i></div>
                            <div class="item-name">Reports</div>
                            <div class="item-text">Lorem Ipsum is simply dummy.</div>
                            <div class="item-details">74 New</div>
                        </a>
                        <a class="mega-toolbar-item" href="javascript:;">
                            <div class="item-icon"><i class="ti-shopping-cart-full"></i></div>
                            <div class="item-name">Orders</div>
                            <div class="item-text">Lorem Ipsum is simply dummy.</div>
                            <div class="item-details">125 New</div>
                        </a>
                    </div>
                    <div class="d-flex">
                        <a class="mega-toolbar-item" href="javascript:;">
                            <div class="item-icon"><i class="ti-wallet"></i></div>
                            <div class="item-name">Profit</div>
                            <div class="item-text">Lorem Ipsum is simply dummy.</div>
                            <div class="item-details">+1200<sup>$</sup></div>
                        </a>
                        <a class="mega-toolbar-item" href="javascript:;">
                            <div class="item-icon"><i class="ti-support"></i></div>
                            <div class="item-name">Support</div>
                            <div class="item-text">Lorem Ipsum is simply dummy.</div>
                            <div class="item-details">54 New Ticket</div>
                        </a>
                    </div>
                </div>
            </div>-->
        </li>
        <li>
            <!--<a class="nav-link search-toggler js-search-toggler"><i class="ti-search"></i>
                <span>Carian...</span>
            </a>-->
        </li>
        <li>
            <button class="btn btn-white btn-rounded" id="applyleave"><i class="fa fa-plus" aria-hidden="true"></i><span class="button-text-approve"> Mohon</span> Cuti</button>
        </li>
    </ul>

    <!-- END TOP-LEFT TOOLBAR-->
    <!--LOGO-->

    <!-- START TOP-RIGHT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li class="dropdown dropdown-user">
            <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                <div class="admin-avatar">
                    <!--<img class="img-circle" src="<%= log.getImageURL()%>" alt="image" />-->
                </div>
                <span> <span class="badge badge-<%= StaffDAO.getBadgeLevelColor(log.getAccessLevel())%> badge-circle"><%=log.getAccessLevel()%></span> <%= log.getFullname()%></span>
            </a>
            <div class="dropdown-menu dropdown-arrow dropdown-menu-right admin-dropdown-menu">
                <div class="dropdown-arrow"></div>
                <div class="dropdown-header">
                    <div class="admin-avatar">
                        <img src="<%= log.getImageURL()%>" alt="image" />
                    </div>
                    <div>
                        <h5 class="font-strong text-white"><%= log.getFullname()%></h5>
                        <div>
                            <!--<span class="admin-badge mr-3"><i class="ti-alarm-clock mr-2"></i>30m.</span>-->
                            <span class="admin-badge"><i class="ti-user mr-2"></i><%= StaffDAO.getInfo(log, log.getUserID()).getPosition()%></span>
                        </div>
                    </div>
                </div>
                <div class="admin-menu-features">
                    <a class="admin-features-item" href="user_page.jsp?id=<%= log.getUserID()%>"><i class="ti-user"></i>
                        <span>PROFILE</span>
                    </a>
                    <a class="admin-features-item" href="javascript:;"><i class="ti-support"></i>
                        <span>SUPPORT</span>
                    </a>
                    <a class="admin-features-item" href="javascript:;"><i class="ti-settings"></i>
                        <span>SETTINGS</span>
                    </a>
                </div>
                <div class="admin-menu-content">
                    <div class="text-muted mb-2">Baki Cuti Anda</div>
                    <div><i class="ti-briefcase h1 mr-3 text-light"></i>
                        <span class="h1 text-success"><%//= LeaveDAO.getBalanceEligibleLeave(log, log.getUserID(), AccountingPeriod.getCurYearByCurrentDate()) %></span>
                    </div>
                    <div class="d-flex justify-content-between mt-2">
                        <a class="text-muted" href="javascript:;">Earnings history</a>

                        </a>

                        <a class="d-flex align-items-center" href="javascript:;" onclick="signOut();">Logout<i class="ti-shift-right ml-2 font-20"></i></a>
                    </div>
                </div>
            </div>
        </li>

        <li>
            <a class="nav-link toolbar-icon" href="user_page.jsp?id=<%= log.getUserID() %>"><i class="fa fa-user" aria-hidden="true"></i></a>
        </li>
        <li class="dropdown dropdown-notification notify-section">

        </li>
        <li class="timeout-toggler">
            <a class="nav-link toolbar-icon" data-toggle="modal" data-target="#session-dialog" href="javascript:;"><i class="fa fa-sign-out" aria-hidden="true"><span class="notify-signal"></span></i></a>
        </li>
    </ul>
    <!-- END TOP-RIGHT TOOLBAR-->
</header>