<%-- 
    Document   : footer
    Created on : Sep 29, 2019, 10:53:35 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<footer class="page-footer">
    <div class="font-13">2020 © <b>LCSB</b> - Istiqamah Mencipta Kecemerlangan</div>
    <!--<div>
        <a class="px-3 pl-4" href="http://themeforest.net/item/adminca-responsive-bootstrap-4-3-angular-4-admin-dashboard-template/20912589" target="_blank">Purchase</a>
        <a class="px-3" href="http://admincast.com/adminca/documentation.html" target="_blank">Docs</a>
    </div>-->
    <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
</footer>
