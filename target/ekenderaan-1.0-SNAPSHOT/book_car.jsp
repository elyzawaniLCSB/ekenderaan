<%-- 
    Document   : book_car
    Created on : Oct 9, 2019, 12:41:18 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.dao.EstateDAO"%>
<%@page import="com.lcsb.smartbooking.model.EstateInfo"%>
<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $("#savebutton").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();

            var a = $("#saveform :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=savebook",
                success: function (result) {
                    $(location).attr('href', 'book_list.jsp');
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


        $("#back").click(function () {
            parent.history.back();
            return false;
        });

        $('#date_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('.clockpicker').clockpicker();

        $(".select2_demo_2").select2({
            placeholder: "Select a state",
            allowClear: true
        });

    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">

                <h5 class="font-strong mb-5">BOOK A CAR</h5>

                <form action="javascript:;" id="saveform">
                    <div class="row">
                        <!-- <div class="col-lg-4">
                              <div>
                                <img src="./assets/img/products/27.jpg" alt="image" />
                             </div>
                             <div class="flexbox-b mt-4">
                                 <div class="mr-2">
                                     <img src="./assets/img/products/28.jpg" alt="image" />
                                 </div>
                                 <div class="mr-2">
                                     <img src="./assets/img/products/29.jpg" alt="image" />
                                 </div>
                                 <div class="mr-2">
                                     <img src="./assets/img/products/30.jpg" alt="image" />
                                 </div>
                                 <div class="file-input-plus file-input"><i class="la la-plus-circle"></i>
                                     <input type="file">
                                 </div>
                             </div>
                         </div>-->

                        <div class="col-lg-4">
                            <div class="ibox">
                                <div class="ibox-body">
                                    <div class="form-group">
                                        <label class="form-control-label">Passenger</label>
                                        <select class="form-control select2_demo_2">
                                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                            </optgroup>
                                            <optgroup label="Pacific Time Zone">
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                                <option value="WA">Washington</option>
                                            </optgroup>
                                            <optgroup label="Mountain Time Zone">
                                                <option value="AZ">Arizona</option>
                                                <option value="CO">Colorado</option>
                                                <option value="ID">Idaho</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="UT">Utah</option>
                                                <option value="WY">Wyoming</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="ibox">
                                <div class="ibox-body">
                                    <h4><i class="la la-calendar"></i> Date & Time</h4>
                                    <div class="form-group" id="date_5">
                                        <label class="font-normal">From and To Date</label>
                                        <div class="input-daterange input-group" id="datepicker">
                                            <input class="input-sm form-control" type="text" name="startdate" value="<%= DateAndTime.getCurrentTimeStamp()%>">
                                            <span class="input-group-addon pl-2 pr-2">to</span>
                                            <input class="input-sm form-control" type="text" name="enddate" value="<%= DateAndTime.getCurrentTimeStamp()%>">
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6 form-group mb-4">
                                            <div class="form-group mb-4">
                                                <label>Start Time</label>
                                                <div class="input-group clockpicker" data-autoclose="true">
                                                    <input class="form-control" type="text" value="00:00" name="starttime">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-clock-o"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 form-group mb-4">
                                            <div class="form-group mb-4">
                                                <label>End Time</label>
                                                <div class="input-group clockpicker" data-autoclose="true">
                                                    <input class="form-control" type="text" value="00:00" name="endtime">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-clock-o"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group mb-4">
                                        <label>Reason</label>
                                        <textarea class="form-control form-control-solid" rows="4"  name="reason"></textarea>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Remark</label>
                                        <textarea class="form-control form-control-solid" rows="4"  name="reason"></textarea>
                                    </div>
                                    <!--<div class="form-group mb-4">
                                        <label class="ui-switch switch-icon mr-3 mb-0">
                                            <input type="checkbox" checked="" name="status">
                                            <span></span>
                                        </label>Available</div>-->
                                    <div class="text-right">
                                        <button class="btn btn-primary btn-air mr-2" id="savebutton">Save</button>
                                        <button class="btn btn-secondary" id="back">Cancel</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="ibox">
                                <div class="ibox-body">
                                    <h4><i class="la la-map-marker"></i> Destination</h4>
                                    <div class="col-sm-12 form-group mb-4">
                                        <div class="form-group mb-4">
                                            <label class="font-normal">Type</label>
                                            <select class="selectpicker show-tick form-control" title="Please select" data-style="btn-solid" name="status">

                                                <option value="Active">Estates</option>
                                                <option value="On Maintenance">Mill</option>
                                                <option value="Not Active">External</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 form-group mb-4">
                                        <div class="form-group mb-4">
                                            <label class="font-normal">Estates</label>
                                            <select class="selectpicker show-tick form-control" title="Please select" data-style="btn-solid" name="status">
                                                <%List<EstateInfo> listAll = (List<EstateInfo>) EstateDAO.getAllEstate(log, null);

                                                    for (EstateInfo j : listAll) {

                                                %>
                                                <option value="Active"><%= j.getEstatedescp()%></option>

                                                <%}%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-lg-4">
                            <div class="ibox">
                                <div class="ibox-body">
                                    <div class="form-group">
                                        <label class="form-control-label">Passenger</label>
                                        <select class="form-control select2_demo_2">
                                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                            </optgroup>
                                            <optgroup label="Pacific Time Zone">
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                                <option value="WA">Washington</option>
                                            </optgroup>
                                            <optgroup label="Mountain Time Zone">
                                                <option value="AZ">Arizona</option>
                                                <option value="CO">Colorado</option>
                                                <option value="ID">Idaho</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="UT">Utah</option>
                                                <option value="WY">Wyoming</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </form>
            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
</body>
</html>
