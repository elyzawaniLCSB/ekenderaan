<%-- 
    Document   : update_leave_modal
    Created on : Jan 23, 2020, 12:12:49 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.eleave.model.Book"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%@page import="com.lcsb.eleave.model.Driver"%>
<%@page import="com.lcsb.eleave.dao.DriverDAO"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));
    CoStaff st = (CoStaff) StaffDAO.getInfo(log, l.getStaffID());

    

%>
<!-- PLUGINS STYLES-->
<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".dial").knob();

        $(".actionto").unbind('click').bind('click', function (e) {
            
            var id = $(this).attr('id');
            var a = $("#saveleaveinfo :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=updateleaveinfo&leaveID=" + id,
                success: function (result) {
                    //if (result > 0) {

                    //}
                    swal({
                        title: "Cuti telah dikemaskini",
                        text: "",
                        type: "success"
                    }, function () {
                        $(location).attr('href', 'leave_list_approval.jsp?tab=2');
                    });

                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="saveleaveinfo">
            <input type="hidden" name="destID" id="destID" value="<%//= destID%>">
            <input type="hidden" name="bookID" id="bookID" value="<%//= l.getLeaveID()%>">
            <div class="modal-header p-4">
                <h5 class="modal-title">Maklumat Cuti</h5><span class="badge badge-<%//= LeaveDAO.getBadgeColor(l.getStatus())%>"><%//= l.getStatus()%></span>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="ibox">
                    <div class="ibox-body">
                        <div class="flexbox-b mb-4">
                            <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="40" />
                            <div class="flex-1">

                                <div class="font-strong font-14">&nbsp;&nbsp;<%= st.getName()%><small class="text-muted float-right"><%= l.getDateapply()%></small></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getPosition()%></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getLocation()%></div><br>


                            </div>



                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <label>Cuti Tahunan</label>
                                <input class="dial" value="0" data-width="85" data-height="85" type="text" name="eligibleleave">
                            </div>
                            <div class="col-sm-4">
                                <label>Baki Cuti Lepas</label>
                                <input class="dial" value="0" data-width="85" data-height="85" type="text" name="bf">
                            </div>
                            <div class="col-sm-4">
                                <label>Cuti Sakit</label>
                                <input class="dial" value="0" data-width="85" data-height="85" type="text" name="mc">
                            </div>

                        </div>

                    </div>
                </div>




            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                   
                    <button class="btn btn-primary btn-rounded mr-1 actionto" title="<%//= titleValid %>" id="<%=l.getLeaveID()%>">Kemaskini</button>
                   
                    
                   

                </div>
            </div>
        </form>
    </div>
</div>

<!-- PAGE LEVEL PLUGINS-->
<script src="./assets/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- CORE SCRIPTS-->
<!-- PAGE LEVEL SCRIPTS-->