<%-- 
    Document   : conf_car_add
    Created on : Sep 30, 2019, 1:03:28 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.dao.ParameterDAO"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $("#savebutton").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();

            var a = $("#saveform :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=savedriver",
                success: function (result) {
                    $(location).attr('href','conf_driver.jsp');
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });
        
        
        $("#back").click(function(){
		parent.history.back();
		return false;
	});
        


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="ibox">
                    <div class="ibox-body">
                        <h5 class="font-strong mb-5">ADD NEW DRIVER</h5>
                        <div class="row">
                            <!-- <div class="col-lg-4">
                                  <div>
                                    <img src="./assets/img/products/27.jpg" alt="image" />
                                 </div>
                                 <div class="flexbox-b mt-4">
                                     <div class="mr-2">
                                         <img src="./assets/img/products/28.jpg" alt="image" />
                                     </div>
                                     <div class="mr-2">
                                         <img src="./assets/img/products/29.jpg" alt="image" />
                                     </div>
                                     <div class="mr-2">
                                         <img src="./assets/img/products/30.jpg" alt="image" />
                                     </div>
                                     <div class="file-input-plus file-input"><i class="la la-plus-circle"></i>
                                         <input type="file">
                                     </div>
                                 </div>
                             </div>-->
                            <div class="col-lg-8">
                                <form action="javascript:;" id="saveform">
                                    <div class="form-group mb-4">
                                        <label>Name</label>
                                        <input class="form-control form-control-solid" type="text" placeholder="Enter Driver Name" name="driverName">
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-sm-6 form-group mb-4">
                                            <label>Staff ID</label>
                                            <input class="form-control form-control-solid" type="text" placeholder="LCSB Staff ID" name="staffID">
                                        </div>
                                        <div class="col-sm-6 form-group mb-4">
                                            <label>Email</label>
                                            
                                            <input class="form-control form-control-solid" type="text" placeholder="LCSB Email" name="email">
                                        </div>
                                        <div class="col-sm-6 form-group mb-4">
                                            <label>Status</label>
                                            <div>
                                                <select class="selectpicker show-tick form-control" title="Please select" data-style="btn-solid" name="status">
                                                        <%= ParameterDAO.parameterList(log,"Service Status", "")%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                                    

                                    <div class="form-group mb-4">
                                        <label>Remark</label>
                                        <textarea class="form-control form-control-solid" rows="4" placeholder="Description" name="remark"></textarea>
                                    </div>
                                    <!--<div class="form-group mb-4">
                                        <label class="ui-switch switch-icon mr-3 mb-0">
                                            <input type="checkbox" checked="" name="status">
                                            <span></span>
                                        </label>Available</div>-->
                                    <div class="text-right">
                                        <button class="btn btn-primary btn-air mr-2" id="savebutton">Save</button>
                                        <button class="btn btn-secondary" id="back">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
</body>
</html>
