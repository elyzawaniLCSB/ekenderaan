<%-- 
    Document   : index
    Created on : Sep 29, 2019, 9:54:45 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String linkTo = "";
    String leaveIDlink = "";
    if (request.getParameter("linkTo") == null || request.getParameter("linkTo").equals("null")) {

    } else {
        linkTo = request.getParameter("linkTo");
        leaveIDlink = request.getParameter("leaveID");
    }
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="google-signin-client_id" content="228272176423-18ark4r5q77bjohhb6lvddon48hj911l.apps.googleusercontent.com"><!--local-->
        <!--<meta name="google-signin-client_id" content="1012990466557-qd2ea4dtu60b26rrmu3n1atshiads02o.apps.googleusercontent.com">-->

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <title>Sistem Permohonan Cuti LCSB</title>

        <link rel="apple-touch-icon" sizes="180x180" href="./assets/favicon_io/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./assets/favicon_io/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./assets/favicon_io/favicon-16x16.png">
        <link rel="manifest" href="./assets/favicon_io/site.webmanifest">


        <!-- GLOBAL MAINLY STYLES-->
        <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
        <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />
        <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <!-- THEME STYLES-->
        <link href="assets/css/main.css" rel="stylesheet" />
        <!-- PAGE LEVEL STYLES-->
        <style>
            body {
                background-repeat: no-repeat;
                background-size: cover;
                background-image: url('./assets/img/blog/bgnew.jpg');
                /*background-color: #097329;*/
            }

            .login-content {
                max-width: 900px;
                margin: 100px auto 50px;
            }

            .auth-head-icon {
                position: relative;
                height: 60px;
                width: 60px;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                font-size: 30px;
                background-color: #fff;
                color: #5c6bc0;
                box-shadow: 0 5px 20px #d6dee4;
                border-radius: 50%;
                transform: translateY(-50%);
                z-index: 2;
            }
            .g-signin2{
                width: 100%;
            }

            .g-signin2 > div{
                margin: 0 auto;
            }
        </style>
        <script type="text/javascript" charset="utf-8">
            function onSignIn(googleUser) {
                var profile = googleUser.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This isnull if the 'email' scope is not present.

                var auth2 = gapi.auth2.init();
                if (auth2.isSignedIn.get()) {
                    console.log('signedin');
                    window.location.href = 'SessionLogin?userID=' + profile.getId() + '&email=' + profile.getEmail() + '&name=' + profile.getName() + '&imageurl=' + profile.getImageUrl() + '&linkTo=<%= linkTo%>&leaveID=<%=leaveIDlink%>';
                    console.log('SessionLogin?userID=' + profile.getId() + '&email=' + profile.getEmail() + '&name=' + profile.getName() + '&imageurl=' + profile.getImageUrl() + '&linkTo=<%= linkTo%>&leaveID=<%=leaveIDlink%>');


                }
            }


            function onFailure(error) {
                console.log(error);
            }

        </script>
        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    </head>

    <body>
        <div class="row login-content">
            <div class="col-lg-6 col-sm-12 bg-white">
                <div class="text-center">
                    <span class="auth-head-icon"><img src="./assets/img/android-chrome-192x192.png" width="85%"></span>
                </div>
                <div class="ibox m-0 centered" style="box-shadow: none;">
                    <form class="ibox-body" id="login-form" action="javascript:;" method="POST">
                        <h2 class="text-center">Selamat Datang ke</h2><h1 class="display-4 font-strong text-center mb-4">eCuti</h1>
                        <div class="g-signin2" data-onsuccess="onSignIn" data-theme="light" data-width="270" data-height="50" data-longtitle="true"  data-prompt="select_account"></div>

                    </form>
                </div>
                <!--<div class="ibox-footer">
                    <table border="0" >
                        <tr>
                            <td><img src="./assets/img/logolcsb3.png" alt="img" width="20" border="0" style="display: block; width: 20;" /></td>
                            <td><span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px"> &nbsp; LKPP CORPORATION SDN BHD</span></td>
                        </tr>
                    
                    </table>
                </div>-->
            </div>

            <div class="col-lg-6 col-sm-12 d-inline-flex align-items-center text-primary py-4 px-5">
                <div>
                    <div class="h2 mb-4 text-primary font-change">Sistem Permohonan Cuti LCSB</div>
                    <!--<div class="mb-4 pt-3">
                        <button class="btn btn-outline btn-icon-only btn-circle mr-3"><i class="fa fa-facebook"></i></button>
                        <button class="btn btn-outline btn-icon-only btn-circle mr-3"><i class="fa fa-twitter"></i></button>
                        <button class="btn btn-outline btn-icon-only btn-circle mr-3"><i class="fa fa-pinterest-p"></i></button>
                    </div>-->
                    <p class="text-primary font-change">Mohon cuti anda dengan beberapa langkah yang mudah.</p>
                    <div class="flexbox-b mb-3 text-primary font-change"><i class="ti-check mr-3 text-primary font-change"></i>Senang untuk diuruskan.</div>
                    <div class="flexbox-b mb-3 text-primary font-change"><i class="ti-check mr-3 text-primary font-change"></i>Semak permohonan anda secara atas talian.</div>
                    <div class="flexbox-b mb-5 text-primary font-change"><i class="ti-check mr-3 text-primary font-change"></i>Lihat rekod-rekod sebelum dan terkini.</div>
                    <button class="btn btn-outline btn-rounded btn-fix  text-primary font-change">Hubungi Pentadbir</button>
                </div>
            </div>
            <footer class="page-footer">
                <div class="font-13">2020 © <b>LCSB</b> - Istiqamah Mencipta Kecemerlangan</div>
                <!--<div>
                    <a class="px-3 pl-4" href="http://themeforest.net/item/adminca-responsive-bootstrap-4-3-angular-4-admin-dashboard-template/20912589" target="_blank">Purchase</a>
                    <a class="px-3" href="http://admincast.com/adminca/documentation.html" target="_blank">Docs</a>
                </div>-->
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
        <!-- BEGIN PAGA BACKDROPS-->
        <div class="sidenav-backdrop backdrop"></div>
        <div class="preloader-backdrop">
            <div class="page-preloader">Loading</div>
        </div>
        <!-- CORE PLUGINS-->
        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="./assets/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js"></script>
        <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="./assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script>
        <script src="./assets/vendors/toastr/toastr.min.js"></script>
        <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <!-- PAGE LEVEL PLUGINS-->
        <!-- CORE SCRIPTS-->
        <script src="assets/js/app.min.js"></script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script>
            $(function () {
                $('#login-form').validate({
                    errorClass: "help-block",
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true
                        }
                    },
                    highlight: function (e) {
                        $(e).closest(".form-group").addClass("has-error")
                    },
                    unhighlight: function (e) {
                        $(e).closest(".form-group").removeClass("has-error")
                    },
                });
            });
        </script>

    </body>

</html>